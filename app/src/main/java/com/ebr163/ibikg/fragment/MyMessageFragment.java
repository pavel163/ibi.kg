package com.ebr163.ibikg.fragment;

import android.content.Intent;
import android.widget.BaseAdapter;

import com.ebr163.ibikg.C;
import com.ebr163.ibikg.activity.MessageDescActivity;
import com.ebr163.ibikg.adapter.MyMessageAdapter;
import com.ebr163.ibikg.event.OffsetEvent;
import com.ebr163.ibikg.fragment.base.BaseFragmentWithListView;

import java.util.List;
import java.util.Map;

import de.greenrobot.event.EventBus;

/**
 * Created by Bakht on 03.04.2016.
 */
public class MyMessageFragment extends BaseFragmentWithListView {

    public static MyMessageFragment newInstance(){
        return new MyMessageFragment();
    }

    @Override
    protected BaseAdapter getAdapter(List<Object> objectList) {
        return new MyMessageAdapter(getActivity(), objectList);
    }

    @Override
    protected void clickItem(int position) {
        Intent intent = new Intent(getActivity(), MessageDescActivity.class);
        intent.putExtra(C.message.id, ((Map)objectList.get(position)).get(C.message.id).toString());
        startActivity(intent);
    }

    @Override
    protected void postOffsetEvent(int page) {
        OffsetEvent event = new OffsetEvent(page);
        event.type = "my_message";
        EventBus.getDefault().post(event);
    }
}


package com.ebr163.ibikg.adapter.category;

import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.ebr163.ibikg.C;
import com.ebr163.ibikg.R;
import com.ebr163.ibikg.activity.AdDescActivity;
import com.ebr163.ibikg.ui.SmartyImageView;
import com.ebr163.ibikg.ui.SmartyTextView;
import com.ebr163.ibikg.util.DataUtil;

/**
 * Created by Bakht on 21.03.2016.
 */
public class AdHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

    public SmartyImageView image;
    protected SmartyTextView title;
    protected SmartyTextView price;
    protected SmartyTextView id;
    protected SmartyTextView data;
    protected SmartyTextView region;
    protected SmartyTextView currency;

    public static AdHolder newInstance(View parent) {
        SmartyImageView image = (SmartyImageView) parent.findViewById(R.id.image);
        SmartyTextView title = (SmartyTextView) parent.findViewById(R.id.title);
        SmartyTextView price = (SmartyTextView) parent.findViewById(R.id.price);
        SmartyTextView data = (SmartyTextView) parent.findViewById(R.id.data);
        SmartyTextView region = (SmartyTextView) parent.findViewById(R.id.region);
        SmartyTextView id = (SmartyTextView) parent.findViewById(R.id.id);
        SmartyTextView currency = (SmartyTextView) parent.findViewById(R.id.currency);
        return new AdHolder(parent, image, title, price, data, region, id, currency);
    }

    public AdHolder(View itemView, SmartyImageView image, SmartyTextView title, SmartyTextView price, SmartyTextView data,
                    SmartyTextView region, SmartyTextView id, SmartyTextView currency) {
        super(itemView);
        itemView.setOnClickListener(this);
        this.image = image;
        this.title = title;
        this.price = price;
        this.data = data;
        this.region = region;
        this.id = id;
        this.currency = currency;
    }

    public void setCurrency(String currency) {
        this.currency.setText(currency);
    }

    public void setPrice(String price) {
        this.price.setText(price + " " + currency.getText().toString());
    }

    public void setData(String data) {
        String [] d = data.split(" ");
        this.data.setText(DataUtil.getFormatData(d[0]) + " " + d[1]);
    }

    public void setRegion(String region) {
        this.region.setText(region);
    }

    public void setId(String id){
        this.id.setText(id);
    }

    public void setTitle(String title){
        this.title.setText(title);
    }

    public void setImage(String imageLink) {
        image.setImage(imageLink);
    }

    @Override
    public void onClick(View v) {
        Intent intent = new Intent(v.getContext(), AdDescActivity.class);
        intent.putExtra(C.fields.parent_id, id.getText().toString());
        v.getContext().startActivity(intent);
    }
}

package com.ebr163.ibikg.model;

/**
 * Created by Ergashev on 13.11.2016.
 */

public class Shop {
    public String id;
    public String ibi_name;
    public String phone;
    public String shop_name;
    public String shop_phone;
    public String shop_phone_mobile;
    public String shop_worktime;
    public String shop_email;
    public String shop_city_id;
    public String shop_address;
    public String shop_activity_kind;
    public String shop_about;
    public String shop_banner;
}

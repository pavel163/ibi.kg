package com.ebr163.ibikg.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.widget.BaseAdapter;

import com.ebr163.ibikg.C;
import com.ebr163.ibikg.R;
import com.ebr163.ibikg.activity.PayAdvertActivity;
import com.ebr163.ibikg.adapter.PaidAdapter;
import com.ebr163.ibikg.fragment.base.BaseFragmentWithListView;

import java.util.List;
import java.util.Map;

/**
 * Created by Bakht on 26.03.2016.
 */
public class PaidFragment extends BaseFragmentWithListView {

    public static PaidFragment newInstance() {
        return new PaidFragment();
    }

    @Override
    protected BaseAdapter getAdapter(List<Object> objectList) {
        return new PaidAdapter(getActivity(), objectList);
    }

    @Override
    protected void clickItem(int position) {
        Intent intent = new Intent(getActivity(), PayAdvertActivity.class);
        intent.putExtra(C.fields.service_id,  ((Map<String, Object>) objectList.get(position)).get(C.fields.id).toString());
        startActivity(intent);
    }
}

package com.ebr163.ibikg.activity;

import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.ebr163.ibikg.BaseApplication;
import com.ebr163.ibikg.R;
import com.ebr163.ibikg.activity.base.BaseActivityWithFragment;
import com.ebr163.ibikg.fragment.ChangeMyInfoFragment;
import com.ebr163.ibikg.model.Contact;
import com.ebr163.ibikg.service.SettingsService;
import com.ebr163.ibikg.service.http.HttpApiFactory;

import java.util.Map;

import rx.functions.Action1;
import rx.functions.Func1;

public class ChangeMyInfoActivity extends BaseActivityWithFragment implements ChangeMyInfoFragment.OnSetObjectsListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.activity_with_fragment);
        super.onCreate(savedInstanceState);
        setToolbarTitle("Редактирование");
    }

    @Override
    protected void initFragment() {
        fragment = ChangeMyInfoFragment.newInstance((Contact) getIntent().getSerializableExtra("contact"));
        getSupportFragmentManager().beginTransaction()
                .add(R.id.main_fragment_container, fragment)
                .commit();
    }

    @Override
    public void setObjects() {
        progress.setMessage("Сохранение");
        progress.show();
        Action1<Map<String, String>> onNextAction = new Action1<Map<String, String>>() {
            @Override
            public void call(Map<String, String> response) {
                progress.hide();
                if (response != null ) {
                    Toast.makeText(ChangeMyInfoActivity.this, "Данные сохранены", Toast.LENGTH_SHORT).show();
                    Log.d("Обновление контакта", "Успех");
                }
            }
        };

        Func1 error = new Func1() {
            @Override
            public Object call(Object o) {
                progress.hide();
                Toast.makeText(ChangeMyInfoActivity.this, "Ошибка. Данные не сохранены", Toast.LENGTH_SHORT).show();
                Log.d("Обновление контакта", "Провал");
                return null;
            }
        };

        Contact contact = ((ChangeMyInfoFragment) fragment).getContact();
        contact.key = SettingsService.getUser(this).key;
        BaseApplication.instance.httpService.subscribeAction(HttpApiFactory.HttpMethodType.SAVE_CONTACT, onNextAction, error, contact);
    }

    @Override
    protected void onStop() {
        super.onStop();
        BaseApplication.instance.httpService.unsubscribe();
    }
}

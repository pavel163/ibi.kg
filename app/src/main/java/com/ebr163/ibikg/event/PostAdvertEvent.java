package com.ebr163.ibikg.event;

import com.ebr163.ibikg.event.base.BaseEvent;
import com.ebr163.ibikg.model.AdPostData;

/**
 * Created by Bakht on 27.03.2016.
 */
public class PostAdvertEvent extends BaseEvent {

    public AdPostData advert;

    public PostAdvertEvent(AdPostData advert){
        this.advert = advert;
    }
}

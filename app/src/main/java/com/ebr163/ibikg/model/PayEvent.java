package com.ebr163.ibikg.model;

import com.ebr163.ibikg.event.base.BaseEvent;

/**
 * Created by Bakht on 19.04.2016.
 */
public class PayEvent extends BaseEvent {

    public String data;

    public PayEvent(String data) {
        this.data = data;
    }
}

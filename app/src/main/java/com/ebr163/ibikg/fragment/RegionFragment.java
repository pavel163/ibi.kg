package com.ebr163.ibikg.fragment;

import android.content.Intent;
import android.widget.BaseAdapter;

import com.ebr163.ibikg.C;
import com.ebr163.ibikg.adapter.RegionAdapter;
import com.ebr163.ibikg.fragment.base.BaseFragmentWithListView;

import java.util.List;
import java.util.Map;

/**
 * Created by Bakht on 01.04.2016.
 */
public class RegionFragment extends CityFragment {

    public static RegionFragment newInstance() {
        return new RegionFragment();
    }

    @Override
    protected BaseAdapter getAdapter(List<Object> objectList) {
        return new RegionAdapter(getActivity(), objectList);
    }

    @Override
    protected void clickItem(int position) {
        Intent intent = new Intent();
        intent.putExtra(C.location.region_id, ((Map<String, String>) objectList.get(position)).get(C.location.id));
        intent.putExtra(C.location.region_name, ((Map<String, String>) objectList.get(position)).get(C.location.region_name));
        getActivity().setResult(getActivity().RESULT_OK, intent);
        getActivity().finish();
    }
}

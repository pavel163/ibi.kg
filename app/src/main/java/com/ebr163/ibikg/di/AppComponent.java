package com.ebr163.ibikg.di;

import com.ebr163.ibikg.BaseApplication;
import com.ebr163.ibikg.di.module.AppModule;
import com.ebr163.ibikg.di.module.CRUDModule;
import com.ebr163.ibikg.di.module.HttpModule;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by Bakht on 23.05.2016.
 */
@Component(modules = {AppModule.class, CRUDModule.class, HttpModule.class})
@Singleton
public interface AppComponent {
    void inject(BaseApplication application);
}

package com.ebr163.ibikg.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.ebr163.ibikg.BaseApplication;
import com.ebr163.ibikg.C;
import com.ebr163.ibikg.R;
import com.ebr163.ibikg.activity.base.BaseActivityWithFragment;
import com.ebr163.ibikg.activity.login.SignInActivity;
import com.ebr163.ibikg.event.BlackListEvent;
import com.ebr163.ibikg.event.OffsetEvent;
import com.ebr163.ibikg.fragment.BlackListFragment;
import com.ebr163.ibikg.fragment.CategoryFragment;
import com.ebr163.ibikg.fragment.FavorsFragment;
import com.ebr163.ibikg.fragment.HistoryOperationFragment;
import com.ebr163.ibikg.fragment.MyMessageFragment;
import com.ebr163.ibikg.fragment.PaidFragment;
import com.ebr163.ibikg.fragment.ShareCategoryFragment;
import com.ebr163.ibikg.fragment.ShopsFragment;
import com.ebr163.ibikg.fragment.VPFragment;
import com.ebr163.ibikg.fragment.base.BaseFragment;
import com.ebr163.ibikg.fragment.base.BaseFragmentWithList;
import com.ebr163.ibikg.fragment.base.BaseFragmentWithListView;
import com.ebr163.ibikg.fragment.base.BaseFragmentWithRecyclerView;
import com.ebr163.ibikg.model.Category;
import com.ebr163.ibikg.model.MessageList;
import com.ebr163.ibikg.model.Response;
import com.ebr163.ibikg.model.ResponseList;
import com.ebr163.ibikg.model.Shop;
import com.ebr163.ibikg.model.ads.Ad;
import com.ebr163.ibikg.model.ads.AdDB;
import com.ebr163.ibikg.model.ads.AdDesc;
import com.ebr163.ibikg.service.SettingsService;
import com.ebr163.ibikg.service.http.HttpApiFactory;
import com.ebr163.ibikg.ui.SmartyTextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.greenrobot.event.EventBus;
import rx.functions.Action1;
import rx.functions.Func1;

public class MainActivity extends BaseActivityWithFragment implements NavigationView.OnNavigationItemSelectedListener,
        View.OnClickListener, BaseFragmentWithListView.OnSetObjectsListener {

    private DrawerLayout drawer;
    private SmartyTextView title;
    private SmartyTextView subtitle;
    private static int mCurrentSelectedPosition = 0;
    private SmartyTextView titleTxt;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.activity_main);
        super.onCreate(savedInstanceState);
        initFragment();
    }

    @Override
    protected void initUI() {
        titleTxt = (SmartyTextView) findViewById(R.id.toolbar_title);
        titleTxt.setFont(this, "title.ttf");
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        View header = navigationView.inflateHeaderView(R.layout.nav_header_main);
        title = (SmartyTextView) header.findViewById(R.id.drawer_header_title);
        subtitle = (SmartyTextView) header.findViewById(R.id.drawer_header_subtitle);
    }

    @Override
    protected void initFragment() {
        mCurrentSelectedPosition = getIntent().getIntExtra("fragment", 0);
        clickMenuItem(mCurrentSelectedPosition);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!"".equals(SettingsService.getUser(this).name) || !"".equals(SettingsService.getUser(this).email)) {
            title.setText(SettingsService.getUser(this).name);
            subtitle.setText(SettingsService.getUser(this).email);
        } else {
            title.setText("Войти или");
            subtitle.setText("зарегистрироваться");
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 111) {
            ((VPFragment) fragment).clearData();
            setObjects(1);
            setObjects(2);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Override
    protected void initToolbar() {
        super.initToolbar();
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();
    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.ad_item) {
            clickMenuItem(0);
        } else if (id == R.id.share_item) {
            if (isAuth()) {
                clickMenuItem(1);
            } else {
                Toast.makeText(MainActivity.this, "Необходимо войти", Toast.LENGTH_SHORT).show();
            }
        } else if (id == R.id.favors_add) {
            clickMenuItem(2);
        } else if (id == R.id.paid_item) {
            if (isAuth()) {
                clickMenuItem(3);
            } else {
                Toast.makeText(MainActivity.this, "Необходимо войти", Toast.LENGTH_SHORT).show();
            }
        } else if (id == R.id.my_adverts) {
            if (isAuth()) {
                clickMenuItem(4);
            } else {
                Toast.makeText(MainActivity.this, "Необходимо войти", Toast.LENGTH_SHORT).show();
            }
        } else if (id == R.id.my_message) {
            if (isAuth()) {
                clickMenuItem(5);
            } else {
                Toast.makeText(MainActivity.this, "Необходимо войти", Toast.LENGTH_SHORT).show();
            }
        } else if (id == R.id.black_list) {
            if (isAuth()) {
                clickMenuItem(6);
            } else {
                Toast.makeText(MainActivity.this, "Необходимо войти", Toast.LENGTH_SHORT).show();
            }
        } else if (id == R.id.history) {
            if (isAuth()) {
                clickMenuItem(7);
            } else {
                Toast.makeText(MainActivity.this, "Необходимо войти", Toast.LENGTH_SHORT).show();
            }
        } else if (id == R.id.shop) {
            clickMenuItem(8);
        }

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void setupFragment(int index) {
        String tag = null;
        switch (index) {
            case 0:
                tag = C.fragment.tag_category;
                fragment = (BaseFragment) getSupportFragmentManager().findFragmentByTag(tag);
                if (fragment == null) {
                    fragment = CategoryFragment.newInstance();
                }
                titleTxt.setVisibility(View.VISIBLE);
                setToolbarTitle(null);
                setToolbarSubTitle(null);
                setVisibleTabs(View.GONE);
                break;
            case 1:
                tag = C.fragment.tag_share;
                fragment = (BaseFragment) getSupportFragmentManager().findFragmentByTag(tag);
                if (fragment == null) {
                    fragment = ShareCategoryFragment.newInstance("", 1);
                }
                titleTxt.setVisibility(View.GONE);
                setToolbarTitle("Новое объявление");
                setToolbarSubTitle("Шаг 1");
                setVisibleTabs(View.GONE);
                break;
            case 2:
                titleTxt.setVisibility(View.GONE);
                tag = C.fragment.tag_favors;
                fragment = (BaseFragment) getSupportFragmentManager().findFragmentByTag(tag);
                if (fragment == null) {
                    fragment = FavorsFragment.newInstance();
                }
                setToolbarTitle("Избранное");
                setToolbarSubTitle(null);
                setVisibleTabs(View.GONE);
                break;
            case 3:
                titleTxt.setVisibility(View.GONE);
                tag = C.fragment.tag_paid;
                fragment = (BaseFragment) getSupportFragmentManager().findFragmentByTag(tag);
                if (fragment == null) {
                    fragment = PaidFragment.newInstance();
                }
                setToolbarTitle("Платные услуги");
                setToolbarSubTitle(null);
                setVisibleTabs(View.GONE);
                break;
            case 4:
                titleTxt.setVisibility(View.GONE);
                tag = C.fragment.tag_my_advert;
                fragment = (BaseFragment) getSupportFragmentManager().findFragmentByTag(tag);
                if (fragment == null) {
                    fragment = VPFragment.newInstance();
                }
                setToolbarTitle("Мои объявления");
                setToolbarSubTitle(null);
                setVisibleTabs(View.VISIBLE);
                break;
            case 5:
                titleTxt.setVisibility(View.GONE);
                tag = C.fragment.tag_message;
                fragment = (BaseFragment) getSupportFragmentManager().findFragmentByTag(tag);
                if (fragment == null) {
                    fragment = MyMessageFragment.newInstance();
                }
                setToolbarTitle("Мои сообщения");
                setToolbarSubTitle(null);
                setVisibleTabs(View.GONE);
                break;
            case 6:
                titleTxt.setVisibility(View.GONE);
                tag = C.fragment.tag_black_list;
                fragment = (BaseFragment) getSupportFragmentManager().findFragmentByTag(tag);
                if (fragment == null) {
                    fragment = BlackListFragment.newInstance();
                }
                setToolbarTitle("Черный список");
                setToolbarSubTitle(null);
                setVisibleTabs(View.GONE);
                break;
            case 7:
                titleTxt.setVisibility(View.GONE);
                tag = C.fragment.tag_history;
                fragment = (BaseFragment) getSupportFragmentManager().findFragmentByTag(tag);
                if (fragment == null) {
                    fragment = HistoryOperationFragment.newInstance();
                }
                setToolbarTitle("История операций");
                setToolbarSubTitle(null);
                setVisibleTabs(View.GONE);
                break;
            case 8:
                titleTxt.setVisibility(View.GONE);
                tag = C.fragment.tag_shop;
                fragment = (BaseFragment) getSupportFragmentManager().findFragmentByTag(tag);
                if (fragment == null) {
                    fragment = new ShopsFragment();
                }
                setToolbarTitle("Магазины");
                setToolbarSubTitle(null);
                setVisibleTabs(View.GONE);
                break;
            default:
                break;
        }
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.main_fragment_container, fragment, tag)
                .commit();
    }

    private void clickMenuItem(int position) {
        mCurrentSelectedPosition = position;
        setupFragment(position);
    }

    @Override
    public void setObjects(Object params) {
        BaseApplication.instance.httpService.unsubscribe();
        Action1 action1 = null;
        HttpApiFactory.HttpMethodType number = HttpApiFactory.HttpMethodType.GET_CATEGORY;
        Object data = null;
        switch (mCurrentSelectedPosition) {
            case 0:
            case 1:
                action1 = getCategory();
                number = HttpApiFactory.HttpMethodType.GET_CATEGORY;
                data = ((CategoryFragment) fragment).getParentId();
                break;
            case 2:
                getAdsFromDB();
                return;
            case 3:
                action1 = getPaids();
                number = HttpApiFactory.HttpMethodType.GET_PAID_SERVICE;
                break;
            case 4:
                number = HttpApiFactory.HttpMethodType.GET_USER_ADVERTS;
                data = new HashMap<>();
                ((Map) data).put(C.fields.key, SettingsService.getUser(this).key);
                if (params != null && (int) params == 1) {
                    action1 = getMyAdverts(0, 1);
                    ((Map) data).put(C.advert.published, "1");
                } else {
                    action1 = getMyAdverts(0, 2);
                    ((Map) data).put(C.advert.published, "0");
                }
                break;
            case 5:
                action1 = getMyMessage(0);
                number = HttpApiFactory.HttpMethodType.GET_USER_MAIL;
                data = new HashMap<>();
                ((Map) data).put(C.fields.key, SettingsService.getUser(this).key);
                break;
            case 6:
                action1 = getBlackList();
                number = HttpApiFactory.HttpMethodType.GET_USER_BLACK_LIST;
                data = SettingsService.getUser(this).key;
                break;
            case 7:
                action1 = getHistory();
                number = HttpApiFactory.HttpMethodType.GET_USER_HISTORY;
                data = new HashMap<>();
                ((Map) data).put(C.fields.key, SettingsService.getUser(this).key);
                ((Map) data).put(C.fields.limit, "100");
                ((Map) data).put(C.fields.offset, "0");
                break;
            case 8:
                action1 = getShop();
                number = HttpApiFactory.HttpMethodType.GET_SHOPS;
                break;
        }

        BaseApplication.instance.httpService.subscribeAction(number, action1, getError(), data);
    }

    private Action1 getShop() {
        Action1<ResponseList<Shop>> onNextAction = new Action1<ResponseList<Shop>>() {
            @Override
            public void call(ResponseList<Shop> response) {
                if (response != null) {
                    ((BaseFragmentWithList) fragment).setObjectList(response.data);
                    ((BaseFragmentWithRecyclerView) fragment).setupRecyclerView(response.data);
                    Log.d("Загрузка сообщений", "Успех");
                }
            }
        };
        return onNextAction;
    }

    private Action1 getHistory() {
        return new Action1<ResponseList<Map<String, Object>>>() {
            @Override
            public void call(ResponseList<Map<String, Object>> response) {
                if (response != null) {
                    if (response.success) {
                        progress.hide();
                        if (response.data.size() != 0) {
                            ((BaseFragmentWithListView) fragment).setVisibleProgress(View.GONE);
                            ((BaseFragmentWithListView) fragment).setupListView(response.data);
                            ((BaseFragmentWithListView) fragment).setObjectList(response.data);
                        }
                    } else {
                        ((BaseFragmentWithListView) fragment).setVisibleErrorMessage(View.VISIBLE);
                        ((BaseFragmentWithListView) fragment).setVisibleProgress(View.GONE);
                        Toast.makeText(MainActivity.this, "Ошибка", Toast.LENGTH_LONG).show();
                        Log.d("История операций", "Сервер вернул ошибку");
                    }
                }
            }
        };
    }

    private Func1 getError() {
        Func1 error = new Func1() {
            @Override
            public Object call(Object o) {
                if (fragment instanceof BaseFragmentWithList) {
                    ((BaseFragmentWithList) fragment).setVisibleErrorMessage(View.VISIBLE);
                    ((BaseFragmentWithList) fragment).setVisibleProgress(View.GONE);
                } else if (fragment instanceof VPFragment) {
                    ((VPFragment) fragment).errorAction();
                }
                Log.d("Загрузка категорий", "Провал");
                return null;
            }
        };
        return error;
    }

    @Override
    public void onClick(View v) {
        if (SettingsService.getUser(this).key.equals("")) {
            startActivity(new Intent(this, SignInActivity.class));
        } else {
            startActivity(new Intent(this, ContactActivity.class));
        }
    }

    private Action1 getCategory() {
        Action1<Response<Category>> onNextAction = new Action1<Response<Category>>() {
            @Override
            public void call(Response<Category> response) {
                if (response != null) {
                    List<Object> categories = new ArrayList<>();

                    for (Map.Entry<String, Category> entry : response.data.entrySet()) {
                        categories.add(entry.getValue());
                    }
                    //TODO Порефакторить setupListView
                    ((BaseFragmentWithList) fragment).setObjectList(categories);
                    ((BaseFragmentWithListView) fragment).setupListView(categories);
                    Log.d("Загрузка категорий", "Успех");
                }
            }
        };
        return onNextAction;
    }

    private void getAdsFromDB() {
        List<Object> objects = BaseApplication.instance.crudService.getAds(AdDB.class);
        ((BaseFragmentWithRecyclerView) fragment).setObjectList(objects);
        ((BaseFragmentWithRecyclerView) fragment).setupRecyclerView(objects);
    }

    private Action1 getPaids() {
        Action1<ResponseList<Object>> onNextAction = new Action1<ResponseList<Object>>() {
            @Override
            public void call(ResponseList<Object> response) {
                if (response != null) {
                    ((BaseFragmentWithList) fragment).setObjectList(response.data);
                    ((BaseFragmentWithListView) fragment).setupListView(response.data);
                    Log.d("Загрузка платных услуг", "Успех");
                }
            }
        };
        return onNextAction;
    }

    private Action1 getMyAdverts(final int page, final int numberFragment) {
        Action1<ResponseList<Map<String, AdDesc>>> onNextAction = new Action1<ResponseList<Map<String, AdDesc>>>() {
            @Override
            public void call(ResponseList<Map<String, AdDesc>> response) {
                if (response != null) {
                    List<Object> data = new ArrayList<>();
                    for (Object object : response.data) {
                        Ad ad = new Ad();
                        ad.id = ((Map) object).get(C.fields.id).toString();
                        ad.advert_name = ((Map) object).get(C.advert.advert_name).toString();
                        ad.price = String.valueOf(((Map) object).get(C.advert.price));
                        ad.date_publish = ((Map) object).get(C.advert.date_publish).toString();
                        ad.region_name = String.valueOf(((Map) object).get(C.location.region_name).toString());
                        if (((Map) object).get(C.advert.name_currency) != null) {
                            ad.name_currency = ((Map) object).get(C.advert.name_currency).toString();
                        }
                        ad.photos = (ArrayList) ((Map) object).get(C.advert.photos);
                        ad.published = String.valueOf(((Map) object).get(C.advert.published));
                        data.add(ad);
                    }
                    if (page == 0) {
                        ((VPFragment) fragment).setObjectList(data, numberFragment);
                    } else {
                        ((VPFragment) fragment).addObjectList(data, numberFragment);
                    }
                    Log.d("Загрузка объявлений", "Успех");
                }
            }
        };
        return onNextAction;
    }

    private Action1 getMyMessage(final int page) {
        Action1<MessageList<Object>> onNextAction = new Action1<MessageList<Object>>() {
            @Override
            public void call(MessageList<Object> response) {
                if (response != null) {
                    if (page == 0) {
                        ((BaseFragmentWithList) fragment).setObjectList(response.data);
                        ((BaseFragmentWithListView) fragment).setupListView(response.data);
                    } else {
                        ((BaseFragmentWithList) fragment).addObjectList(response.data);
                        ((BaseFragmentWithListView) fragment).updateList();
                    }
                    Log.d("Загрузка сообщений", "Успех");
                }
            }
        };
        return onNextAction;
    }

    private Action1 getBlackList() {
        Action1<ResponseList<Object>> onNextAction = new Action1<ResponseList<Object>>() {
            @Override
            public void call(ResponseList<Object> response) {
                if (response != null) {
                    ((BaseFragmentWithList) fragment).setObjectList(response.data);
                    ((BaseFragmentWithListView) fragment).setupListView(response.data);
                    Log.d("Загрузка черного списка", "Успех");
                }
            }
        };
        return onNextAction;
    }

    public void onEvent(BlackListEvent event) {
        progress.setMessage("Удаление...");
        progress.show();
        Action1<Map<String, String>> onNextAction = new Action1<Map<String, String>>() {
            @Override
            public void call(Map<String, String> response) {
                if (response != null) {
                    ((BaseFragmentWithListView) fragment).updateData();
                    Toast.makeText(MainActivity.this, "Контакт удален", Toast.LENGTH_SHORT).show();
                    progress.hide();
                }
            }
        };

        Func1 error = new Func1() {
            @Override
            public Object call(Object o) {
                progress.hide();
                Toast.makeText(MainActivity.this, "Ошибка. Контакт не удален", Toast.LENGTH_SHORT).show();
                Log.d("Удаление из ЧС", "Провал");
                return null;
            }
        };

        BaseApplication.instance.httpService.subscribeAction(HttpApiFactory.HttpMethodType.DELETE_FROM_BLACK_LIST, onNextAction, error, event.data);
    }

    private boolean isAuth() {
        if (SettingsService.getUser(this) != null && !SettingsService.getUser(this).key.equals("")) {
            return true;
        }
        return false;
    }

    public void onEvent(OffsetEvent event) {
        Map<String, String> data = new HashMap<>();
        if ("my_advert_publish".equals(event.type)) {
            ((Map) data).put(C.fields.key, SettingsService.getUser(this).key);
            ((Map) data).put(C.fields.offset, String.valueOf(event.offset * 10));
            ((Map) data).put(C.advert.published, "1");
            BaseApplication.instance.httpService.subscribeAction(HttpApiFactory.HttpMethodType.GET_USER_ADVERTS, getMyAdverts(event.offset, 1), getError(), data);
        }
        if ("my_advert_no_publish".equals(event.type)) {
            ((Map) data).put(C.fields.key, SettingsService.getUser(this).key);
            ((Map) data).put(C.fields.offset, String.valueOf(event.offset * 10));
            ((Map) data).put(C.advert.published, "0");
            BaseApplication.instance.httpService.subscribeAction(HttpApiFactory.HttpMethodType.GET_USER_ADVERTS, getMyAdverts(event.offset, 2), getError(), data);
        }
        if ("my_message".equals(event.type)) {
            ((Map) data).put(C.fields.key, SettingsService.getUser(this).key);
            ((Map) data).put(C.fields.offset, String.valueOf(event.offset * 10));
            BaseApplication.instance.httpService.subscribeAction(HttpApiFactory.HttpMethodType.GET_USER_MAIL, getMyMessage(event.offset), getError(), data);
        }
    }
}

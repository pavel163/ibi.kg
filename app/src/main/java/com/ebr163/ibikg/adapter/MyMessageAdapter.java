package com.ebr163.ibikg.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.ebr163.ibikg.C;
import com.ebr163.ibikg.R;
import com.ebr163.ibikg.helper.ConvertHelper;
import com.ebr163.ibikg.ui.SmartyTextView;
import com.ebr163.ibikg.util.DataUtil;

import java.util.List;
import java.util.Map;

/**
 * Created by Bakht on 26.03.2016.
 */
public class MyMessageAdapter extends BaseAdapter {

    protected List<Object> messages;
    protected LayoutInflater lInflater;
    protected Context context;

    public MyMessageAdapter(Context context, List<Object> objectList) {
        this.messages = objectList;
        this.context = context;
        lInflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return messages.size();
    }

    @Override
    public Object getItem(int position) {
        return messages.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View rowView = convertView;
        if (rowView == null) {
            rowView = lInflater.inflate(R.layout.item_my_message, null);
            ViewHolder viewHolder = new ViewHolder();
            viewHolder.title = (SmartyTextView) rowView.findViewById(R.id.title);
            viewHolder.from = (SmartyTextView) rowView.findViewById(R.id.from_user);
            viewHolder.descr = (SmartyTextView) rowView.findViewById(R.id.description);
            viewHolder.data = (SmartyTextView) rowView.findViewById(R.id.data);
            rowView.setTag(viewHolder);
        }

        ViewHolder holder = (ViewHolder) rowView.getTag();
        Map<String, Object> map = (Map<String, Object>) messages.get(position);
        holder.title.setText("Тема: " + map.get(C.message.mail_title).toString());
        if (map.get(C.message.unread).equals("0")){
            holder.from.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_drafts_24dp, 0, 0, 0);
        }
        holder.from.setText("От: " + map.get(C.message.from_name).toString());
        String [] d = map.get(C.message.mail_date).toString().split(" ");
        holder.data.setText(DataUtil.getFormatData(d[0]) + " " + d[1]);
        holder.descr.setText(map.get(C.message.mail_body).toString());
        return rowView;
    }

    static class ViewHolder {
        public SmartyTextView title;
        public SmartyTextView from;
        public SmartyTextView descr;
        public SmartyTextView data;
    }
}

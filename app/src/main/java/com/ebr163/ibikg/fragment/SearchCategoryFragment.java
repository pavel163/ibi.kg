package com.ebr163.ibikg.fragment;

import android.content.Intent;
import android.os.Bundle;

import com.ebr163.ibikg.C;
import com.ebr163.ibikg.model.Category;

/**
 * Created by Bakht on 30.03.2016.
 */
public class SearchCategoryFragment extends CategoryFragment {

    public static SearchCategoryFragment newInstance(String id) {
        Bundle args = new Bundle();
        args.putString(C.fields.parent_id, id);
        SearchCategoryFragment searchCategoryFragment = new SearchCategoryFragment();
        searchCategoryFragment.setArguments(args);
        return searchCategoryFragment;
    }

    @Override
    protected void clickItem(int position) {
        Intent intent = new Intent();
        intent.putExtra(C.fields.parent_id, ((Category) objectList.get(position)).id);
        intent.putExtra(C.fields.childs, ((Category) objectList.get(position)).childs);
        intent.putExtra(C.advert.name_category, ((Category) objectList.get(position)).name_category);
        getActivity().setResult(getActivity().RESULT_OK, intent);
        getActivity().onBackPressed();
    }
}

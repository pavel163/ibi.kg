package com.ebr163.ibikg.fragment;

import com.ebr163.ibikg.event.OffsetEvent;

import de.greenrobot.event.EventBus;

/**
 * Created by Bakht on 17.04.2016.
 */
public class MyAdvertFragmentNoPublish extends MyAdvertFragmentPublish {

    public static MyAdvertFragmentNoPublish newInstance() {
        return new MyAdvertFragmentNoPublish();
    }

    @Override
    protected void checkData() {
        if (objectList == null){
            objectsListener.setObjects(2);
        } else {
            setupRecyclerView(objectList);
        }
    }

    @Override
    protected void postOffsetEvent(int page) {
        OffsetEvent event = new OffsetEvent(page);
        event.type = "my_advert_no_publish";
        EventBus.getDefault().post(event);
    }
}

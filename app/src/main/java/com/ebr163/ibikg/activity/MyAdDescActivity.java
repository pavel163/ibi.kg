package com.ebr163.ibikg.activity;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.ebr163.ibikg.BaseApplication;
import com.ebr163.ibikg.C;
import com.ebr163.ibikg.R;
import com.ebr163.ibikg.fragment.MyAdDescFragment;
import com.ebr163.ibikg.model.Response;
import com.ebr163.ibikg.service.SettingsService;
import com.ebr163.ibikg.service.http.HttpApiFactory;

import java.util.HashMap;
import java.util.Map;

import rx.functions.Action1;
import rx.functions.Func1;

public class MyAdDescActivity extends AdDescActivity implements MyAdDescFragment.OnMenuItemListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        progress = new ProgressDialog(this);
    }

    @Override
    protected void initFragment() {
        fragment = MyAdDescFragment.newInstance();
        getSupportFragmentManager().beginTransaction()
                .add(R.id.main_fragment_container, fragment)
                .commit();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_my_advert, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onPublish(String id) {
        progress.setMessage("Публикация...");
        progress.show();
        Action1<Response<String>> onNextAction = new Action1<Response<String>>() {
            @Override
            public void call(Response<String> response) {
                progress.hide();
                if (response != null) {
                    ((MyAdDescFragment) fragment).statusPublished(response.data.get(C.advert.advert_status));
                    Log.d("Смена статуса", "Успех");
                }
            }
        };

        sendPublishStatus(onNextAction, id, "1");
    }

    @Override
    public void onRemovePublish(String id) {
        progress.setMessage("Снятие публикации...");
        progress.show();
        Action1<Response<String>> onNextAction = new Action1<Response<String>>() {
            @Override
            public void call(Response<String> response) {
                progress.hide();
                if (response != null) {
                    ((MyAdDescFragment) fragment).statusPublished(response.data.get(C.advert.advert_status));
                    Log.d("Смена статуса", "Успех");
                }
            }
        };

        sendPublishStatus(onNextAction, id, "0");
    }

    @Override
    public void onRemoveAdvert(String id) {
        progress.setMessage("Удаление объявления...");
        progress.show();
        Action1<Response<String>> onNextAction = new Action1<Response<String>>() {
            @Override
            public void call(Response<String> response) {
                progress.hide();
                if (response != null) {
                    Log.d("Удаление объявления", "Успех");
                    onBackPressed();
                }
            }
        };

        Func1 error = new Func1() {
            @Override
            public Object call(Object o) {
                progress.hide();
                Log.d("Удаление объявления", "Провал");
                return null;
            }
        };

        Map<String, String> map = new HashMap<>();
        map.put(C.fields.key, SettingsService.getUser(this).key);
        map.put(C.advert.advert_id, id);
        BaseApplication.instance.httpService.subscribeAction(HttpApiFactory.HttpMethodType.DELETE_USER_ADVERT, onNextAction, error, map);
    }

    private void sendPublishStatus(Action1 action1, String id, String publish) {
        Func1 error = new Func1() {
            @Override
            public Object call(Object o) {
                progress.hide();
                Log.d("Смена статуса", "Провал");
                return null;
            }
        };
        Map<String, String> map = new HashMap<>();
        map.put(C.fields.key, SettingsService.getUser(this).key);
        map.put(C.advert.published, publish);
        map.put(C.advert.advert_id, id);
        BaseApplication.instance.httpService.subscribeAction(HttpApiFactory.HttpMethodType.PUBLISH_USER_ADVERT, action1, error, map);
    }

    @Override
    protected void onStop() {
        super.onStop();
        BaseApplication.instance.httpService.unsubscribe();
    }
}

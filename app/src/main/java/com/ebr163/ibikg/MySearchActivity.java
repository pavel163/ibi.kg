package com.ebr163.ibikg;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.ebr163.ibikg.activity.AdActivity;
import com.ebr163.ibikg.activity.base.BaseActivityWithFragment;
import com.ebr163.ibikg.fragment.GlobalSearchFragment;
import com.ebr163.ibikg.model.Category;
import com.ebr163.ibikg.model.Response;
import com.ebr163.ibikg.service.http.HttpApiFactory;

import rx.functions.Action1;
import rx.functions.Func1;

public class MySearchActivity extends BaseActivityWithFragment implements GlobalSearchFragment.OnSetObjectsListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setToolbarTitle("Уточнить");
    }

    @Override
    protected void initFragment() {
        fragment = GlobalSearchFragment.newInstance(getIntent().getStringExtra(C.fields.parent_id));
        getSupportFragmentManager().beginTransaction()
                .add(R.id.main_fragment_container, fragment)
                .commit();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_search, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.show_result) {
            Intent intent = new Intent(this, AdActivity.class);
            intent.putExtra(C.fields.parent_id, fragment.getArguments().getString(C.fields.parent_id));
            intent.putExtra(C.location.region_id, fragment.getArguments().getString(C.location.region_id));
            intent.putExtra(C.location.city_id, fragment.getArguments().getString(C.location.city_id));
            intent.putExtra(C.fields.title, fragment.getArguments().getString(C.fields.title));
            intent.putExtra(C.fields.limit, ((GlobalSearchFragment) fragment).getLimit());
            intent.putExtra(C.fields.filter, ((GlobalSearchFragment) fragment).getFilter());
            startActivity(intent);
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void setObjects(String parentId) {
        progress.show();
        Action1<Response<Category>> onNextAction = new Action1<Response<Category>>() {
            @Override
            public void call(Response<Category> response) {
                if (response != null) {
                    progress.hide();
                    LayoutInflater lInflater = (LayoutInflater) MySearchActivity.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    View view = lInflater.inflate(R.layout.txt_category, null);
                    ((GlobalSearchFragment) fragment).addContent(view);
                }
            }
        };

        Func1 error = new Func1() {
            @Override
            public Object call(Object o) {
                progress.hide();
                Log.d("Загрузка категорий", "Провал");
                return null;
            }
        };

        BaseApplication.instance.httpService.subscribeAction(HttpApiFactory.HttpMethodType.GET_CATEGORY, onNextAction, error, parentId);
    }

    @Override
    protected void onStop() {
        super.onStop();
        BaseApplication.instance.httpService.unsubscribe();
    }
}

package com.ebr163.ibikg.ui.autoloadingrecycler;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ebr163.ibikg.R;

/**
 * Created by Bakht on 09.04.2016.
 */
public class LoadingRecyclerViewAdapter extends AutoLoadingRecyclerViewAdapter<View> {

    private static final int MAIN_VIEW = 0;

    static class MainViewHolder extends RecyclerView.ViewHolder {

        TextView textView;

        public MainViewHolder(View itemView) {
            super(itemView);
            textView = (TextView) itemView.findViewById(R.id.text);
        }
    }

    @Override
    public long getItemId(int position) {
        return getItem(position).getId();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
//        if (viewType == MAIN_VIEW) {
//        //    View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_item, parent, false);
//            return new MainViewHolder(v);
//        }
        return null;
    }

    @Override
    public int getItemViewType(int position) {
        return MAIN_VIEW;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        switch (getItemViewType(position)) {
            case MAIN_VIEW:
                onBindTextHolder(holder, position);
                break;
        }
    }

    private void onBindTextHolder(RecyclerView.ViewHolder holder, int position) {
        MainViewHolder mainHolder = (MainViewHolder) holder;
       // mainHolder.textView.setText(getItem(position).getItemStr());
    }

}

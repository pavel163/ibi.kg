package com.ebr163.ibikg.fragment.login;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.ebr163.ibikg.R;
import com.ebr163.ibikg.event.LoginEvent;
import com.ebr163.ibikg.fragment.base.BaseFragment;
import com.ebr163.ibikg.util.ValidationUtils;

import de.greenrobot.event.EventBus;

/**
 * Created by Bakht on 16.03.2016.
 */
public class SignUpFragment extends BaseFragment implements View.OnClickListener{

    private TextInputLayout name;
    private TextInputLayout email;
    private TextInputLayout phone;
    private TextInputLayout password;

    public static SignUpFragment newInstance() {
        return new SignUpFragment();
    }

    @Override
    public int getLayout() {
        return R.layout.fragment_sign_up;
    }

    @Override
    protected void initUI(View view) {
        name = (TextInputLayout) view.findViewById(R.id.name);
        email = (TextInputLayout) view.findViewById(R.id.email);
        phone = (TextInputLayout) view.findViewById(R.id.phone);
        password = (TextInputLayout) view.findViewById(R.id.password);
        Button signUp = (Button)view.findViewById(R.id.sign_up);
        signUp.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        // TODO Баг в выводе ошибки. При повторной ошибки не показывает текст и не загорается красным
        String name = ValidationUtils.getFieldValue(this.name);
        String email = ValidationUtils.getFieldValue(this.email);
        String phone = ValidationUtils.getFieldValue(this.phone);
        String password = ValidationUtils.getFieldValue(this.password);
        if (name != null && email != null && phone != null && password != null){
            EventBus.getDefault().post(new LoginEvent(name, email, phone, password));
        }
    }
}

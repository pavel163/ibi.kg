package com.ebr163.ibikg.fragment;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.view.View;
import android.widget.CheckBox;
import android.widget.Spinner;

import com.ebr163.ibikg.C;
import com.ebr163.ibikg.model.ads.AdDesc;
import com.ebr163.ibikg.util.StringUtils;

import java.util.List;
import java.util.Map;

/**
 * Created by mac1 on 19.04.16.
 */
public class UpdateShareFragment extends ShareFragment {

    public static UpdateShareFragment newInstance(AdDesc adDesc) {
        Bundle args = new Bundle();
        args.putSerializable(C.fields.data, adDesc);
        UpdateShareFragment shareFragment = new UpdateShareFragment();
        shareFragment.setArguments(args);
        return shareFragment;
    }

    @Override
    public void startActivity() {
        Intent intent = new Intent();
        getActivity().setResult(Activity.RESULT_OK, intent);
        getActivity().onBackPressed();
    }

    @Override
    public void setCustomFields(List<Object> customFields) {
        super.setCustomFields(customFields);
        for (Map customFieldData: adDesc.advert_custom_values){
            String customFieldKey = null;
            for (Object map: customFields){
                if (((Map) map).get(C.custom_fields.custom_name).equals(customFieldData.get(C.custom_fields.custom_name))) {
                    customFieldKey = String.valueOf(((Map)map).get(C.custom_fields.id));
                }
            }
            View view = this.customFields.get(customFieldKey);
            if (view instanceof TextInputLayout){
                ((TextInputLayout) view).getEditText().setText(customFieldData.get(C.custom_fields.custom_value).toString());
            } else if (view instanceof Spinner) {
                for (int i = 0; i < ((Spinner) view).getAdapter().getCount() ; i++) {
                    if (((Spinner) view).getAdapter().getItem(i).equals(customFieldData.get(C.custom_fields.custom_value))){
                        ((Spinner) view).setSelection(i);
                        break;
                    }
                }
            } else if (view instanceof CheckBox){
                if ("Да".equals(customFieldData.get(C.custom_fields.custom_value))){
                    ((CheckBox) view).setChecked(true);
                }
            }
        }
    }
}

package com.ebr163.ibikg;

import android.app.Application;

import com.ebr163.ibikg.di.AppComponent;
import com.ebr163.ibikg.di.DaggerAppComponent;
import com.ebr163.ibikg.di.module.AppModule;
import com.ebr163.ibikg.di.module.CRUDModule;
import com.ebr163.ibikg.di.module.HttpModule;
import com.ebr163.ibikg.service.database.CRUDService;
import com.ebr163.ibikg.service.http.HttpService;

import javax.inject.Inject;

/**
 * Created by Bakht on 11.03.2016.
 */
public class BaseApplication extends Application {

    public static BaseApplication instance;

    @Inject
    public CRUDService crudService;
    @Inject
    public HttpService httpService;

    private AppComponent appComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        appComponent = DaggerAppComponent.builder()
                .appModule(new AppModule(this))
                .cRUDModule(new CRUDModule())
                .httpModule(new HttpModule())
                .build();
        appComponent.inject(this);
    }
}

package com.ebr163.ibikg.activity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.ebr163.ibikg.BaseApplication;
import com.ebr163.ibikg.C;
import com.ebr163.ibikg.R;
import com.ebr163.ibikg.activity.base.BaseActivityWithFragment;
import com.ebr163.ibikg.fragment.HistoryOperationFragment;
import com.ebr163.ibikg.fragment.base.BaseFragmentWithList;
import com.ebr163.ibikg.fragment.base.BaseFragmentWithListView;
import com.ebr163.ibikg.model.ResponseList;
import com.ebr163.ibikg.service.SettingsService;
import com.ebr163.ibikg.service.http.HttpApiFactory;

import java.util.HashMap;
import java.util.Map;

import rx.functions.Action1;
import rx.functions.Func1;

public class HistoryOperationActivity extends BaseActivityWithFragment implements BaseFragmentWithList.OnSetObjectsListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.activity_with_fragment);
        super.onCreate(savedInstanceState);
        setToolbarTitle("История операций");
    }

    @Override
    protected void initFragment() {
        fragment = HistoryOperationFragment.newInstance();
        getSupportFragmentManager().beginTransaction()
                .add(R.id.main_fragment_container, fragment)
                .commit();
    }

    @Override
    public void setObjects(Object params) {
        progress.show();
        Action1<ResponseList<Object>> onNextAction = new Action1<ResponseList<Object>>() {
            @Override
            public void call(ResponseList<Object> response) {
                if (response != null) {
                    if (response.success) {
                        progress.hide();
                        if (response.data.size() == 0){
                            ((BaseFragmentWithListView) fragment).setVisibleProgress(View.GONE);
                        }
                    } else {
                        ((BaseFragmentWithListView) fragment).setVisibleErrorMessage(View.VISIBLE);
                        ((BaseFragmentWithListView) fragment).setVisibleProgress(View.GONE);
                        Toast.makeText(HistoryOperationActivity.this, "Ошибка", Toast.LENGTH_LONG).show();
                        Log.d("История операций", "Сервер вернул ошибку");
                    }
                }
            }
        };

        Map<String, String> map = new HashMap<>();
        map.put(C.fields.key, SettingsService.getUser(this).key);
        map.put(C.fields.limit, "100");
        map.put(C.fields.offset, "0");
        BaseApplication.instance.httpService.subscribeAction(HttpApiFactory.HttpMethodType.GET_USER_HISTORY, onNextAction, getErrorFunc(), map);
    }

    private Func1 getErrorFunc() {
        return new Func1() {
            @Override
            public Object call(Object o) {
                progress.hide();
                Toast.makeText(HistoryOperationActivity.this, "Ошибка. Проверьте подключение к интернету", Toast.LENGTH_LONG).show();
                Log.d("История операций", "Провал");
                return null;
            }
        };
    }

    @Override
    protected void onStop() {
        super.onStop();
        BaseApplication.instance.httpService.unsubscribe();
    }
}

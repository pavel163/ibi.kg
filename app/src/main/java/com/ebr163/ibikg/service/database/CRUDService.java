package com.ebr163.ibikg.service.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import java.util.List;

import static nl.qbusict.cupboard.CupboardFactory.cupboard;

/**
 * Created by mac1 on 24.03.16.
 */
public class CRUDService {

    private SQLiteDatabase db;

    public CRUDService(Context context) {
        CupboardSQLiteOpenHelper cupboardSQLiteOpenHelper = new CupboardSQLiteOpenHelper(context);
        db = cupboardSQLiteOpenHelper.getWritableDatabase();
    }

    public long put(Object object){
        return cupboard().withDatabase(db).put(object);
    }

    public List<Object> getAds(Class clazz){
        return cupboard().withDatabase(db).query(clazz).list();
    }

    public Object getAd(Class clazz, String id){
        return cupboard().withDatabase(db).query(clazz).withSelection("id = ?", id).get();
    }

    public void deleteSubscribe(Class clazz, String id){
        cupboard().withDatabase(db).delete(clazz, "id = ?", id);
    }

    public void deleteAll(Class clazz){
        cupboard().withDatabase(db).delete(clazz, null);
    }
}

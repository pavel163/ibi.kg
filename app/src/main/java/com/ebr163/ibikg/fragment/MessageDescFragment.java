package com.ebr163.ibikg.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.ebr163.ibikg.C;
import com.ebr163.ibikg.R;
import com.ebr163.ibikg.activity.MessageActivity;
import com.ebr163.ibikg.activity.ShareActivity;
import com.ebr163.ibikg.fragment.base.BaseFragment;
import com.ebr163.ibikg.model.ResponseMsgDesc;
import com.ebr163.ibikg.ui.SmartyTextView;
import com.ebr163.ibikg.util.DataUtil;

/**
 * Created by Bakht on 03.04.2016.
 */
public class MessageDescFragment extends BaseFragment {

    private SmartyTextView title;
    private SmartyTextView body;
    private SmartyTextView dataTime;
    private SmartyTextView from;
    private ResponseMsgDesc<String> message;
    private OnSetObjectsListener objectsListener;

    public interface OnSetObjectsListener {
        void setObjects(String id);
    }

    public static MessageDescFragment newInstance() {
        return new MessageDescFragment();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        objectsListener = (OnSetObjectsListener) context;
    }

    @Override
    public int getLayout() {
        return R.layout.fragment_message_desc;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_message, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.answer) {
            if (!message.reply_is_blocked) {
                Intent intent = new Intent(getActivity(), MessageActivity.class);
                intent.putExtra(C.advert.ibi_name, message.data.get(C.message.from_name));
                intent.putExtra(C.advert.user_id, message.data.get(C.message.from_user_id));
                startActivity(intent);
            } else {
                Toast.makeText(getActivity(), from.getText().toString() + " добавил вас в черный список", Toast.LENGTH_LONG).show();
            }
        }
        if (item.getItemId() == R.id.add_black_list) {
            objectsListener.setObjects(message.data.get(C.message.from_user_id));
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void initUI(View view) {
        title = (SmartyTextView) view.findViewById(R.id.title);
        from = (SmartyTextView) view.findViewById(R.id.from_user);
        body = (SmartyTextView) view.findViewById(R.id.description);
        dataTime = (SmartyTextView) view.findViewById(R.id.data);
    }

    public void setData(Object data) {
        message = (ResponseMsgDesc<String>) data;
        title.setText(message.data.get(C.message.mail_title));
        body.setText(message.data.get(C.message.mail_body));

        String[] d = message.data.get(C.message.mail_date).toString().split(" ");
        dataTime.setText(DataUtil.getFormatData(d[0]) + " " + d[1]);

        from.setText(message.data.get(C.message.from_name));
    }
}

package com.ebr163.ibikg.fragment;

import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

import com.ebr163.ibikg.C;
import com.ebr163.ibikg.R;
import com.ebr163.ibikg.activity.ShopActivity;
import com.ebr163.ibikg.adapter.ShopsAdapter;
import com.ebr163.ibikg.fragment.base.BaseFragmentWithRecyclerView;
import com.ebr163.support.ItemClickSupport;

import java.util.List;
import java.util.Map;

public class ShopsFragment extends BaseFragmentWithRecyclerView {

    @Override
    protected void initUI(View view) {
        super.initUI(view);
        setVisibleProgress(View.VISIBLE);
        ((ViewGroup) view).removeView(view.findViewById(R.id.fab));
        ItemClickSupport.addTo(recyclerView).setOnItemClickListener(new ItemClickSupport.OnItemClickListener() {
            @Override
            public void onItemClicked(RecyclerView recyclerView, int i, View view) {
                Intent intent = new Intent(getContext(), ShopActivity.class);
                intent.putExtra(C.fields.parent_id, ((Map) objectList.get(i)).get("id").toString());
                startActivity(intent);
            }
        });
    }

    @Override
    protected RecyclerView.Adapter<RecyclerView.ViewHolder> getAdapter(List<Object> objectList) {
        return new ShopsAdapter(objectList);
    }
}

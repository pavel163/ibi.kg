package com.ebr163.ibikg.activity;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.ebr163.ibikg.BaseApplication;
import com.ebr163.ibikg.C;
import com.ebr163.ibikg.R;
import com.ebr163.ibikg.activity.base.BaseActivity;
import com.ebr163.ibikg.model.Response;
import com.ebr163.ibikg.model.post.Message;
import com.ebr163.ibikg.service.SettingsService;
import com.ebr163.ibikg.service.http.HttpApiFactory;

import rx.functions.Action1;
import rx.functions.Func1;

public class MessageActivity extends BaseActivity implements View.OnClickListener{

    private TextInputLayout title;
    private EditText body;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.activity_message);
        super.onCreate(savedInstanceState);
        setToolbarTitle(getIntent().getStringExtra(C.advert.ibi_name));
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Отправка");
    }

    @Override
    protected void initUI() {
        title = (TextInputLayout) findViewById(R.id.title);
        body = (EditText) findViewById(R.id.body);
    }

    @Override
    public void onClick(View v) {
        progressDialog.show();
        Message message = new Message();
        message.key = SettingsService.getUser(this).key;
        message.mail_body = body.getText().toString();
        message.mail_title = title.getEditText().getText().toString();
        message.to_user_id = getIntent().getStringExtra(C.advert.user_id);
        message.reply_to = "";

        Action1<Response<Object>> onNextAction = new Action1<Response<Object>>() {
            @Override
            public void call(Response<Object> response) {
                progressDialog.hide();
                if (response.success == true) {
                    Toast.makeText(MessageActivity.this, "Сообщение отправлено", Toast.LENGTH_LONG).show();
                    Log.d("Отправка сообщения", "Успех");
                }
            }
        };

        Func1 error = new Func1() {
            @Override
            public Object call(Object o) {
                progressDialog.hide();
                Toast.makeText(MessageActivity.this, "Ошибка. Сообщение не отправлено", Toast.LENGTH_LONG).show();
                Log.d("Отправка сообщения", "Провал");
                return null;
            }
        };

        BaseApplication.instance.httpService.subscribeAction(HttpApiFactory.HttpMethodType.SEND_MESSAGE, onNextAction, error, message);
    }

    @Override
    protected void onStop() {
        super.onStop();
        BaseApplication.instance.httpService.unsubscribe();
    }
}

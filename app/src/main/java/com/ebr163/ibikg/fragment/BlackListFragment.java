package com.ebr163.ibikg.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.widget.BaseAdapter;

import com.ebr163.ibikg.C;
import com.ebr163.ibikg.adapter.BlackListAdapter;
import com.ebr163.ibikg.event.BlackListEvent;
import com.ebr163.ibikg.fragment.base.BaseFragmentWithListView;
import com.ebr163.ibikg.fragment.dialog.CurrencyDialog;
import com.ebr163.ibikg.fragment.dialog.MessageDialog;
import com.ebr163.ibikg.service.SettingsService;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.greenrobot.event.EventBus;

/**
 * Created by Bakht on 04.04.2016.
 */
public class BlackListFragment extends BaseFragmentWithListView {

    public static BlackListFragment newInstance() {
        return new BlackListFragment();
    }

    @Override
    protected BaseAdapter getAdapter(List<Object> objectList) {
        return new BlackListAdapter(getActivity(), objectList);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EventBus.getDefault().register(this);
        setRetainInstance(true);
    }

    @Override
    protected void clickItem(int position) {
        DialogFragment dialog = new MessageDialog();
        Bundle args = new Bundle();
        args.putString(C.fields.id, ((Map) objectList.get(position)).get(C.advert.user_id).toString());
        args.putInt("position", position);
        dialog.setArguments(args);
        dialog.show(getFragmentManager(), dialog.getClass().getName());
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    public void onEvent(BlackListEvent event){
        objectList.remove(event.position);
    }
}

package com.ebr163.ibikg.fragment;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.View;
import android.widget.TextView;

import com.ebr163.ibikg.R;
import com.ebr163.ibikg.fragment.base.BaseFragment;
import com.ebr163.ibikg.ui.SmartyImageView;

import java.util.Map;

public class ShopFragment extends BaseFragment implements View.OnClickListener {

    private View rootView;
    protected OnSetObjectsListener objectsListener;
    private Map data;

    public interface OnSetObjectsListener {
        void setObjects();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        objectsListener = (OnSetObjectsListener) context;
    }

    @Override
    public int getLayout() {
        return R.layout.fragment_shop;
    }

    @Override
    protected void initUI(View view) {
        this.rootView = view;
        objectsListener.setObjects();
    }

    public void setData(Map data) {
        this.data = data;

        rootView.findViewById(R.id.progress).setVisibility(View.GONE);
        rootView.findViewById(R.id.content).setVisibility(View.VISIBLE);

        ((SmartyImageView) rootView.findViewById(R.id.image)).setImage("http:" + data.get("shop_shoplogo"));
        ((TextView) rootView.findViewById(R.id.title)).setText(data.get("shop_name").toString());
        ((TextView) rootView.findViewById(R.id.city)).setText(data.get("shop_city_id").toString());
        ((TextView) rootView.findViewById(R.id.address)).setText(data.get("shop_address").toString());
        ((TextView) rootView.findViewById(R.id.kind)).setText(data.get("shop_activity_kind").toString());
        ((TextView) rootView.findViewById(R.id.description)).setText(data.get("shop_about").toString());
        ((TextView) rootView.findViewById(R.id.description)).setText(data.get("shop_about").toString());
        ((TextView) rootView.findViewById(R.id.contact)).setText(data.get("ibi_name").toString());
        ((TextView) rootView.findViewById(R.id.work_time)).setText(data.get("shop_worktime").toString());
        ((TextView) rootView.findViewById(R.id.phone)).setText(data.get("shop_phone").toString());
        rootView.findViewById(R.id.phone).setOnClickListener(this);
        ((TextView) rootView.findViewById(R.id.mobile)).setText(data.get("shop_phone_mobile").toString());
        rootView.findViewById(R.id.mobile).setOnClickListener(this);
        ((TextView) rootView.findViewById(R.id.mail)).setText(data.get("shop_email").toString());
        rootView.findViewById(R.id.mail).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.phone:
                Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + data.get("shop_phone").toString()));
                startActivity(intent);
                break;
            case R.id.mobile:
                Intent intent1 = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + data.get("shop_phone_mobile").toString()));
                startActivity(intent1);
                break;
        }
    }
}

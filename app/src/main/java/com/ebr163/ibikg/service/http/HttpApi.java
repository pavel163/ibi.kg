package com.ebr163.ibikg.service.http;

import com.ebr163.ibikg.model.Category;
import com.ebr163.ibikg.model.MessageList;
import com.ebr163.ibikg.model.Response;
import com.ebr163.ibikg.model.ResponseContact;
import com.ebr163.ibikg.model.ResponseList;
import com.ebr163.ibikg.model.ResponseMsgDesc;
import com.ebr163.ibikg.model.Shop;
import com.ebr163.ibikg.model.ads.AdDesc;
import com.ebr163.ibikg.model.ads.Ads;

import java.util.Map;

import retrofit2.http.Field;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Query;
import retrofit2.http.QueryMap;
import rx.Observable;

/**
 * Created by Bakht on 11.03.2016.
 */
public interface HttpApi {

    @GET("/index.php/api/?app=ibi&format=raw&resource=get_categories")
    Observable<Response<Category>> getCategory(@Query("parent_id") String id);

    @GET("/index.php/api/?app=ibi&format=raw&resource=registration")
    Observable<Response<Object>> registerUser(@Query("user_name") String name, @Query("user_email") String email,
                                              @Query("user_phone") String phone, @Query("password") String password);

    @GET("/index.php/api/?app=ibi&format=raw&resource=authorization")
    Observable<Response<Object>> authorization(@Query("user_email") String email, @Query("user_password") String password);

    @GET("/index.php/api/?app=ibi&format=raw&resource=get_adverts")
    Observable<Ads> getAds(@Query("cat_id") String id, @Query("region_id") String region, @Query("city_id") String city,
                           @Query("limit") String limit, @Query("offset") String offset, @Query("custom_filter") String filter,
                           @QueryMap Map<String, String> filters);

    @GET("/index.php/api/?app=ibi&format=raw&resource=get_advert")
    Observable<Response<Object>> getAd(@Query("advert_id") String id);

    @GET("/index.php/api/?app=ibi&format=raw&resource=get_currencies")
    Observable<ResponseList<Object>> getCurrency();

    @GET("/index.php/api/?app=ibi&format=raw&resource=get_regions_cities")
    Observable<ResponseList<Object>> getRegionsAndCities();

    @GET("/index.php/api/?app=ibi&format=raw&resource=get_paid_services")
    Observable<ResponseList<Object>> getPaidServices();

    @Headers({"Content-Type: application/json", "charset: UTF-8"})
    @POST("/index.php/api/?app=ibi&format=raw&resource=send_mail")
    Observable<Response<String>> sendMessage(@Query("key") String key, @Query("to_user_id") String userId, @Query("mail_title") String title,
                                             @Query("mail_body") String body);

    @GET("/index.php/api/?app=ibi&format=raw&resource=get_regions")
    Observable<ResponseList<Object>> getRegions();

    @GET("/index.php/api/?app=ibi&format=raw&resource=get_cities")
    Observable<ResponseList<Object>> getCities(@Query("region_id") String region);

    @FormUrlEncoded
    @POST("/index.php/api/?app=ibi&format=raw&resource=add_user_advert")
    Observable<Response<Object>> addUserAdvert(@Query("key") String key, @Field("category_id") String categoryId, @Field("city_id") String cityId,
                                               @Field("advert_name") String advertName, @Field("address") String address, @Field("advert_descr") String advertDescr,
                                               @Field("currency_id") String currencyId, @Field("is_negotiable") String isNegotiable,
                                               @Field("price") String price, @Field("slot-1") String slot1, @Field("slot-2") String slot2,
                                               @Field("slot-3") String slot3, @Field("slot-4") String slot4, @Field("slot-5") String slot5,
                                               @FieldMap Map<String, String> fields);

    @GET("/index.php/api/?app=ibi&format=raw&resource=get_user_adverts")
    Observable<ResponseList<Map<String, AdDesc>>> getUserAdverts(@Query("key") String key, @Query("published") String published, @Query("offset") String offset,
                                                                 @Query("limit") String limit);

    @Headers({"Content-Type: application/json", "charset: UTF-8"})
    @POST("/index.php/api/?app=ibi&format=raw&resource=publish_user_advert")
    Observable<Response<String>> publishUserAdvert(@Query("key") String key, @Query("publish") String publish, @Query("advert_id") String advertId);

    @Headers({"Content-Type: application/json", "charset: UTF-8"})
    @POST("/index.php/api/?app=ibi&format=raw&resource=delete_user_advert")
    Observable<Response<String>> deleteUserAdvert(@Query("key") String key, @Query("advert_id") String advertId);

    @FormUrlEncoded
    @POST("/index.php/api/?app=ibi&format=raw&resource=save_user_advert")
    Observable<Response<Object>> saveUserAdvert(@Query("key") String key, @Field("category_id") String categoryId, @Field("city_id") String cityId,
                                                @Field("advert_name") String advertName, @Field("address") String address, @Field("advert_descr") String advertDescr, @Field("currency_id") String currencyId, @Field("is_negotiable") String isNegotiable,
                                                @Field("price") String price, @Field("slot-1") String slot1, @Field("slot-2") String slot2,
                                                @Field("slot-3") String slot3, @Field("slot-4") String slot4, @Field("slot-5") String slot5, @Query("advert_id") String advertId,
                                                @FieldMap Map<String, String> fields);

    @GET("/index.php/api/?app=ibi&format=raw&resource=get_user_mail")
    Observable<MessageList<Object>> getUserMail(@Query("key") String key, @Query("tab") String tab, @Query("offset") String offset,
                                                @Query("limit") String limit);

    @GET("/index.php/api/?app=ibi&format=raw&resource=get_user_mail_by_id")
    Observable<ResponseMsgDesc<Object>> getUserMailById(@Query("key") String key, @Query("id") String id);

    @GET("/index.php/api/?app=ibi&format=raw&resource=get_user_blacklist")
    Observable<ResponseList<Object>> getUserBlackList(@Query("key") String key);

    @GET("/index.php/api/?app=ibi&format=raw&resource=add_to_blacklist")
    Observable<Response<String>> addToBlackList(@Query("key") String key, @Query("id") String contactId);

    @GET("/index.php/api/?app=ibi&format=raw&resource=get_contact")
    Observable<ResponseContact> getContact(@Query("key") String key);

    @GET("/index.php/api/?app=ibi&format=raw&resource=get_user_balance")
    Observable<Response<Object>> getUserBalance(@Query("key") String key);

    @POST("/index.php/api/?app=ibi&format=raw&resource=update_pass")
    Observable<Map<String, Object>> updatePass(@Query("key") String key, @Query("old_pass") String oldPass, @Query("new_pass") String newPass);

    @POST("/index.php/api/?app=ibi&format=raw&resource=delete_from_blacklist")
    Observable<Map<String, String>> deleteFromBlacklist(@Query("key") String key, @Query("id") String id);

    @FormUrlEncoded
    @POST("/index.php/api/?app=ibi&format=raw&resource=save_contact")
    Observable<Map<String, String>> saveContact(@Query("key") String key, @Query("ibi_name") String name, @Query("phone") String phone,
                                                @Query("subscribe") String subscribe, @Query("notification") String notification, @Query("remind") String remind,
                                                @Query("shop_enabled") String shopEnabled, @Query("shop_name") String shopName, @Query("shop_phone") String shopPhone,
                                                @Query("shop_email") String shopEmail, @Query("shop_city_id") String shopCityId, @Query("shop_address") String shopAddress,
                                                @Query("shop_about") String shopAbout, @Field("banner_data") String bannerData);

    @POST("/index.php/api/?app=ibi&format=raw&resource=pay_service")
    Observable<Map<String, Object>> payService(@Query("key") String key, @Query("service_id") String serviceId, @Query("advert_id") String advertId);

    @GET("/index.php/api/?app=ibi&format=raw&resource=get_user_balance_history")
    Observable<ResponseList<Map<String, String>>> getUserBalanceHistory( @Query("key") String key, @Query("offset") String offset, @Query("limit") String limit);

    @GET("/index.php/api/?app=ibi&format=raw&resource=get_custom_fields")
    Observable<ResponseList<Object>> getCustomFields(@Query("category_id") String categoryId);

    @GET("/index.php/api/?app=ibi&format=raw&resource=get_shops")
    Observable<ResponseList<Shop>> getShops();

    @GET("/index.php/api/?app=ibi&format=raw&resource=get_shops")
    Observable<Object> getShop(@Query("shop_id") String shopId);
}

package com.ebr163.ibikg.fragment.base;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.ebr163.ibikg.R;

import java.util.List;

/**
 * Created by Bakht on 21.03.2016.
 */
public abstract class BaseFragmentWithList extends BaseFragment implements View.OnClickListener {

    protected ProgressBar progressBar;
    protected List<Object> objectList;
    protected OnSetObjectsListener objectsListener;
    protected LinearLayout error;
    protected Button update;

    public interface OnSetObjectsListener {
        void setObjects(Object params);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        objectsListener = (OnSetObjectsListener) context;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);
        return view;
    }

    @Override
    protected void initUI(View view) {
        progressBar = (ProgressBar) view.findViewById(R.id.progress);
        error = (LinearLayout) view.findViewById(R.id.error_message);
        update = (Button) view.findViewById(R.id.update);
        update.setOnClickListener(this);
    }

    public void setVisibleProgress(int visibility) {
        progressBar.setVisibility(visibility);
    }

    public void setVisibleErrorMessage(int visibility) {
        error.setVisibility(visibility);
    }

    public void setObjectList(List<Object> objectList) {
        if (this.objectList == null) {
            this.objectList = objectList;
        }
    }

    public void addObjectList(List<Object> objectList){
        for (Object obj : objectList) {
            this.objectList.add(obj);
        }
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.update) {
            update();
        }
    }

    protected void update() {
        setVisibleProgress(View.VISIBLE);
        setVisibleErrorMessage(View.GONE);
        objectsListener.setObjects(null);
    }

    public void clearData(){
        objectList.clear();
        objectList = null;
        setVisibleProgress(View.VISIBLE);
    }
}

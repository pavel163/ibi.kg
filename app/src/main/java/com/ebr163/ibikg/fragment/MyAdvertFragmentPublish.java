package com.ebr163.ibikg.fragment;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

import com.ebr163.ibikg.adapter.category.MyAdAdapter;
import com.ebr163.ibikg.event.OffsetEvent;

import java.util.List;

import de.greenrobot.event.EventBus;

/**
 * Created by Bakht on 03.04.2016.
 */
public class MyAdvertFragmentPublish extends AdFragment {

    public static MyAdvertFragmentPublish newInstance() {
        return new MyAdvertFragmentPublish();
    }

    @Override
    protected void initUI(View view) {
        super.initUI(view);
        ((ViewGroup)view).removeView(fab);
    }

    @Override
    protected RecyclerView.Adapter<RecyclerView.ViewHolder> getAdapter(List<Object> objectList) {
        return new MyAdAdapter(objectList);
    }

    @Override
    protected void checkData() {
        if (objectList == null){
            objectsListener.setObjects(1);
        } else {
            setupRecyclerView(objectList);
        }
    }

    @Override
    protected void postOffsetEvent(int page) {
        OffsetEvent event = new OffsetEvent(page);
        event.type = "my_advert_publish";
        EventBus.getDefault().post(event);
    }
}

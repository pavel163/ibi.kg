package com.ebr163.ibikg.event;

import com.ebr163.ibikg.event.base.BaseEvent;

/**
 * Created by Bakht on 17.03.2016.
 */
public class LoginEvent extends BaseEvent {

    public final String name;
    public final String email;
    public final String phone;
    public final String password;

    public LoginEvent(String name, String email, String phone, String password) {
        this.name = name;
        this.email = email;
        this.phone = phone;
        this.password = password;
    }

    public LoginEvent(String email, String password) {
        this.name = "";
        this.email = email;
        this.phone = "";
        this.password = password;
    }
}

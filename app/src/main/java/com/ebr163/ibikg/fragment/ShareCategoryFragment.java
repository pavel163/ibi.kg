package com.ebr163.ibikg.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.widget.BaseAdapter;

import com.ebr163.ibikg.C;
import com.ebr163.ibikg.activity.ShareActivity;
import com.ebr163.ibikg.activity.ShareCategoryActivity;
import com.ebr163.ibikg.adapter.ShareCategoryAdapter;
import com.ebr163.ibikg.model.Category;

import java.util.List;

/**
 * Created by Bakht on 19.03.2016.
 */
public class ShareCategoryFragment extends CategoryFragment {

    public static ShareCategoryFragment newInstance() {
        return new ShareCategoryFragment();
    }

    public static ShareCategoryFragment newInstance(String id, int step) {
        Bundle args = new Bundle();
        args.putString(C.fields.parent_id, id);
        args.putInt(C.fields.step, step);
        ShareCategoryFragment shareFragment = new ShareCategoryFragment();
        shareFragment.setArguments(args);
        return shareFragment;
    }

    //TODO убрать дубляж
    @Override
    protected void clickItem(int position) {
        Intent intent = null;
        if ("1".equals(((Category) objectList.get(position)).childs)) {
            intent = new Intent(getActivity(), ShareCategoryActivity.class);
        } else {
            intent = new Intent(getActivity(), ShareActivity.class);
        }
        intent.putExtra(C.fields.parent_id, ((Category) objectList.get(position)).id);
        intent.putExtra(C.fields.step, getArguments().getInt(C.fields.step) + 1);
        intent.putExtra(C.fields.title, ((Category) objectList.get(position)).name_category);
        startActivity(intent);
    }

    @Override
    protected BaseAdapter getAdapter(List<Object> objectList) {
        return new ShareCategoryAdapter(getActivity(), objectList);
    }
}

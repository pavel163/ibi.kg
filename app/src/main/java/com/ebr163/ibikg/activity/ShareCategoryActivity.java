package com.ebr163.ibikg.activity;

import android.os.Bundle;

import com.ebr163.ibikg.C;
import com.ebr163.ibikg.R;
import com.ebr163.ibikg.fragment.ShareCategoryFragment;

public class ShareCategoryActivity extends CategoryActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setToolbarSubTitle("Шаг " + getIntent().getIntExtra(C.fields.step, 1));
    }

    @Override
    protected void initFragment() {
        fragment = ShareCategoryFragment.newInstance(getIntent().getStringExtra(C.fields.parent_id), getIntent().getIntExtra(C.fields.step, 1));
        getSupportFragmentManager().beginTransaction()
                .add(R.id.main_fragment_container, fragment)
                .commit();
    }
}

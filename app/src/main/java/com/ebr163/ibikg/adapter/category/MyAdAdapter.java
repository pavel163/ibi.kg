package com.ebr163.ibikg.adapter.category;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ebr163.ibikg.R;
import com.ebr163.ibikg.model.ads.Ad;

import java.util.List;

/**
 * Created by Bakht on 03.04.2016.
 */
public class MyAdAdapter extends AdAdapter {

    public MyAdAdapter(List<Object> ads) {
        super(ads);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_my_ad, parent, false);
        return MyAdHolder.newInstance(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        AdHolder holder = (MyAdHolder) viewHolder;
        setData(holder, position);
    }

    @Override
    protected void setData(AdHolder holder, int position) {
        super.setData(holder, position);
        Ad ad = (Ad) ads.get(position);
        ((MyAdHolder)holder).setPublish(ad.published);
    }
}

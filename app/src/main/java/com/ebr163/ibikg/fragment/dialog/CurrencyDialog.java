package com.ebr163.ibikg.fragment.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;

import com.ebr163.ibikg.C;

/**
 * Created by Bakht on 26.03.2016.
 */
public class CurrencyDialog extends DialogFragment implements DialogInterface.OnClickListener {

    String data [];
    String currency;

    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        data = getArguments().getStringArray(C.util.data);
        builder.setTitle("Валюта")
                .setSingleChoiceItems(data, -1, this)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent();
                        intent.putExtra(C.fields.data, currency);
                        getTargetFragment().onActivityResult(getTargetRequestCode(), Activity.RESULT_OK, intent);
                    }
                });
        return builder.create();
    }

    public void onClick(DialogInterface dialog, int which) {
        currency = data[which];
    }

    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
        Log.d("dialog", "Dialog 2: onDismiss");
    }

    public void onCancel(DialogInterface dialog) {
        super.onCancel(dialog);
        Log.d("dialog", "Dialog 2: onCancel");
    }
}

package com.ebr163.ibikg.service;

import android.content.Context;
import android.content.SharedPreferences;

import com.ebr163.ibikg.C;
import com.ebr163.ibikg.model.User;

/**
 * Created by Bakht on 18.03.2016.
 */
public class SettingsService {

    public static void saveUser(Context context, User user){
        SharedPreferences preferences = context.getSharedPreferences(C.util.settings, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();

        editor.putString(C.login.name, user.name);
        editor.putString(C.login.email, user.email);
        editor.putString(C.login.phone, user.phone);
        editor.putString(C.login.password, user.password);
        editor.putString(C.login.id, user.id);
        editor.putString(C.login.user_key, user.key);
        editor.apply();
    }

    public static User getUser(Context context){
        SharedPreferences preferences = context.getSharedPreferences(C.util.settings, Context.MODE_PRIVATE);
        User user = new User(preferences.getString(C.login.name, ""), preferences.getString(C.login.email, ""), preferences.getString(C.login.phone, ""),
                preferences.getString(C.login.password, ""), preferences.getString(C.login.id, ""), preferences.getString(C.login.user_key, ""));
        return user;
    }

    public static String getUserEmail(Context context){
        SharedPreferences preferences = context.getSharedPreferences(C.util.settings, Context.MODE_PRIVATE);
        return preferences.getString(C.login.email, "");
    }

}

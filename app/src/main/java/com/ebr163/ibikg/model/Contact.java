package com.ebr163.ibikg.model;

import java.io.Serializable;

/**
 * Created by Bakht on 04.04.2016.
 */
public class Contact implements Serializable{
    public String id;
    public String user_id;
    public String ibi_name;
    public String phone;
    public String subscribe;
    public String notification;
    public String remind;
    public String shop_enabled;
    public String shop_name;
    public String shop_phone;
    public String shop_email;
    public String shop_city_id;
    public String shop_about;
    public String shop_address;
    public String shop_banner;
    public String key;
}

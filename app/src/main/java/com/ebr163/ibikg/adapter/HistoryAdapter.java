package com.ebr163.ibikg.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.ebr163.ibikg.C;
import com.ebr163.ibikg.R;
import com.ebr163.ibikg.ui.SmartyTextView;

import java.util.List;
import java.util.Map;

/**
 * Created by Bakht on 17.04.2016.
 */
public class HistoryAdapter extends BaseAdapter {

    protected List<Object> histories;
    protected LayoutInflater lInflater;
    protected Context context;

    public HistoryAdapter(Context context, List<Object> objectList) {
        this.histories = objectList;
        this.context = context;
        lInflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return histories.size();
    }

    @Override
    public Object getItem(int position) {
        return histories.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View rowView = convertView;
        if (rowView == null) {
            rowView = lInflater.inflate(R.layout.item_history, null);
            ViewHolder viewHolder = new ViewHolder();
            viewHolder.method = (SmartyTextView) rowView.findViewById(R.id.method);
            viewHolder.data = (SmartyTextView) rowView.findViewById(R.id.date_balance);
            viewHolder.advert = (SmartyTextView) rowView.findViewById(R.id.advert);
            viewHolder.balance = (SmartyTextView) rowView.findViewById(R.id.balance);
            rowView.setTag(viewHolder);
        }

        ViewHolder holder = (ViewHolder) rowView.getTag();
        Map<String, Object> map = (Map<String, Object>) histories.get(position);
        holder.method.setText(map.get("name_method").toString());
        holder.data.setText(map.get("date_balance").toString());

        if (map.get("advert_name") != null){
            holder.advert.setText(map.get("advert_name").toString());
        }

        holder.balance.setText(map.get("balance").toString());
        return rowView;
    }

    static class ViewHolder {
        public SmartyTextView method;
        public SmartyTextView data;
        public SmartyTextView advert;
        public SmartyTextView balance;
    }
}

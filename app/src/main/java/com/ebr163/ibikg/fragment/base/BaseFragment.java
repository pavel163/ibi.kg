package com.ebr163.ibikg.fragment.base;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by Bakht on 10.03.2016.
 */
public abstract class BaseFragment extends Fragment {

    private int layout;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(layout, container, false);
        initUI(view);
        return view;
    }

    public abstract int getLayout();

    public void setLayout(int layout) {
        this.layout = layout;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        layout = getLayout();
    }

    protected abstract void initUI(View view);
}

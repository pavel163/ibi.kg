package com.ebr163.ibikg.fragment.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;

import com.ebr163.ibikg.C;
import com.ebr163.ibikg.model.PayEvent;

import de.greenrobot.event.EventBus;

/**
 * Created by Bakht on 19.04.2016.
 */
public class PayDialogFragment extends DialogFragment {

    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Покупка")
                .setMessage("Вы действительно хотите применить платную услугу к этому объявлению")
                .setNegativeButton("Отмена", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dismiss();
                    }
                })
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        EventBus.getDefault().post(new PayEvent(getArguments().getString(C.fields.id)));
                    }
                });
        return builder.create();
    }
}

package com.ebr163.ibikg.activity.login;

import android.app.ProgressDialog;
import android.os.Bundle;

import com.ebr163.ibikg.activity.base.BaseActivityWithFragment;

import de.greenrobot.event.EventBus;

/**
 * Created by Bakht on 18.03.2016.
 */
public abstract class LoginActivity extends BaseActivityWithFragment {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //TODO поменять цвет круга на синий
        progress.setMessage("Пожалуйста, подождите...");
    }

    @Override
    protected void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

}

package com.ebr163.ibikg.service.http;

import com.ebr163.ibikg.C;
import com.ebr163.ibikg.event.LoginEvent;
import com.ebr163.ibikg.model.AdPostData;
import com.ebr163.ibikg.model.AdvertsParams;
import com.ebr163.ibikg.model.Contact;
import com.ebr163.ibikg.model.post.Message;

import java.util.HashMap;

import javax.inject.Singleton;

import retrofit2.Retrofit;
import rx.Observable;

/**
 * Created by Bakht on 23.05.2016.
 */
@Singleton
public class HttpApiFactory {

    protected HttpApi httpApi;

    public HttpApiFactory(Retrofit retrofit) {
        httpApi = retrofit.create(HttpApi.class);
    }

    public enum HttpMethodType {
        GET_CATEGORY, REGISTER, AUTHORIZATION, GET_AD, GET_ADS,
        GET_CURRENCY, GET_REGIONS_AND_CITIES, GET_PAID_SERVICE, SEND_MESSAGE, ADD_USER_ADVERT,
        GET_REGIONS, GET_CITIES, GET_USER_ADVERTS, PUBLISH_USER_ADVERT, DELETE_USER_ADVERT,
        SAVE_USER_ADVERT, GET_USER_MAIL, GET_USER_MAIL_BY_ID, GET_USER_BLACK_LIST, ADD_TO_BLACK_LIST,
        GET_CONTACT, GET_USER_BALANCE, UPDATE_PASS, DELETE_FROM_BLACK_LIST, SAVE_CONTACT,
        PAY_SERVICE, GET_USER_HISTORY, GET_CUSTOM_FIELDS, GET_SHOPS, GET_SHOP
    }

    public Observable getHttpMethod(HttpMethodType type, Object query) {
        Observable observable = null;
        switch (type) {
            case GET_CATEGORY:
                observable = httpApi.getCategory((String) query);
                break;
            case REGISTER:
                LoginEvent regEvent = (LoginEvent) query;
                observable = httpApi.registerUser(regEvent.name, regEvent.email, regEvent.phone, regEvent.password);
                break;
            case AUTHORIZATION:
                LoginEvent authEvent = (LoginEvent) query;
                observable = httpApi.authorization(authEvent.email, authEvent.password);
                break;
            case GET_ADS:
                observable = httpApi.getAds(((AdvertsParams) query).id, ((AdvertsParams) query).region,
                        ((AdvertsParams) query).city, ((AdvertsParams) query).limit, ((AdvertsParams) query).offset, ((AdvertsParams) query).filter, ((AdvertsParams) query).filters);
                break;
            case GET_AD:
                observable = httpApi.getAd((String) query);
                break;
            case GET_CURRENCY:
                observable = httpApi.getCurrency();
                break;
            case GET_REGIONS_AND_CITIES:
                observable = httpApi.getRegionsAndCities();
                break;
            case GET_PAID_SERVICE:
                observable = httpApi.getPaidServices();
                break;
            case SEND_MESSAGE:
                Message message = (Message) query;
                observable = httpApi.sendMessage(message.key, message.to_user_id, message.mail_title, message.mail_body);
                break;
            case ADD_USER_ADVERT:
                observable = httpApi.addUserAdvert(((AdPostData) query).key, ((AdPostData) query).category_id, ((AdPostData) query).city_id,
                        ((AdPostData) query).advert_name, ((AdPostData) query).address, ((AdPostData) query).advert_descr,
                        ((AdPostData) query).currency_id, ((AdPostData) query).is_negotiable, ((AdPostData) query).price, ((AdPostData) query).slot1,
                        ((AdPostData) query).slot2, ((AdPostData) query).slot3, ((AdPostData) query).slot4, ((AdPostData) query).slot5,
                        ((AdPostData) query).customFields);
                break;
            case GET_REGIONS:
                observable = httpApi.getRegions();
                break;
            case GET_CITIES:
                observable = httpApi.getCities(String.valueOf(query));
                break;
            case GET_USER_ADVERTS:
                observable = httpApi.getUserAdverts((String) ((HashMap) query).get(C.fields.key),
                        (String) ((HashMap) query).get(C.advert.published), (String) ((HashMap) query).get(C.fields.offset),
                        (String) ((HashMap) query).get(C.fields.limit));
                break;
            case PUBLISH_USER_ADVERT:
                observable = httpApi.publishUserAdvert((String) ((HashMap) query).get(C.fields.key), (String) ((HashMap) query).get(C.advert.published),
                        (String) ((HashMap) query).get(C.advert.advert_id));
                break;
            case DELETE_USER_ADVERT:
                observable = httpApi.deleteUserAdvert((String) ((HashMap) query).get(C.fields.key), (String) ((HashMap) query).get(C.advert.advert_id));
                break;
            case SAVE_USER_ADVERT:
                observable = httpApi.saveUserAdvert(((AdPostData) query).key, ((AdPostData) query).category_id, ((AdPostData) query).city_id,
                        ((AdPostData) query).advert_name, ((AdPostData) query).address, ((AdPostData) query).advert_descr,
                        ((AdPostData) query).currency_id, ((AdPostData) query).is_negotiable, ((AdPostData) query).price,
                        ((AdPostData) query).slot1, ((AdPostData) query).slot2, ((AdPostData) query).slot3,
                        ((AdPostData) query).slot4, ((AdPostData) query).slot5, ((AdPostData) query).advert_id,
                        ((AdPostData) query).customFields);
                break;
            case GET_USER_MAIL:
                observable = httpApi.getUserMail((String) ((HashMap) query).get(C.fields.key),
                        (String) ((HashMap) query).get(C.fields.tab), (String) ((HashMap) query).get(C.fields.offset),
                        (String) ((HashMap) query).get(C.fields.limit));
                break;
            case GET_USER_MAIL_BY_ID:
                observable = httpApi.getUserMailById((String) ((HashMap) query).get(C.fields.key),
                        (String) ((HashMap) query).get(C.message.id));
                break;
            case GET_USER_BLACK_LIST:
                observable = httpApi.getUserBlackList((String) query);
                break;
            case ADD_TO_BLACK_LIST:
                observable = httpApi.addToBlackList((String) ((HashMap) query).get(C.fields.key), (String) ((HashMap) query).get(C.message.id));
                break;
            case GET_CONTACT:
                observable = httpApi.getContact((String) query);
                break;
            case GET_USER_BALANCE:
                observable = httpApi.getUserBalance((String) query);
                break;
            case UPDATE_PASS:
                observable = httpApi.updatePass((String) ((HashMap) query).get(C.fields.key), (String) ((HashMap) query).get(C.fields.old_pass),
                        (String) ((HashMap) query).get(C.fields.new_pass));
                break;
            case DELETE_FROM_BLACK_LIST:
                observable = httpApi.deleteFromBlacklist((String) ((HashMap) query).get(C.fields.key), (String) ((HashMap) query).get(C.fields.id));
                break;
            case SAVE_CONTACT:
                Contact contact = (Contact) query;
                observable = httpApi.saveContact(contact.key, contact.ibi_name, contact.phone, contact.subscribe, contact.notification, contact.remind,
                        contact.shop_enabled, contact.shop_name, contact.shop_phone, contact.shop_email, contact.shop_city_id, contact.shop_address, contact.shop_about,
                        contact.shop_banner);
                break;
            case PAY_SERVICE:
                observable = httpApi.payService(((HashMap) query).get(C.fields.key).toString(), ((HashMap) query).get(C.fields.service_id).toString(), ((HashMap) query).get(C.advert.advert_id).toString());
                break;
            case GET_USER_HISTORY:
                observable = httpApi.getUserBalanceHistory(((HashMap) query).get(C.fields.key).toString(), ((HashMap) query).get(C.fields.offset).toString(), ((HashMap) query).get(C.fields.limit).toString());
                break;
            case GET_CUSTOM_FIELDS:
                observable = httpApi.getCustomFields((String) query);
                break;
            case GET_SHOPS:
                observable = httpApi.getShops();
                break;
            case GET_SHOP:
                observable = httpApi.getShop((String) query);
                break;
        }
        return observable;
    }

}

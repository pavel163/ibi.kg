package com.ebr163.ibikg.activity;

import android.os.Bundle;
import android.util.Log;

import com.ebr163.ibikg.BaseApplication;
import com.ebr163.ibikg.C;
import com.ebr163.ibikg.R;
import com.ebr163.ibikg.activity.base.BaseActivityWithFragment;
import com.ebr163.ibikg.fragment.ShopFragment;
import com.ebr163.ibikg.service.http.HttpApiFactory;

import java.util.List;
import java.util.Map;

import rx.functions.Action1;
import rx.functions.Func1;

public class ShopActivity extends BaseActivityWithFragment implements ShopFragment.OnSetObjectsListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.activity_ad_desc);
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void initFragment() {
        fragment = new ShopFragment();
        getSupportFragmentManager().beginTransaction()
                .add(R.id.main_fragment_container, fragment)
                .commit();
    }

    @Override
    public void setObjects() {
        Action1<Object> onNextAction = new Action1<Object>() {
            @Override
            public void call(Object response) {
                if (((Map) response).get("success").equals(true)) {
                    ((ShopFragment) fragment).setData((Map) ((List) ((Map) response).get("data")).get(0));
                    Log.d("Загрузка магазина", "Успех");
                }
            }
        };

        Func1 error = new Func1() {
            @Override
            public Object call(Object o) {
                Log.d("Загрузка магазина", "Провал");
                return null;
            }
        };

        BaseApplication.instance.httpService.subscribeAction(HttpApiFactory.HttpMethodType.GET_SHOP, onNextAction, error, getIntent().getStringExtra(C.fields.parent_id));
    }

    @Override
    protected void onStop() {
        super.onStop();
        BaseApplication.instance.httpService.unsubscribe();
    }
}

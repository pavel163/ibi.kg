package com.ebr163.ibikg.model;

/**
 * Created by Bakht on 18.03.2016.
 */
public class SignUpResult {
    public int id;
    public String user_key;
    public Double error_code;
    public String error_description;
}

package com.ebr163.ibikg.fragment;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;
import android.widget.ImageView;

import com.ebr163.ibikg.C;
import com.ebr163.ibikg.R;
import com.ebr163.ibikg.fragment.base.BaseFragment;
import com.ebr163.ibikg.helper.ConvertHelper;
import com.ebr163.ibikg.model.Contact;
import com.ebr163.ibikg.ui.SmartyImageView;

import java.io.FileNotFoundException;
import java.io.IOException;

/**
 * Created by Bakht on 09.04.2016.
 */
public class ChangeMyInfoFragment extends BaseFragment {

    private TextInputLayout name;
    private TextInputLayout phone;
    private TextInputLayout shopName;
    private TextInputLayout shopPhone;
    private TextInputLayout shopEmail;
    private TextInputLayout shopAddress;
    private TextInputLayout shopAbout;

    private CheckBox remind;
    private CheckBox notification;
    private CheckBox subscribe;
    private CheckBox shopEnable;

    private SmartyImageView banner;

    private OnSetObjectsListener objectsListener;
    private Contact contact;

    public interface OnSetObjectsListener {
        void setObjects();
    }

    public static ChangeMyInfoFragment newInstance(Contact contact) {
        Bundle args = new Bundle();
        args.putSerializable("contact", contact);
        ChangeMyInfoFragment fragment = new ChangeMyInfoFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        objectsListener = (OnSetObjectsListener) context;
    }

    @Override
    public int getLayout() {
        return R.layout.fragment_change_my_info;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        contact = (Contact) getArguments().getSerializable("contact");
    }

    @Override
    protected void initUI(View view) {
        name = (TextInputLayout) view.findViewById(R.id.ibi_name);
        name.getEditText().setText(contact.ibi_name);

        phone = (TextInputLayout) view.findViewById(R.id.phone);
        phone.getEditText().setText(contact.phone);

        shopName = (TextInputLayout) view.findViewById(R.id.shop_name);
        if (contact.shop_name != null){
            shopName.getEditText().setText(contact.shop_name);
        }

        shopEmail = (TextInputLayout) view.findViewById(R.id.shop_email);
        if (contact.shop_email != null){
            shopEmail.getEditText().setText(contact.shop_email);
        }

        shopPhone = (TextInputLayout) view.findViewById(R.id.shop_phone);
        if (contact.shop_phone != null){
            shopPhone.getEditText().setText(contact.shop_phone);
        }

        shopAddress = (TextInputLayout) view.findViewById(R.id.shop_address);
        if (contact.shop_address != null){
            shopAddress.getEditText().setText(contact.shop_address);
        }

        shopAbout = (TextInputLayout) view.findViewById(R.id.shop_about);
        if (contact.shop_about != null){
            shopAbout.getEditText().setText(contact.shop_about);
        }

        subscribe = (CheckBox)view.findViewById(R.id.subscribe);
        if ("1".equals(contact.subscribe)){
            subscribe.setChecked(true);
        }

        subscribe = (CheckBox)view.findViewById(R.id.subscribe);
        if ("1".equals(contact.subscribe)){
            subscribe.setChecked(true);
        }

        remind = (CheckBox)view.findViewById(R.id.remind);
        if ("1".equals(contact.remind)){
            remind.setChecked(true);
        }

        notification = (CheckBox)view.findViewById(R.id.notification);
        if ("1".equals(contact.notification)){
            notification.setChecked(true);
        }

        shopEnable = (CheckBox)view.findViewById(R.id.shop_enabled);
        if ("1".equals(contact.shop_enabled)){
            shopEnable.setChecked(true);
        }

        banner = (SmartyImageView)view.findViewById(R.id.shop_banner);
        banner.setImage("http:" +contact.shop_banner);
        banner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Intent.ACTION_PICK);
                i.setType("image/*");
                startActivityForResult(i, 1);
            }
        });
    }

    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_change_my_info, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.save){
            saveContact();
        }
        return super.onOptionsItemSelected(item);
    }

    private void saveContact(){
        contact.ibi_name = name.getEditText().getText().toString().trim();
        contact.phone = phone.getEditText().getText().toString().trim();
        contact.shop_name = shopName.getEditText().getText().toString().trim();
        contact.shop_phone = shopPhone.getEditText().getText().toString().trim();
        contact.shop_email = shopEmail.getEditText().getText().toString().trim();
        contact.shop_about = shopAbout.getEditText().getText().toString().trim();
        contact.shop_address = shopAddress.getEditText().getText().toString().trim();

        contact.subscribe = subscribe.isChecked() ? "1" : "0";
        contact.notification = notification.isChecked() ? "1" : "0";
        contact.remind = remind.isChecked() ? "1" : "0";
        contact.shop_enabled = shopEnable.isChecked() ? "1" : "0";

        objectsListener.setObjects();
    }

    private String setBitmap(ImageView image, Intent data) {
        Bitmap img = null;
        Uri selectedImage = data.getData();
        try {
            img = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), selectedImage);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        image.setImageBitmap(img);
        return "data:image/jpeg;base64,"+ ConvertHelper.encodeToBase64(img, Bitmap.CompressFormat.JPEG, 100);
    }

    public Contact getContact(){
        return contact;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1 && resultCode == Activity.RESULT_OK) {
            contact.shop_banner = setBitmap(banner, data);
        }
        super.onActivityResult(requestCode, resultCode, data);
    }
}

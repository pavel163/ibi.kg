package com.ebr163.ibikg.activity;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.widget.Toast;

import com.ebr163.ibikg.BaseApplication;
import com.ebr163.ibikg.C;
import com.ebr163.ibikg.R;
import com.ebr163.ibikg.activity.base.BaseActivityWithFragment;
import com.ebr163.ibikg.event.FakeEvent;
import com.ebr163.ibikg.event.LoadCustomFieldsEvent;
import com.ebr163.ibikg.event.PostAdvertEvent;
import com.ebr163.ibikg.fragment.ShareFragment;
import com.ebr163.ibikg.fragment.UpdateShareFragment;
import com.ebr163.ibikg.fragment.dialog.CurrencyDialog;
import com.ebr163.ibikg.model.Response;
import com.ebr163.ibikg.model.ResponseList;
import com.ebr163.ibikg.model.ads.AdDesc;
import com.ebr163.ibikg.service.http.HttpApiFactory;

import java.util.Map;

import de.greenrobot.event.EventBus;
import rx.functions.Action1;
import rx.functions.Func1;

public class ShareActivity extends BaseActivityWithFragment {

    protected AdDesc adDesc = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.activity_with_fragment);
        adDesc = (AdDesc) getIntent().getSerializableExtra(C.fields.data);
        super.onCreate(savedInstanceState);
        if (adDesc == null) {
            setToolbarTitle(getIntent().getStringExtra(C.fields.title));
            setToolbarSubTitle("Шаг " + getIntent().getIntExtra(C.fields.step, 1));

        }
        EventBus.getDefault().register(this);
    }

    @Override
    protected void initFragment() {
        progress = new ProgressDialog(this);
        if (adDesc == null) {
            fragment = ShareFragment.newInstance(getIntent().getStringExtra(C.fields.parent_id), getIntent().getIntExtra(C.fields.step, 1));
        } else {
            fragment = UpdateShareFragment.newInstance(adDesc);
        }
        getSupportFragmentManager().beginTransaction()
                .add(R.id.main_fragment_container, fragment)
                .commit();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    public void onEvent(FakeEvent event) {
        progress.setMessage("Загрузка валют");
        progress.show();
        Action1<ResponseList<Object>> onNextAction = new Action1<ResponseList<Object>>() {
            @Override
            public void call(ResponseList<Object> response) {
                if (response != null) {
                    progress.hide();
                    String[] currency = new String[response.data.size()];
                    for (int i = 0; i < response.data.size(); i++) {
                        currency[i] = ((Map) response.data.get(i)).get(C.fields.name_currency).toString();
                    }
                    ((ShareFragment) fragment).setData(response.data);
                    DialogFragment dialog = new CurrencyDialog();
                    dialog.setTargetFragment(fragment, 11);
                    Bundle args = new Bundle();
                    args.putStringArray(C.util.data, currency);
                    dialog.setArguments(args);
                    dialog.show(fragment.getFragmentManager(), dialog.getClass().getName());
                    Log.d("Загрузка валюты", "Успех");
                }
            }
        };

        Func1 error = new Func1() {
            @Override
            public Object call(Object o) {
                progress.hide();
                Log.d("Загрузка валюты", "Провал");
                return null;
            }
        };

        BaseApplication.instance.httpService.subscribeAction(HttpApiFactory.HttpMethodType.GET_CURRENCY, onNextAction, error, null);
    }

    public void onEvent(PostAdvertEvent event) {
        progress.setMessage("Публикация...");
        progress.show();
        Action1<Response<String>> onNextAction = new Action1<Response<String>>() {
            @Override
            public void call(Response<String> response) {
                progress.hide();
                if (response != null && response.success == true) {
                    Toast.makeText(ShareActivity.this, "Объявление опубликованно", Toast.LENGTH_LONG).show();
                    Log.d("Отправка объявления", "Успех");
                    ((ShareFragment) fragment).startActivity();
                } else {
                    Toast.makeText(ShareActivity.this, "Не все поля заполнены", Toast.LENGTH_LONG).show();
                }
            }
        };

        Func1 error = new Func1() {
            @Override
            public Object call(Object o) {
                progress.hide();
                Toast.makeText(ShareActivity.this, "Ошибка. Объявление не опубликованно", Toast.LENGTH_LONG).show();
                Log.d("Отправка объявления", "Провал");
                return null;
            }
        };
        if (event.advert.advert_id == null) {
            BaseApplication.instance.httpService.subscribeAction(HttpApiFactory.HttpMethodType.ADD_USER_ADVERT, onNextAction, error, event.advert);
        } else {
            BaseApplication.instance.httpService.subscribeAction(HttpApiFactory.HttpMethodType.SAVE_USER_ADVERT, onNextAction, error, event.advert);
        }
    }

    public void onEvent(LoadCustomFieldsEvent event) {
        progress.setMessage("Загрузка...");
        progress.show();
        Action1<ResponseList<Object>> onNextAction = new Action1<ResponseList<Object>>() {
            @Override
            public void call(ResponseList<Object> response) {
                progress.hide();
                if (response != null && response.success == true) {
                    ((ShareFragment)fragment).setCustomFields(response.data);
                } else {
                    Log.d("Провал", "Загрузка кастомных полей");
                }
            }
        };

        Func1 error = new Func1() {
            @Override
            public Object call(Object o) {
                progress.hide();
                Log.d("Провал", "Загрузка кастомных полей");
                return null;
            }
        };
        BaseApplication.instance.httpService.subscribeAction(HttpApiFactory.HttpMethodType.GET_CUSTOM_FIELDS, onNextAction, error, getIntent().getStringExtra(C.fields.parent_id));
    }

    @Override
    protected void onStop() {
        super.onStop();
        BaseApplication.instance.httpService.unsubscribe();
    }
}

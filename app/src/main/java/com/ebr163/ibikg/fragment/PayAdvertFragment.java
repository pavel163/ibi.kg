package com.ebr163.ibikg.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

import com.ebr163.ibikg.C;
import com.ebr163.ibikg.adapter.category.PayAdAdapter;
import com.ebr163.ibikg.event.DataEvent;
import com.ebr163.ibikg.fragment.dialog.PayDialogFragment;

import java.util.List;

import de.greenrobot.event.EventBus;

/**
 * Created by Bakht on 16.04.2016.
 */
public class PayAdvertFragment extends AdFragment {

    public static PayAdvertFragment newInstance() {
        return new PayAdvertFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EventBus.getDefault().register(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    @Override
    protected void initUI(View view) {
        super.initUI(view);
        ((ViewGroup)view).removeView(fab);
    }

    @Override
    protected RecyclerView.Adapter<RecyclerView.ViewHolder> getAdapter(List<Object> objectList) {
        return new PayAdAdapter(objectList);
    }

    public void onEvent(DataEvent event){
        DialogFragment dialog = new PayDialogFragment();
        Bundle args = new Bundle();
        args.putString(C.fields.id, event.data);
        dialog.setArguments(args);
        dialog.show(getFragmentManager(), dialog.getClass().getName());
    }
}

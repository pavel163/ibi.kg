package com.ebr163.ibikg.activity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.ebr163.ibikg.BaseApplication;
import com.ebr163.ibikg.R;
import com.ebr163.ibikg.activity.base.BaseActivityWithFragment;
import com.ebr163.ibikg.fragment.ContactFragment;
import com.ebr163.ibikg.model.Contact;
import com.ebr163.ibikg.model.Response;
import com.ebr163.ibikg.model.ResponseContact;
import com.ebr163.ibikg.model.User;
import com.ebr163.ibikg.service.SettingsService;
import com.ebr163.ibikg.service.http.HttpApiFactory;

import rx.functions.Action1;
import rx.functions.Func1;

public class ContactActivity extends BaseActivityWithFragment implements ContactFragment.OnSetObjectsListener {

    private Contact contact;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.activity_with_fragment);
        super.onCreate(savedInstanceState);
        setToolbarTitle("Личные данные");
    }

    @Override
    protected void initFragment() {
        fragment = ContactFragment.newInstance();
        getSupportFragmentManager().beginTransaction()
                .add(R.id.main_fragment_container, fragment)
                .commit();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_contact, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.exit_contax) {
            SettingsService.saveUser(this, new User("", "", "", "", "", ""));
            startActivity(new Intent(this, MainActivity.class));
            finish();
        }
        if (item.getItemId() == R.id.update_pass) {
            startActivity(new Intent(this, UpdatePassActivity.class));
        }
        if (item.getItemId() == R.id.update_contact) {
            Intent intent = new Intent(this, ChangeMyInfoActivity.class);
            intent.putExtra("contact", ((ContactFragment) fragment).getData());
            startActivity(intent);
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void setObjects() {
        progress.show();
        Action1<ResponseContact> onNextAction = new Action1<ResponseContact>() {
            @Override
            public void call(ResponseContact response) {
                if (response != null) {
                    if (response.success) {
                        contact = response.data;
                        ((ContactFragment) fragment).setData(response.data);
                        getBalance();
                    } else {
                        //TODO Заменить на SnackBar
                        Toast.makeText(ContactActivity.this, "Ошибка", Toast.LENGTH_LONG).show();
                        Log.d("getContact", "Сервер вернул ошибку");
                    }
                }
            }
        };

        BaseApplication.instance.httpService.subscribeAction(HttpApiFactory.HttpMethodType.GET_CONTACT, onNextAction, getErrorFunc(), SettingsService.getUser(this).key);
    }

    private Func1 getErrorFunc() {
        return new Func1() {
            @Override
            public Object call(Object o) {
                progress.hide();
                Toast.makeText(ContactActivity.this, "Ошибка. Проверьте подключение к интернету", Toast.LENGTH_LONG).show();
                Log.d("getContact", "Провал");
                return null;
            }
        };
    }

    private void getBalance() {
        Action1<Response<Object>> onNextAction = new Action1<Response<Object>>() {
            @Override
            public void call(Response<Object> response) {
                progress.hide();
                if (response != null) {
                    ((ContactFragment) fragment).setBalance(String.valueOf(response.data.get("balance")));
                }
            }
        };

        BaseApplication.instance.httpService.subscribeAction(HttpApiFactory.HttpMethodType.GET_USER_BALANCE, onNextAction, getErrorFunc(), SettingsService.getUser(this).key);
    }

    @Override
    protected void onStop() {
        super.onStop();
        BaseApplication.instance.httpService.unsubscribe();
    }
}

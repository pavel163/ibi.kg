package com.ebr163.ibikg.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.ebr163.ibikg.C;
import com.ebr163.ibikg.R;
import com.ebr163.ibikg.model.Category;
import com.ebr163.ibikg.ui.SmartyImageView;
import com.ebr163.ibikg.ui.SmartyTextView;

import java.util.List;
import java.util.Map;

/**
 * Created by Bakht on 11.04.2016.
 */
public class BlackListAdapter extends BaseAdapter {

    protected List<Object> contacts;
    protected LayoutInflater lInflater;
    protected Context context;

    public BlackListAdapter(Context context, List<Object> contacts) {
        this.contacts = contacts;
        this.context = context;
        lInflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return contacts.size();
    }

    @Override
    public Object getItem(int position) {
        return contacts.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View rowView = convertView;
        if (rowView == null) {
            rowView = lInflater.inflate(R.layout.item_black_list, null);
            ViewHolder viewHolder = new ViewHolder();
            viewHolder.name = (SmartyTextView) rowView.findViewById(R.id.name);
            viewHolder.phone = (SmartyTextView) rowView.findViewById(R.id.phone);
            viewHolder.id = (SmartyTextView) rowView.findViewById(R.id.id);
            rowView.setTag(viewHolder);
        }

        ViewHolder holder = (ViewHolder) rowView.getTag();
        Map<String, Object> map = (Map<String, Object>) contacts.get(position);
        holder.name.setText(map.get(C.advert.ibi_name).toString());
        holder.phone.setText(map.get(C.contact.phone).toString());
        holder.id.setText(map.get(C.advert.user_id).toString());

        return rowView;
    }

    static class ViewHolder {
        public SmartyTextView name;
        public SmartyTextView phone;
        public SmartyTextView id;
    }
}

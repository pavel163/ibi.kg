package com.ebr163.ibikg.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ebr163.ibikg.R;
import com.ebr163.ibikg.ui.SmartyImageView;

import java.util.List;
import java.util.Map;

public class ShopsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<Object> shops;

    public ShopsAdapter(List<Object> shops) {
        this.shops = shops;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_shop, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ((ViewHolder) holder).bind((Map) shops.get(position));
    }

    @Override
    public int getItemCount() {
        return shops.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        private SmartyImageView imageView;
        private TextView title;
        private TextView city;
        private TextView address;

        public ViewHolder(View itemView) {
            super(itemView);
            this.imageView = (SmartyImageView) itemView.findViewById(R.id.image);
            this.title = (TextView) itemView.findViewById(R.id.title);
            this.city = (TextView) itemView.findViewById(R.id.city);
            this.address = (TextView) itemView.findViewById(R.id.address);
        }

        void bind(Map shop) {
            title.setText(shop.get("shop_name").toString());
            city.setText(shop.get("shop_city_id").toString());
            address.setText(shop.get("shop_address").toString());
            imageView.setImage("http:" + shop.get("shop_shoplogo"));
        }
    }
}

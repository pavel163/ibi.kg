package com.ebr163.ibikg.model;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Bakht on 01.04.2016.
 */
public class AdvertsParams {
    public String id;
    public String city;
    public String region;
    public String limit;
    public String offset;
    public String filter;
    public Map<String, String> filters = new HashMap<>();
}

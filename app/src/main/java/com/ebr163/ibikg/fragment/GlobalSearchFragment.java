package com.ebr163.ibikg.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.ebr163.ibikg.C;
import com.ebr163.ibikg.R;
import com.ebr163.ibikg.activity.CityActivity;
import com.ebr163.ibikg.activity.RegionActivity;
import com.ebr163.ibikg.activity.SearchCategoryActivity;
import com.ebr163.ibikg.event.LoadCustomFieldsEvent;
import com.ebr163.ibikg.fragment.base.BaseFragment;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.greenrobot.event.EventBus;

/**
 * Created by Bakht on 24.03.2016.
 */
public class GlobalSearchFragment extends BaseFragment implements View.OnClickListener {

    private LinearLayout content;
    private OnSetObjectsListener objectsListener;
    private TextInputLayout region;
    private TextInputLayout city;
    private TextInputLayout filter;
    private TextInputLayout limit;
    private View rootView;
    private Map<String, View> customFields = new HashMap<>();

    public interface OnSetObjectsListener {
        void setObjects(String parentId);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        objectsListener = (OnSetObjectsListener) context;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        EventBus.getDefault().post(new LoadCustomFieldsEvent());
    }

    public static GlobalSearchFragment newInstance(String parentId) {
        Bundle args = new Bundle();
        args.putString(C.fields.parent_id, parentId);
        GlobalSearchFragment globalSearchFragment = new GlobalSearchFragment();
        globalSearchFragment.setArguments(args);
        return globalSearchFragment;
    }

    @Override
    public int getLayout() {
        return R.layout.fragment_global_search;
    }

    @Override
    protected void initUI(View view) {
        this.rootView = view;

        content = (LinearLayout) view.findViewById(R.id.content);
        region = (TextInputLayout) view.findViewById(R.id.region);
        filter = (TextInputLayout) view.findViewById(R.id.filter);

        region.getEditText().setOnClickListener(this);
        city = (TextInputLayout) view.findViewById(R.id.city);
        city.getEditText().setOnClickListener(this);
        limit = (TextInputLayout) view.findViewById(R.id.limit);
    }

    public void addContent(final View view) {
        final String parentId = getArguments().getString(C.fields.parent_id);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        layoutParams.bottomMargin = 16;

        view.setLayoutParams(layoutParams);
        view.setId(Integer.parseInt(parentId));
        content.addView(view);

        ((TextInputLayout) view).getEditText().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity(), SearchCategoryActivity.class);
                i.putExtra(C.fields.parent_id, parentId);
                startActivityForResult(i, Integer.parseInt(parentId));
                while (((View) v.getParent()).getId() != content.getChildAt(content.getChildCount() - 1).getId()) {
                    content.removeView(content.getChildAt(content.getChildCount() - 1));
                }
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1 && data != null) {
            city.getEditText().setText(data.getStringExtra(C.location.city_name));
            getArguments().putString(C.location.city_id, data.getStringExtra(C.location.city_id));
            return;
        }
        if (requestCode == 2 && data != null) {
            region.getEditText().setText(data.getStringExtra(C.location.region_name));
            getArguments().putString(C.location.region_id, data.getStringExtra(C.location.region_id));
            return;
        }

        if (data != null) {
            String categoryName = data.getStringExtra(C.advert.name_category);
            ((TextInputLayout) content.findViewById(requestCode)).getEditText().setText(categoryName);
            getArguments().putString(C.fields.parent_id, data.getStringExtra(C.fields.parent_id));
            getArguments().putString(C.fields.title, categoryName);

            if (data.getStringExtra(C.fields.childs).equals("1")) {
                objectsListener.setObjects(data.getStringExtra(C.fields.parent_id));
            }
        }
    }

    @Override
    public void onClick(View v) {
        int id = ((View) v.getParent()).getId();
        switch (id) {
            case R.id.region:
                city.getEditText().setText("");
                getArguments().putString(C.location.city_id, "0");
                startActivityForResult(new Intent(getActivity(), RegionActivity.class), 2);
                break;
            case R.id.city:
                String region = getArguments().getString(C.location.region_id);
                Intent intent = new Intent(getActivity(), CityActivity.class);
                if (region != null) {
                    intent.putExtra(C.location.region_id, Integer.parseInt(region));
                }
                startActivityForResult(intent, 1);
                break;
        }
    }

    public void setCustomFields(List<Object> customFields) {
        for (Object object : customFields) {
            Map<String, Object> map = (Map<String, Object>) object;
            createCustomField(map);
        }
    }

    private void createCustomField(Map<String, Object> map) {
        LayoutInflater lInflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (map.get(C.custom_fields.id) != null)
            switch (map.get(C.custom_fields.custom_type).toString()) {
                case "1":
                    TextInputLayout editText = (TextInputLayout) lInflater.inflate(R.layout.custom_text_input_layout, null);
                    editText.setHint(map.get(C.custom_fields.custom_name).toString());
                    ((ViewGroup) rootView.findViewById(R.id.custom_fields)).addView(editText);
                    customFields.put(map.get(C.custom_fields.id).toString(), editText);
                    break;
                case "2":
                    RelativeLayout spinner = (RelativeLayout) lInflater.inflate(R.layout.custom_spinner, null);
                    String[] option = map.get(C.custom_fields.custom_options).toString().split(";");
                    ArrayList<String> o = new ArrayList<>();
                    o.add("Не выбрано");
                    List<String> splitList = Arrays.asList(option);
                    for (int i = 0; i<splitList.size(); i++){
                        o.add(splitList.get(i).trim());
                    }
                    ArrayAdapter<String> adapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_item, o);
                    ((Spinner) spinner.findViewById(R.id.spinner)).setAdapter(adapter);
                    ((TextView) spinner.findViewById(R.id.title)).setText(map.get(C.custom_fields.custom_name).toString());
                    ((ViewGroup) rootView.findViewById(R.id.custom_fields)).addView(spinner);
                    customFields.put(map.get(C.custom_fields.id).toString(), spinner.findViewById(R.id.spinner));
                    break;
                case "3":
                    CheckBox checkBox = (CheckBox) lInflater.inflate(R.layout.custom_check_box, null);
                    checkBox.setText(map.get(C.custom_fields.custom_name).toString());
                    ((ViewGroup) rootView.findViewById(R.id.custom_fields)).addView(checkBox);
                    customFields.put(map.get(C.custom_fields.id).toString(), checkBox);
                    break;
            }
    }

    public Map<String, String> getCustomFields() {
        Map<String, String> result = new HashMap<>();
        for (Map.Entry<String, View> entry : customFields.entrySet()) {
            String value = null;
            View view = entry.getValue();
            if (view instanceof TextInputLayout) {
                if (!"".equals(((TextInputLayout) view).getEditText().getText().toString()))
                    result.put("custom_field_" + entry.getKey(), ((TextInputLayout) view).getEditText().getText().toString());
            } else if (view instanceof Spinner) {
                if (!((Spinner) view).getSelectedItem().toString().equals("Не выбрано")) {
                    result.put("custom_field_" + entry.getKey(), ((Spinner) view).getSelectedItem().toString());
                }
            } else {
                result.put("custom_field_" + entry.getKey(), ((CheckBox) view).isChecked() ? "1" : "0");
            }
        }

        return result;
    }

    public String getLimit() {
        return limit.getEditText().getText().toString();
    }

    public String getFilter() {
        return filter.getEditText().getText().toString();
    }
}

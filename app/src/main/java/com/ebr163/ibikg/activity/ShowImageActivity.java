package com.ebr163.ibikg.activity;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ebr163.ibikg.R;
import com.ebr163.ibikg.ui.SmartyImageView;
import com.ebr163.ibikg.util.DepthPageTransformer;

import java.util.ArrayList;

public class ShowImageActivity extends AppCompatActivity {

    private SectionsPagerAdapter adapter;
    private ArrayList<String> data;
    private ViewPager viewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //TODO Порефакторить и прикрутить точки на просмотре
        setContentView(R.layout.activity_show_image);
        data = getIntent().getStringArrayListExtra("data");

        adapter = new SectionsPagerAdapter(getSupportFragmentManager(), data);
        viewPager = (ViewPager) findViewById(R.id.container);
        viewPager.setPageTransformer(true, new DepthPageTransformer());

        viewPager.setAdapter(adapter);
        viewPager.setCurrentItem(getIntent().getIntExtra("pos", 0));
    }

    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public ArrayList<String> data = new ArrayList<>();

        public SectionsPagerAdapter(FragmentManager fm, ArrayList<String> data) {
            super(fm);
            this.data = data;
        }

        @Override
        public Fragment getItem(int position) {
            return PlaceholderFragment.newInstance(position, data.get(position));
        }

        @Override
        public int getCount() {
            return data.size();
        }


        public CharSequence getPageTitle(int position) {
            return "";
        }
    }

    public static class PlaceholderFragment extends Fragment {

        String name, url;
        int pos;
        private static final String ARG_SECTION_NUMBER = "section_number";
        private static final String ARG_IMG_URL = "image_url";


        @Override
        public void setArguments(Bundle args) {
            super.setArguments(args);
            this.pos = args.getInt(ARG_SECTION_NUMBER);
            this.url = args.getString(ARG_IMG_URL);
        }

        public static PlaceholderFragment newInstance(int sectionNumber, String url) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            args.putString(ARG_IMG_URL, url);
            fragment.setArguments(args);
            return fragment;
        }

        public PlaceholderFragment() {
        }

        @Override
        public void onStart() {
            super.onStart();
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_show_image, container, false);
            SmartyImageView imageView = (SmartyImageView) rootView.findViewById(R.id.detail_image);
            imageView.setImage(url);
            return rootView;
        }


    }
}

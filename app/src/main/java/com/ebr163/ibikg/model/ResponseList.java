package com.ebr163.ibikg.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Bakht on 26.03.2016.
 */
public class ResponseList<T> {
    public boolean success;
    public List<Object> data = new ArrayList<>();
}

package com.ebr163.ibikg.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.ebr163.ibikg.BaseApplication;
import com.ebr163.ibikg.C;
import com.ebr163.ibikg.R;
import com.ebr163.ibikg.activity.base.BaseActivityWithFragment;
import com.ebr163.ibikg.event.LoadCustomFieldsEvent;
import com.ebr163.ibikg.fragment.GlobalSearchFragment;
import com.ebr163.ibikg.model.Category;
import com.ebr163.ibikg.model.Response;
import com.ebr163.ibikg.model.ResponseList;
import com.ebr163.ibikg.service.http.HttpApiFactory;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import de.greenrobot.event.EventBus;
import rx.functions.Action1;
import rx.functions.Func1;

public class GlobalSearchActivity extends BaseActivityWithFragment implements GlobalSearchFragment.OnSetObjectsListener {

    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.activity_with_fragment);
        super.onCreate(savedInstanceState);
        EventBus.getDefault().register(this);
        setToolbarTitle("Уточнить");
        setObjects(getIntent().getStringExtra(C.fields.parent_id));
    }

    @Override
    protected void initUI() {
        super.initUI();
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Закгрузка...");
    }

    @Override
    protected void initFragment() {
        fragment = GlobalSearchFragment.newInstance(getIntent().getStringExtra(C.fields.parent_id));
        getSupportFragmentManager().beginTransaction()
                .add(R.id.main_fragment_container, fragment)
                .commit();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_search, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.show_result) {
            Intent intent = new Intent(this, AdActivity.class);
            intent.putExtra(C.fields.parent_id, fragment.getArguments().getString(C.fields.parent_id));
            intent.putExtra(C.location.region_id, fragment.getArguments().getString(C.location.region_id));
            intent.putExtra(C.location.city_id, fragment.getArguments().getString(C.location.city_id));
            intent.putExtra(C.fields.title, fragment.getArguments().getString(C.fields.title));
            intent.putExtra(C.fields.limit, ((GlobalSearchFragment) fragment).getLimit());
            intent.putExtra(C.fields.filter, ((GlobalSearchFragment) fragment).getFilter());
            intent.putExtra("search", true);
            intent.putExtra("filters", (Serializable) ((GlobalSearchFragment) fragment).getCustomFields());
            startActivity(intent);
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void setObjects(String parentId) {
        progressDialog.show();
        Action1<Response<Category>> onNextAction = new Action1<Response<Category>>() {
            @Override
            public void call(Response<Category> response) {
                if (response != null) {
                    progressDialog.hide();
                    LayoutInflater lInflater = (LayoutInflater) GlobalSearchActivity.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    View view = lInflater.inflate(R.layout.txt_category, null);
                    ((GlobalSearchFragment) fragment).addContent(view);
                }
            }
        };

        Func1 error = new Func1() {
            @Override
            public Object call(Object o) {
                progressDialog.hide();
                Log.d("Загрузка категорий", "Провал");
                return null;
            }
        };

        Map<String, Object> map = new HashMap<>();
        map.put(C.fields.parent_id, parentId);
        map.put(C.custom_fields.custom_type, ((GlobalSearchFragment)fragment).getCustomFields());
        BaseApplication.instance.httpService.subscribeAction(HttpApiFactory.HttpMethodType.GET_CATEGORY, onNextAction, error, parentId);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    public void onEvent(LoadCustomFieldsEvent event) {
        progress.setMessage("Загрузка...");
        progress.show();
        Action1<ResponseList<Object>> onNextAction = new Action1<ResponseList<Object>>() {
            @Override
            public void call(ResponseList<Object> response) {
                progress.hide();
                if (response != null && response.success == true) {
                   ((GlobalSearchFragment)fragment).setCustomFields(response.data);
                } else {
                    Log.d("Провал", "Загрузка кастомных полей");
                }
            }
        };

        Func1 error = new Func1() {
            @Override
            public Object call(Object o) {
                progress.hide();
                Log.d("Провал", "Загрузка кастомных полей");
                return null;
            }
        };
        BaseApplication.instance.httpService.subscribeAction(HttpApiFactory.HttpMethodType.GET_CUSTOM_FIELDS, onNextAction, error, getIntent().getStringExtra(C.fields.parent_id));
    }

    @Override
    protected void onStop() {
        super.onStop();
        BaseApplication.instance.httpService.unsubscribe();
    }
}

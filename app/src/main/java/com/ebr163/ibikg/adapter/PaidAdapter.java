package com.ebr163.ibikg.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.ebr163.ibikg.C;
import com.ebr163.ibikg.R;
import com.ebr163.ibikg.ui.SmartyTextView;

import java.util.List;
import java.util.Map;

/**
 * Created by Bakht on 26.03.2016.
 */
public class PaidAdapter extends BaseAdapter {

    protected List<Object> paids;
    protected LayoutInflater lInflater;
    protected Context context;

    public PaidAdapter(Context context, List<Object> paids) {
        this.paids = paids;
        this.context = context;
        lInflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return paids.size();
    }

    @Override
    public Object getItem(int position) {
        return paids.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View rowView = convertView;
        if (rowView == null) {
            rowView = lInflater.inflate(R.layout.item_paid, null);
            ViewHolder viewHolder = new ViewHolder();
            viewHolder.title = (SmartyTextView) rowView.findViewById(R.id.title);
            viewHolder.id = (SmartyTextView) rowView.findViewById(R.id.id);
            viewHolder.cost = (SmartyTextView) rowView.findViewById(R.id.cost);
            rowView.setTag(viewHolder);
        }

        ViewHolder holder = (ViewHolder) rowView.getTag();
        Map<String, Object> paid = (Map<String, Object>) paids.get(position);
        holder.title.setText(paid.get(C.fields.name_method).toString());
        holder.id.setText(paid.get(C.fields.id).toString());
        holder.cost.setText("Цена " + paid.get(C.fields.cost).toString());

        return rowView;
    }

    static class ViewHolder {
        public SmartyTextView title;
        public SmartyTextView id;
        public SmartyTextView cost;
    }
}

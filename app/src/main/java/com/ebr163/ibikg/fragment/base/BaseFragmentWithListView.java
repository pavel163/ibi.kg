package com.ebr163.ibikg.fragment.base;

import android.view.View;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;

import com.ebr163.ibikg.R;
import com.ebr163.ibikg.event.OffsetEvent;
import com.ebr163.ibikg.util.pagination.EndlessListViewScrollListener;

import java.util.List;

import de.greenrobot.event.EventBus;

/**
 * Created by Bakht on 11.03.2016.
 */
public abstract class BaseFragmentWithListView extends BaseFragmentWithList implements AdapterView.OnItemClickListener {

    protected ListView listView;

    @Override
    protected void initUI(View view) {
        super.initUI(view);
        listView = (ListView) view.findViewById(R.id.list);
        listView.setOnItemClickListener(this);
        listView.setEmptyView(progressBar);
        if (objectList == null){
            objectsListener.setObjects(null);
        } else {
            setupListView(objectList);
        }
    }

    public void setupListView(List<Object> objectList){
        listView.setAdapter(getAdapter(objectList));
        listView.setOnScrollListener(new EndlessListViewScrollListener() {
            @Override
            public boolean onLoadMore(int page, int totalItemsCount) {
                postOffsetEvent(page);
                return true;
            }
        });
        setVisibleProgress(View.GONE);
    }

    public void updateList(){
        ((BaseAdapter)listView.getAdapter()).notifyDataSetChanged();
    }

    protected void postOffsetEvent(int page){
        EventBus.getDefault().post(new OffsetEvent(page));
    }

    public void updateData(){
        ((BaseAdapter)listView.getAdapter()).notifyDataSetChanged();
        if (objectList.size() == 0){
            setVisibleProgress(View.GONE);
        }
    }

    protected abstract BaseAdapter getAdapter(List<Object> objectList);

    protected abstract void clickItem(int position);

    @Override
    public int getLayout() {
        return R.layout.fragment_with_list_view;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        clickItem(position);
    }
}

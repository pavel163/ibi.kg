package com.ebr163.ibikg.ui.autoloadingrecycler;

import java.util.List;

import rx.Observable;

/**
 * Created by Bakht on 09.04.2016.
 */
public interface ILoading<T> {

    Observable<List<T>> getLoadingObservable(OffsetAndLimit offsetAndLimit);
}
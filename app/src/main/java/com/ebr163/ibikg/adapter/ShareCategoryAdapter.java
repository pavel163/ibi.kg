package com.ebr163.ibikg.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ebr163.ibikg.R;
import com.ebr163.ibikg.model.Category;
import com.ebr163.ibikg.ui.SmartyTextView;

import java.util.List;

/**
 * Created by Bakht on 19.03.2016.
 */
public class ShareCategoryAdapter extends CategoryAdapter {

    public ShareCategoryAdapter(Context context, List<Object> categories) {
        super(context, categories);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View rowView = convertView;
        if (rowView == null) {
            rowView = lInflater.inflate(R.layout.item_category_share, null);
            ViewHolder viewHolder = new ViewHolder();
            viewHolder.title = (SmartyTextView) rowView.findViewById(R.id.title);
            viewHolder.id = (TextView) rowView.findViewById(R.id.id);
            viewHolder.order = (TextView) rowView.findViewById(R.id.order);
            rowView.setTag(viewHolder);
        }

        ViewHolder holder = (ViewHolder) rowView.getTag();
        Category category = (Category) categories.get(position);
        holder.title.setText(category.name_category);
        holder.id.setText(category.id);
        holder.order.setText(category.order);
        return rowView;
    }
}

package com.ebr163.ibikg.event;

import com.ebr163.ibikg.event.base.BaseEvent;

/**
 * Created by Bakht on 16.04.2016.
 */
public class DataEvent extends BaseEvent{

    public String data;

    public DataEvent(String data) {
        this.data = data;
    }
}

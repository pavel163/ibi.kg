package com.ebr163.ibikg.di.module;

import android.content.Context;

import com.ebr163.ibikg.service.database.CRUDService;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Bakht on 23.05.2016.
 */
@Module
public class CRUDModule {

    @Singleton
    @Provides
    CRUDService provideCRUDService(Context context){
        return new CRUDService(context);
    }

}

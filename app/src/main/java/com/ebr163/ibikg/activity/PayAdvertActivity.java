package com.ebr163.ibikg.activity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.ebr163.ibikg.BaseApplication;
import com.ebr163.ibikg.C;
import com.ebr163.ibikg.R;
import com.ebr163.ibikg.activity.base.BaseActivityWithFragment;
import com.ebr163.ibikg.fragment.PayAdvertFragment;
import com.ebr163.ibikg.fragment.base.BaseFragmentWithList;
import com.ebr163.ibikg.fragment.base.BaseFragmentWithRecyclerView;
import com.ebr163.ibikg.model.PayEvent;
import com.ebr163.ibikg.model.ResponseList;
import com.ebr163.ibikg.model.ads.Ad;
import com.ebr163.ibikg.model.ads.AdDesc;
import com.ebr163.ibikg.service.SettingsService;
import com.ebr163.ibikg.service.http.HttpApiFactory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.greenrobot.event.EventBus;
import rx.functions.Action1;
import rx.functions.Func1;

public class PayAdvertActivity extends BaseActivityWithFragment implements BaseFragmentWithList.OnSetObjectsListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.activity_with_fragment);
        super.onCreate(savedInstanceState);
        setToolbarTitle("Выберите объявление");
        EventBus.getDefault().register(this);
    }

    @Override
    protected void initFragment() {
        fragment = PayAdvertFragment.newInstance();
        getSupportFragmentManager().beginTransaction()
                .add(R.id.main_fragment_container, fragment)
                .commit();
    }


    @Override
    public void setObjects(Object params) {
        Action1<ResponseList<Map<String, AdDesc>>> onNextAction = new Action1<ResponseList<Map<String, AdDesc>>>() {
            @Override
            public void call(ResponseList<Map<String, AdDesc>> response) {
                if (response != null) {
                    List<Object> data = new ArrayList<>();
                    for (Object object: response.data){
                        Ad ad = new Ad();
                        ad.id = ((Map)object).get(C.fields.id).toString();
                        ad.advert_name = ((Map)object).get(C.advert.advert_name).toString();
                        ad.price = String.valueOf(((Map) object).get(C.advert.price));
                        ad.date_publish = ((Map)object).get(C.advert.date_publish).toString();
                        ad.region_name = String.valueOf(((Map) object).get(C.location.region_name).toString());
                        if (((Map)object).get(C.advert.name_currency) != null) {
                            ad.name_currency = ((Map)object).get(C.advert.name_currency).toString();
                        }
                        ad.photos = (ArrayList) ((Map)object).get(C.advert.photos);
                        data.add(ad);
                    }
                    ((BaseFragmentWithRecyclerView) fragment).setObjectList(data);
                    ((BaseFragmentWithRecyclerView) fragment).setupRecyclerView(data);
                    Log.d("Загрузка объявлений", "Успех");
                }
            }
        };

        Func1 error = new Func1() {
            @Override
            public Object call(Object o) {
                ((BaseFragmentWithList) fragment).setVisibleErrorMessage(View.VISIBLE);
                ((BaseFragmentWithList) fragment).setVisibleProgress(View.GONE);
                Log.d("Загрузка категорий", "Провал");
                return null;
            }
        };

        Map<String, String> data = new HashMap<>();
        ((Map)data).put(C.fields.key, SettingsService.getUser(this).key);
        BaseApplication.instance.httpService.subscribeAction(HttpApiFactory.HttpMethodType.GET_USER_ADVERTS, onNextAction, error, data);
    }

    public void onEvent(PayEvent event){
        progress.show();
        Action1<Map<String, Object>> onNextAction = new Action1<Map<String, Object>>() {
            @Override
            public void call(Map<String, Object> response) {
                progress.hide();
                if ((Boolean) response.get("success")) {
                    Toast.makeText(PayAdvertActivity.this, "Покупка совершена", Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(PayAdvertActivity.this, MainActivity.class);
                    intent.putExtra("fragment", 3);
                    Log.d("Покупка", "Успех");
                } else {
                    Toast.makeText(PayAdvertActivity.this, "Недостаточно средств", Toast.LENGTH_SHORT).show();
                }
            }
        };

        Func1 error = new Func1() {
            @Override
            public Object call(Object o) {
                progress.hide();
                Log.d("Покупка", "Провал");
                return null;
            }
        };

        Map<String, String> map = new HashMap<>();
        map.put(C.fields.key, SettingsService.getUser(this).key);
        map.put(C.fields.service_id, getIntent().getStringExtra(C.fields.service_id));
        map.put(C.advert.advert_id, event.data);
        BaseApplication.instance.httpService.subscribeAction(HttpApiFactory.HttpMethodType.PAY_SERVICE, onNextAction, error, map);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        BaseApplication.instance.httpService.unsubscribe();
    }
}

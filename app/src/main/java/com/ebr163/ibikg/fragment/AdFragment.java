package com.ebr163.ibikg.fragment;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.DialogFragment;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

import com.ebr163.ibikg.C;
import com.ebr163.ibikg.R;
import com.ebr163.ibikg.activity.GlobalSearchActivity;
import com.ebr163.ibikg.adapter.category.AdAdapter;
import com.ebr163.ibikg.fragment.base.BaseFragmentWithRecyclerView;
import com.ebr163.ibikg.fragment.dialog.MsgWarningDialog;
import com.ebr163.ibikg.model.ads.Ad;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Bakht on 16.03.2016.
 */
public class AdFragment extends BaseFragmentWithRecyclerView implements SearchView.OnQueryTextListener {

    protected FloatingActionButton fab;

    public static AdFragment newInstance(String id) {
        Bundle args = new Bundle();
        args.putString(C.fields.parent_id, id);
        AdFragment adFragment = new AdFragment();
        adFragment.setArguments(args);
        return adFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    protected void initUI(View view) {
        super.initUI(view);
        setVisibleProgress(View.VISIBLE);
        fab = (FloatingActionButton) view.findViewById(R.id.fab);
        fab.setOnClickListener(this);
    }

    @Override
    protected RecyclerView.Adapter<RecyclerView.ViewHolder> getAdapter(List<Object> objectList) {
        return new AdAdapter(objectList);
    }

    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_search_menu, menu);
        MenuItem item = menu.findItem(R.id.action_search);
        SearchView searchView = (SearchView) MenuItemCompat.getActionView(item);
        searchView.setOnQueryTextListener(this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        if (v.getId() == R.id.fab){
            boolean enabled = false;
            if (Build.VERSION.SDK_INT >= 17) {
                enabled = Settings.System.getInt(getActivity().getContentResolver(), Settings.System.ALWAYS_FINISH_ACTIVITIES, 0) == 1;
            }
            if (enabled){
                DialogFragment dialog = new MsgWarningDialog();
                dialog.show(getFragmentManager(), dialog.getClass().getName());
            } else {
                if (getActivity().getIntent().getBooleanExtra("search", false)){
                    getActivity().onBackPressed();
                } else {
                    Intent intent = new Intent(getActivity(), GlobalSearchActivity.class);
                    intent.putExtra(C.fields.parent_id, getActivity().getIntent().getStringExtra(C.fields.parent_id));
                    startActivity(intent);
                }
            }
        }
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        ((AdAdapter)recyclerView.getAdapter()).setFilter(filter(objectList, newText));
        return true;
    }

    private List<Object> filter (List<Object> ads, String query) {
        if (query.equals("")){
            return objectList;
        }
        query = query.toLowerCase();
        final List<Object> filteredModelList = new ArrayList<>();
        for (Object ad : ads) {
            final String text = ((Ad)ad).advert_name.toLowerCase();
            if (text.contains(query)) {
                filteredModelList.add(ad);
            }
        }
        return filteredModelList;
    }
}

package com.ebr163.ibikg.model;

/**
 * Created by Bakht on 18.03.2016.
 */
public class User {

    public final String name;
    public final String email;
    public final String phone;
    public final String password;
    public final String id;
    public final String key;

    public User(String name, String email, String phone, String password, String id, String user_key) {
        this.name = name;
        this.email = email;
        this.phone = phone;
        this.password = password;
        this.id = id;
        this.key = user_key;
    }
}

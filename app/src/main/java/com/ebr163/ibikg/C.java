package com.ebr163.ibikg;

/**
 * Created by Bakht on 11.03.2016.
 */
public final class C {

    public static final class fragment {
        public static final String tag_category = "category";
        public static final String tag_share = "share";
        public static final String tag_message = "message";
        public static final String tag_favors = "favors";
        public static final String tag_paid = "paid";
        public static final String tag_my_advert= "tag_my_advert";
        public static final String tag_black_list= "tag_black_list";
        public static final String tag_history= "tag_history";
        public static final String tag_shop = "tag_shop";
    }

    public static final class util {
        public static final String settings = "settings";
        public static final String data = "data";
    }

    public static final class contact {
        public static final String id = "id";
        public static final String user_id = "user_id";
        public static final String ibi_name = "ibi_name";
        public static final String phone = "phone";
        public static final String subscribe = "subscribe";
        public static final String notiofication = "notification";
        public static final String remind = "remind";
        public static final String shop_name = "shop_name";
        public static final String shop_phone = "shop_phone";
        public static final String shop_email = "shop_email";
        public static final String shop_city_id = "shop_city_id";
        public static final String shop_about = "shop_about";
        public static final String shop_banner = "shop_banner";
        public static final String shop_enabled = "shop_enabled";
        public static final String shop_address = "shop_address";
    }

    public static final class fields {
        public static final String parent_id = "parent_id";
        public static final String data = "data";
        public static final String id = "id";
        public static final String title = "title";
        public static final String subtitle = "subtitle";
        public static final String step = "step";
        public static final String name_currency = "name_currency";
        public static final String name_method = "name_method";
        public static final String cost = "cost";
        public static final String childs = "childs";
        public static final String limit = "limit";
        public static final String key = "key";
        public static final String offset = "offset";
        public static final String tab = "tab";
        public static final String old_pass = "old_pass";
        public static final String new_pass = "new_pass";
        public static final String service_id = "service_id";
        public static final String filter = "filter";
    }

    public static final class message {
        public static final String from_name = "from_name";
        public static final String mail_date = "mail_date";
        public static final String mail_body = "mail_body";
        public static final String mail_title = "mail_title";
        public static final String id = "id";
        public static final String from_user_id = "from_user_id";
        public static final String reply_is_blocked = "reply_is_blocked";
        public static final String unread = "unread";
    }


    public static final class login {
        public static final String name = "name";
        public static final String email = "email";
        public static final String phone = "phone";
        public static final String password = "password";
        public static final String id = "id";
        public static final String user_key = "user_key";
    }

    public static final class location {
        public static final String id = "id";
        public static final String region_id = "region_id";
        public static final String region_name = "region_name";
        public static final String city_name = "city_name";
        public static final String city_id = "city_id";
    }

    public static final class custom_fields {
        public static final String id = "id";
        public static final String custom_type = "custom_type";
        public static final String custom_name = "custom_name";
        public static final String custom_options = "custom_options";
        public static final String custom_value = "custom_value";
    }

    public static final class advert {
        public static final String id = "id";
        public static final String category_id = "category_id";
        public static final String user_id = "user_id";
        public static final String city_id = "city_id";
        public static final String advert_name = "advert_name";
        public static final String advert_id = "advert_id";
        public static final String advert_status = "advert_status";
        public static final String advert_url = "advert_url";
        public static final String advert_descr = "advert_descr";
        public static final String price = "price";
        public static final String currency_id = "currency_id";
        public static final String date_publish = "date_publish";
        public static final String address = "address";
        public static final String advert_phone = "advert_phone";
        public static final String contact = "contact";
        public static final String hits = "hits";
        public static final String published = "published";
        public static final String publish = "publish";
        public static final String mode_premium_until = "mode_premium_until";
        public static final String mode_vip_until = "mode_vip_until";
        public static final String mode_highlight_until = "mode_highlight_until";
        public static final String name_category = "name_category";
        public static final String city_name = "city_name";
        public static final String ibi_name = "ibi_name";
        public static final String name_currency = "name_currency";
        public static final String shop_enabled = "shop_enabled";
        public static final String photos = "photos";
        public static final String order = "order";
        public static final String photo_path = "photo_path";
        public static final String advert_custom_values = "advert_custom_values";
        public static final String custom_name = "custom_name";
        public static final String custom_value = "custom_value";
    }
}

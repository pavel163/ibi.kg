package com.ebr163.ibikg.adapter.favors;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ebr163.ibikg.R;
import com.ebr163.ibikg.adapter.category.AdAdapter;
import com.ebr163.ibikg.adapter.category.AdHolder;
import com.ebr163.ibikg.model.ads.AdDB;

import java.util.List;

/**
 * Created by Bakht on 25.03.2016.
 */
public class FavorsAdapter extends AdAdapter {

    public FavorsAdapter(List<Object> ads) {
        super(ads);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_ad, parent, false);
        return AdHolder.newInstance(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        AdHolder holder = (AdHolder) viewHolder;
        AdDB ad = (AdDB) ads.get(position);
        holder.setCurrency(ad.name_currency);
        holder.setId(ad.id);
        holder.setTitle(ad.advert_name.trim());
        holder.setPrice(ad.price);
        holder.setData(ad.date_publish);
        holder.setRegion(ad.region_name);
        holder.setImage("http:" + ad.photo);
    }
}

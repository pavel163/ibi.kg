package com.ebr163.ibikg.activity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.ebr163.ibikg.BaseApplication;
import com.ebr163.ibikg.C;
import com.ebr163.ibikg.R;
import com.ebr163.ibikg.activity.base.BaseActivityWithFragment;
import com.ebr163.ibikg.event.OffsetEvent;
import com.ebr163.ibikg.fragment.AdFragment;
import com.ebr163.ibikg.fragment.base.BaseFragmentWithList;
import com.ebr163.ibikg.fragment.base.BaseFragmentWithRecyclerView;
import com.ebr163.ibikg.model.AdvertsParams;
import com.ebr163.ibikg.model.ads.Ads;
import com.ebr163.ibikg.service.http.HttpApiFactory;

import java.util.Map;

import de.greenrobot.event.EventBus;
import rx.functions.Action1;
import rx.functions.Func1;

public class AdActivity extends BaseActivityWithFragment implements BaseFragmentWithList.OnSetObjectsListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.activity_with_fragment);
        super.onCreate(savedInstanceState);
        setToolbarTitle(getIntent().getStringExtra(C.fields.title));
    }

    @Override
    protected void initFragment() {
        fragment = AdFragment.newInstance(getIntent().getStringExtra(C.fields.parent_id));
        getSupportFragmentManager().beginTransaction()
                .add(R.id.main_fragment_container, fragment)
                .commit();
    }

    @Override
    public void setObjects(Object params) {
        getObject(0);
    }

    @Override
    protected void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
        BaseApplication.instance.httpService.unsubscribe();
    }

    public void onEvent(OffsetEvent event) {
        getObject(event.offset);
    }

    private void getObject(final int page) {
        Action1<Ads> onNextAction = new Action1<Ads>() {
            @Override
            public void call(Ads response) {
                if (response != null) {
                    if (page == 0) {
                        ((BaseFragmentWithRecyclerView) fragment).setObjectList(response.data);
                        ((BaseFragmentWithRecyclerView) fragment).setupRecyclerView(response.data);
                    } else {
                        ((BaseFragmentWithRecyclerView) fragment).addObjectList(response.data);
                        ((BaseFragmentWithRecyclerView) fragment).updateList();
                    }
                    Log.d("Загрузка объявлений", "Успех");
                }
            }
        };

        Func1 error = new Func1() {
            @Override
            public Object call(Object o) {
                ((BaseFragmentWithRecyclerView) fragment).setVisibleProgress(View.GONE);
                ((BaseFragmentWithRecyclerView) fragment).setVisibleErrorMessage(View.VISIBLE);
                Log.d("Загрузка объявлений", "Провал");
                return null;
            }
        };
        AdvertsParams params = new AdvertsParams();
        params.id = getIntent().getStringExtra(C.fields.parent_id);
        params.city = getIntent().getStringExtra(C.location.city_id);
        params.region = getIntent().getStringExtra(C.location.region_id);
        params.offset = String.valueOf(page * 10);

        String limit = getIntent().getStringExtra(C.fields.limit);
        if ("".equals(limit)) {
            params.limit = null;
        } else {
            params.limit = limit;
        }

        String filter = getIntent().getStringExtra(C.fields.filter);
        if ("".equals(filter)) {
            params.filter = null;
        } else {
            params.filter = filter;
        }

        if (getIntent().getSerializableExtra("filters") != null) {
            params.filters = (Map<String, String>) getIntent().getSerializableExtra("filters");
        }

        BaseApplication.instance.httpService.subscribeAction(HttpApiFactory.HttpMethodType.GET_ADS, onNextAction, error, params);
    }


}

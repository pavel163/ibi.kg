package com.ebr163.ibikg.activity.base;

import android.os.Bundle;

import com.ebr163.ibikg.R;
import com.ebr163.ibikg.fragment.base.BaseFragment;

/**
 * Created by Bakht on 10.03.2016.
 */
public abstract class BaseActivityWithFragment extends BaseActivity {

    protected BaseFragment fragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void initUI() {
        initFragment();
    }

    protected abstract void initFragment();
}

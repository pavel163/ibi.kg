package com.ebr163.ibikg.activity.login;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.ebr163.ibikg.BaseApplication;
import com.ebr163.ibikg.C;
import com.ebr163.ibikg.R;
import com.ebr163.ibikg.activity.MainActivity;
import com.ebr163.ibikg.event.LoginEvent;
import com.ebr163.ibikg.fragment.login.SignUpFragment;
import com.ebr163.ibikg.model.Response;
import com.ebr163.ibikg.model.User;
import com.ebr163.ibikg.service.SettingsService;
import com.ebr163.ibikg.service.http.HttpApiFactory;

import java.util.Map;

import rx.functions.Action1;
import rx.functions.Func1;

public class SignUpActivity extends LoginActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.activity_with_fragment);
        super.onCreate(savedInstanceState);
        setToolbarTitle("Регистрация");
    }

    @Override
    protected void initFragment() {
        fragment = SignUpFragment.newInstance();
        getSupportFragmentManager().beginTransaction()
                .add(R.id.main_fragment_container, fragment)
                .commit();
    }

    public void onEvent(final LoginEvent event){
        //TODO Object заменить на SignUpResult
        Action1<Response<Object>> onNextAction = new Action1<Response<Object>>() {
            @Override
            public void call(Response<Object> response) {
                if (response != null) {
                    if (response.success){
                        Map<String, String> map = (Map) response.data;
                        SettingsService.saveUser(SignUpActivity.this, new User(event.name, event.email, event.phone, event.password, map.get(C.login.id), map.get(C.login.user_key)));
                        Log.d("Регистрация", "Успех");
                        startActivity(new Intent(SignUpActivity.this, MainActivity.class));
                        finish();
                    } else {
                        //TODO Заменить на SnackBar
                        Toast.makeText(SignUpActivity.this, "Ошибка. Некорректные данные", Toast.LENGTH_LONG).show();
                        Log.d("Регистрация", "Сервер вернул ошибку");
                    }
                }
                progress.hide();
            }
        };

        Func1 error = new Func1() {
            @Override
            public Object call(Object o) {
                progress.hide();
                Toast.makeText(SignUpActivity.this, "Ошибка. Проверьте подключение к интернету", Toast.LENGTH_LONG).show();
                Log.d("Регистрация", "Провал");
                return null;
            }
        };

        progress.show();
        BaseApplication.instance.httpService.subscribeAction(HttpApiFactory.HttpMethodType.REGISTER, onNextAction, error, event);
    }

    @Override
    protected void onStop() {
        super.onStop();
        BaseApplication.instance.httpService.unsubscribe();
    }
}

package com.ebr163.ibikg.fragment.base;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.ebr163.ibikg.R;
import com.ebr163.ibikg.event.OffsetEvent;
import com.ebr163.ibikg.util.pagination.EndlessRecyclerOnScrollListener;

import java.util.List;

import de.greenrobot.event.EventBus;

/**
 * Created by Bakht on 21.03.2016.
 */
public abstract class BaseFragmentWithRecyclerView extends BaseFragmentWithList {

    protected RecyclerView recyclerView;

    @Override
    protected void initUI(View view) {
        super.initUI(view);
        recyclerView = (RecyclerView) view.findViewById(R.id.recycler);
        checkData();
    }

    protected void checkData(){
        if (objectList == null){
            objectsListener.setObjects(null);
        } else {
            setupRecyclerView(objectList);
        }
    }

    @Override
    public int getLayout() {
        return R.layout.fragment_with_recycler;
    }

    public void setupRecyclerView(List<Object> objectList){
        setVisibleProgress(View.GONE);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(getAdapter(objectList));
        recyclerView.addOnScrollListener(new EndlessRecyclerOnScrollListener(layoutManager) {
            @Override
            public void onLoadMore(int current_page) {
                postOffsetEvent(current_page);
            }
        });
    }

    protected void postOffsetEvent(int page){
        EventBus.getDefault().post(new OffsetEvent(page));
    }

    public void updateList(){
        recyclerView.getAdapter().notifyDataSetChanged();
    }

    protected abstract RecyclerView.Adapter<RecyclerView.ViewHolder> getAdapter(List<Object> objectList);

    public void setVisibleProgress(int visibility){
        progressBar.setVisibility(visibility);
    }
}

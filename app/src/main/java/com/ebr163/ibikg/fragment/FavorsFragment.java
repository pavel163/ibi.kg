package com.ebr163.ibikg.fragment;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

import com.ebr163.ibikg.adapter.favors.FavorsAdapter;

import java.util.List;

/**
 * Created by Bakht on 24.03.2016.
 */
public class FavorsFragment extends AdFragment {

    public static FavorsFragment newInstance() {
        return new FavorsFragment();
    }

    @Override
    protected void initUI(View view) {
        super.initUI(view);
        ((ViewGroup)view).removeView(fab);
        setVisibleProgress(View.GONE);
    }

    @Override
    protected void checkData() {}

    @Override
    protected RecyclerView.Adapter<RecyclerView.ViewHolder> getAdapter(List<Object> objectList) {
        return new FavorsAdapter(objectList);
    }

    @Override
    public void onResume() {
        super.onResume();
        objectsListener.setObjects(null);
    }
}

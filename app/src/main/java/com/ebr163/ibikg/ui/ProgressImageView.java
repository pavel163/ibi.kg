package com.ebr163.ibikg.ui;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestListener;
import com.ebr163.ibikg.R;
import com.ebr163.ibikg.helper.ConvertHelper;

/**
 * Created by mac1 on 19.04.16.
 */
public class ProgressImageView extends RelativeLayout {

    private SmartyImageView imageView;
    private ProgressBar progressBar;
    private RequestListener<String, Bitmap> requestListener;

    public ProgressImageView(Context context) {
        super(context);
        init();
    }

    public ProgressImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public ProgressImageView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {}

    private void init(){
        View view = inflate(getContext(), R.layout.image_view, this);
        progressBar = (ProgressBar) view.findViewById(R.id.progress);
        imageView = (SmartyImageView)view.findViewById(R.id.image_with_progress);
        requestListener = new RequestListener<String, Bitmap>() {
            @Override
            public boolean onException(Exception e, String model, com.bumptech.glide.request.target.Target<Bitmap> target, boolean isFirstResource) {
                return false;
            }

            @Override
            public boolean onResourceReady(Bitmap resource, String model, com.bumptech.glide.request.target.Target<Bitmap> target, boolean isFromMemoryCache, boolean isFirstResource) {
                imageView.setImageBitmap(resource);
                progressBar.setVisibility(VISIBLE);
                return false;
            }
        };
    }

    public void setImage(String uri){
        progressBar.setVisibility(VISIBLE);
        Glide.with(getContext())
                .load(uri)
                .asBitmap()
                .placeholder(R.drawable.no_image)
                .listener(requestListener)
                .preload();
    }
}

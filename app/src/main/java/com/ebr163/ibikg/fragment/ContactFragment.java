package com.ebr163.ibikg.fragment;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.CheckBox;

import com.ebr163.ibikg.R;
import com.ebr163.ibikg.activity.HistoryOperationActivity;
import com.ebr163.ibikg.fragment.base.BaseFragment;
import com.ebr163.ibikg.model.Contact;
import com.ebr163.ibikg.model.User;
import com.ebr163.ibikg.service.SettingsService;
import com.ebr163.ibikg.ui.SmartyImageView;
import com.ebr163.ibikg.ui.SmartyTextView;

/**
 * Created by Bakht on 04.04.2016.
 */
public class ContactFragment extends BaseFragment {

    private View rootView;
    private Contact data;
    private OnSetObjectsListener objectsListener;

    public interface OnSetObjectsListener {
        void setObjects();
    }

    public static ContactFragment newInstance() {
        return new ContactFragment();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        objectsListener = (OnSetObjectsListener) context;
    }

    @Override
    public int getLayout() {
        return R.layout.fragment_contact;
    }

    @Override
    protected void initUI(View view) {
        rootView = view;
        SmartyTextView name = (SmartyTextView) view.findViewById(R.id.name);
        SmartyTextView email = (SmartyTextView) view.findViewById(R.id.email);
        SmartyTextView phone = (SmartyTextView) view.findViewById(R.id.phone);
        SmartyTextView history = (SmartyTextView) view.findViewById(R.id.history);
        history.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), HistoryOperationActivity.class));
            }
        });
        User user = SettingsService.getUser(getActivity());
        name.setText(user.name);
        email.setText(user.email);
        phone.setText(user.phone);
        objectsListener.setObjects();
    }

    public void setBalance(String balance){
        SmartyTextView wallet = (SmartyTextView) rootView.findViewById(R.id.wallet);
        wallet.setText(balance + " com");
    }

    public void setData(Contact contact) {
        data = contact;
        SmartyTextView shopName = (SmartyTextView) rootView.findViewById(R.id.shop_name);
        shopName.setText("Нет магазина");

        if (contact.shop_name != null) {
            shopName.setText("Название: " + contact.shop_name);
        }

        if (contact.shop_email != null) {
            SmartyTextView shopEmail = (SmartyTextView) rootView.findViewById(R.id.shop_email);
            shopEmail.setText("Email: " + contact.shop_email);
        }

        if (contact.shop_phone != null) {
            SmartyTextView shopPhone = (SmartyTextView) rootView.findViewById(R.id.shop_phone);
            shopPhone.setText("Телефон: " + contact.shop_phone);
        }

        if (contact.shop_address != null) {
            SmartyTextView shopAddress = (SmartyTextView) rootView.findViewById(R.id.shop_address);
            shopAddress.setText("Адрес: " + contact.shop_address);
        }

        if (contact.shop_about != null) {
            SmartyTextView shopAbout = (SmartyTextView) rootView.findViewById(R.id.shop_about);
            shopAbout.setText("О нас: \n" + contact.shop_about);
        }

        if (contact.shop_banner != null) {
            SmartyImageView shopBanner = (SmartyImageView) rootView.findViewById(R.id.shop_banner);
            shopBanner.setImage(contact.shop_banner);
        }

        if (contact.shop_enabled != null) {
            CheckBox shopEnabled = (CheckBox) rootView.findViewById(R.id.shop_enabled);
            shopEnabled.setChecked("1".equals(contact.shop_enabled) ? true : false);
        }

        if (contact.remind != null) {
            CheckBox remind = (CheckBox) rootView.findViewById(R.id.remind);
            remind.setChecked("1".equals(contact.remind) ? true : false);
        }

        if (contact.notification != null) {
            CheckBox notification = (CheckBox) rootView.findViewById(R.id.notification);
            notification.setChecked("1".equals(contact.notification) ? true : false);
        }

        if (contact.subscribe != null) {
            CheckBox subscribe = (CheckBox) rootView.findViewById(R.id.subscribe);
            subscribe.setChecked("1".equals(contact.subscribe) ? true : false);
        }

        if (contact.shop_banner != null) {
            SmartyImageView banner = (SmartyImageView) rootView.findViewById(R.id.shop_banner);
            banner.setImage("http:" +contact.shop_banner);
        }
    }

    public Contact getData(){
        return data;
    }
}

package com.ebr163.ibikg.model.ads;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by Bakht on 22.03.2016.
 */
public class Ad {
    public String id;
    public String advert_name;
    public String price;
    public String date_publish;
    public String contact;
    public String region_name;
    public String name_currency;
    public String published;
    public List<Map<String, Object>> photos = new ArrayList<>();
}

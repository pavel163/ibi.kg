package com.ebr163.ibikg.model;

import com.google.gson.annotations.SerializedName;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Bakht on 26.03.2016.
 */
public class AdPostData {
    public String currency_id;
    public String advert_id;
    public String key;
    public String city_id;
    public String advert_name;
    public String address;
    public String advert_descr;
    public String is_negotiable;
    public String price;
    public String category_id;
    public String resource = "add_user_advert";
    public String app = "ibi";
    public String format = "raw";
    @SerializedName("slot-1")
    public String slot1;
    @SerializedName("slot-2")
    public String slot2;
    @SerializedName("slot-3")
    public String slot3;
    @SerializedName("slot-4")
    public String slot4;
    @SerializedName("slot-5")
    public String slot5;
    public Map<String, String> customFields = new HashMap<>();
}

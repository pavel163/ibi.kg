package com.ebr163.ibikg.adapter.category;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ebr163.ibikg.R;

import java.util.List;

/**
 * Created by Bakht on 16.04.2016.
 */
public class PayAdAdapter extends MyAdAdapter {

    public PayAdAdapter(List<Object> ads) {
        super(ads);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_my_ad, parent, false);
        return PayAdHolder.newInstance(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        AdHolder holder = (PayAdHolder) viewHolder;
        setData(holder, position);
    }
}

package com.ebr163.ibikg.model;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Bakht on 11.03.2016.
 */
public class Response<T> {
    public boolean success;
    public Map<String, T> data = new HashMap<>();
}

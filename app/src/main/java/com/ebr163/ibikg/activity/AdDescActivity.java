package com.ebr163.ibikg.activity;

import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.ebr163.ibikg.BaseApplication;
import com.ebr163.ibikg.C;
import com.ebr163.ibikg.R;
import com.ebr163.ibikg.activity.base.BaseActivityWithFragment;
import com.ebr163.ibikg.fragment.AdDescFragment;
import com.ebr163.ibikg.helper.ConvertHelper;
import com.ebr163.ibikg.model.Response;
import com.ebr163.ibikg.model.ads.AdDB;
import com.ebr163.ibikg.service.http.HttpApiFactory;

import rx.functions.Action1;
import rx.functions.Func1;

public class AdDescActivity extends BaseActivityWithFragment implements AdDescFragment.OnSetObjectsListener {

    private Menu menu;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.activity_ad_desc);
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void initUI() {
        super.initUI();
    }

    @Override
    protected void initFragment() {
        fragment = AdDescFragment.newInstance();
        getSupportFragmentManager().beginTransaction()
                .add(R.id.main_fragment_container, fragment)
                .commit();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        this.menu = menu;
        getMenuInflater().inflate(R.menu.menu_ad_desc, menu);
        if (isInDB()) {
            menu.getItem(1).setVisible(true);
        } else {
            menu.getItem(0).setVisible(true);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.favors_add) {
            saveAdvert();
            item.setVisible(false);
            menu.getItem(1).setVisible(true);
        }
        if (item.getItemId() == R.id.favors_delete) {
            deleteAdvert();
            item.setVisible(false);
            menu.getItem(0).setVisible(true);
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void setObjects() {
        Action1<Response<Object>> onNextAction = new Action1<Response<Object>>() {
            @Override
            public void call(Response<Object> response) {
                if (response.success == true) {
                    ((AdDescFragment) fragment).setData(ConvertHelper.adDescCreate(response.data));
                    Log.d("Загрузка объявления", "Успех");
                }
            }
        };

        Func1 error = new Func1() {
            @Override
            public Object call(Object o) {
                Log.d("Загрузка объявления", "Провал");
                return null;
            }
        };

        BaseApplication.instance.httpService.subscribeAction(HttpApiFactory.HttpMethodType.GET_AD, onNextAction, error, getIntent().getStringExtra(C.fields.parent_id));
    }

    private void saveAdvert() {
        BaseApplication.instance.crudService.put(ConvertHelper.adDBCreate(((AdDescFragment) fragment).getData()));
        Toast.makeText(this, "Добавлено в избранные", Toast.LENGTH_LONG).show();
    }

    private void deleteAdvert() {
        AdDB adDB = ConvertHelper.adDBCreate(((AdDescFragment) fragment).getData());
        BaseApplication.instance.crudService.deleteSubscribe(adDB.getClass(), adDB.id);
        Toast.makeText(this, "Удалено из избранного", Toast.LENGTH_LONG).show();
    }

    private boolean isInDB() {
        Object o = BaseApplication.instance.crudService.getAd(AdDB.class, getIntent().getStringExtra(C.fields.parent_id));
        if (o != null) {
            return true;
        }
        return false;
    }

    @Override
    protected void onStop() {
        super.onStop();
        BaseApplication.instance.httpService.unsubscribe();
    }
}

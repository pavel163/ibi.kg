package com.ebr163.ibikg.adapter.category;

import android.view.View;

import com.ebr163.ibikg.R;
import com.ebr163.ibikg.event.DataEvent;
import com.ebr163.ibikg.ui.SmartyImageView;
import com.ebr163.ibikg.ui.SmartyTextView;

import de.greenrobot.event.EventBus;

/**
 * Created by Bakht on 16.04.2016.
 */
public class PayAdHolder extends MyAdHolder {

    public static PayAdHolder newInstance(View parent) {
        SmartyImageView image = (SmartyImageView) parent.findViewById(R.id.image);
        SmartyTextView title = (SmartyTextView) parent.findViewById(R.id.title);
        SmartyTextView price = (SmartyTextView) parent.findViewById(R.id.price);
        SmartyTextView data = (SmartyTextView) parent.findViewById(R.id.data);
        SmartyTextView region = (SmartyTextView) parent.findViewById(R.id.region);
        SmartyTextView id = (SmartyTextView) parent.findViewById(R.id.id);
        SmartyTextView currency = (SmartyTextView) parent.findViewById(R.id.currency);
        SmartyTextView publish = (SmartyTextView) parent.findViewById(R.id.published);
        return new PayAdHolder(parent, image, title, price, data, region, id, currency, publish);
    }

    public PayAdHolder(View itemView, SmartyImageView image, SmartyTextView title, SmartyTextView price,
                       SmartyTextView data, SmartyTextView region, SmartyTextView id, SmartyTextView currency, SmartyTextView publish) {
        super(itemView, image, title, price, data, region, id, currency, publish);
        this.publish = publish;
    }

    @Override
    public void onClick(View v) {
        EventBus.getDefault().post(new DataEvent(id.getText().toString()));
    }
}

package com.ebr163.ibikg.helper;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;

import com.ebr163.ibikg.C;
import com.ebr163.ibikg.model.ads.AdDB;
import com.ebr163.ibikg.model.ads.AdDesc;

import java.io.ByteArrayOutputStream;
import java.util.List;
import java.util.Map;

/**
 * Created by Bakht on 25.03.2016.
 */
public class ConvertHelper {

    public static AdDB adDBCreate(AdDesc adDesc) {
        AdDB adDB = new AdDB();
        adDB.id = adDesc.id;
        adDB.advert_name = adDesc.advert_name;
        adDB.name_currency = adDesc.name_currency;
        adDB.contact = adDesc.contact;
        adDB.date_publish = adDesc.date_publish;
        adDB.photo = adDesc.photos.get(0).get(C.advert.photo_path);
        adDB.price = adDesc.price;
        adDB.name_currency = adDesc.name_currency;
        adDB.region_name = adDesc.city_name;
        return adDB;
    }

    public static AdDesc adDescCreate(Map<String, Object> data) {
        //TODO Возложить эту хрень на Retrofit2
        AdDesc adDesc = new AdDesc();
        adDesc.advert_name = data.get(C.advert.advert_name).toString().trim();
        adDesc.advert_descr = data.get(C.advert.advert_descr).toString().trim();
        adDesc.address = data.get(C.advert.address).toString().trim();
        if (data.get(C.advert.price) != null){
            adDesc.price = data.get(C.advert.price).toString().trim();
            adDesc.name_currency = data.get(C.advert.name_currency).toString().trim();
        } else {
            adDesc.price = "";
            adDesc.name_currency = "";
        }

        adDesc.city_name = data.get(C.advert.city_name).toString().trim();
        adDesc.ibi_name = data.get(C.advert.ibi_name).toString().trim();
        adDesc.advert_phone = data.get(C.advert.advert_phone).toString().trim();
        adDesc.city_id = data.get(C.advert.city_id).toString().trim();
        adDesc.id = data.get(C.advert.id).toString().trim();
        adDesc.user_id = data.get(C.advert.user_id).toString().trim();
        adDesc.photos = (List<Map<String, String>>) data.get(C.advert.photos);
        adDesc.date_publish = data.get(C.advert.date_publish).toString().trim();
        adDesc.category_id = data.get(C.advert.category_id).toString().trim();
        adDesc.contact = data.get(C.advert.contact).toString().trim();
        adDesc.currency_id = data.get(C.advert.currency_id).toString().trim();
        adDesc.hits = data.get(C.advert.hits).toString().trim();
        adDesc.advert_url = data.get(C.advert.advert_url).toString().trim();
        if (data.get(C.advert.published) != null) {
            adDesc.published = data.get(C.advert.published).toString().trim();
        }
        if (data.get(C.advert.mode_highlight_until) != null) {
            adDesc.mode_highlight_until = data.get(C.advert.mode_highlight_until).toString();
        }
        if (data.get(C.advert.mode_premium_until) != null) {
            adDesc.mode_premium_until = data.get(C.advert.mode_premium_until).toString();
        }
        if (data.get(C.advert.mode_vip_until) != null) {
            adDesc.mode_vip_until = data.get(C.advert.mode_vip_until).toString();
        }
        adDesc.name_category = data.get(C.advert.name_category).toString().trim();
        adDesc.published = data.get(C.advert.published).toString().trim();
        adDesc.shop_enabled = data.get(C.advert.shop_enabled).toString().trim();
        adDesc.advert_custom_values = (List<Map<String, Object>>) data.get(C.advert.advert_custom_values);
        return adDesc;
    }

    public static String encodeToBase64(Bitmap image, Bitmap.CompressFormat compressFormat, int quality) {
        ByteArrayOutputStream byteArrayOS = new ByteArrayOutputStream();
        image.compress(compressFormat, quality, byteArrayOS);
        return "data:image/jpeg;base64,"+Base64.encodeToString(byteArrayOS.toByteArray(), Base64.DEFAULT);
    }

    public static Bitmap decodeBase64(String input) {
        byte[] decodedBytes = Base64.decode(input, 0);
        return BitmapFactory.decodeByteArray(decodedBytes, 0, decodedBytes.length);
    }

}

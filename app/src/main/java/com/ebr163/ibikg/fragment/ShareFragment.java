package com.ebr163.ibikg.fragment;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestListener;
import com.ebr163.ibikg.C;
import com.ebr163.ibikg.R;
import com.ebr163.ibikg.activity.CityActivity;
import com.ebr163.ibikg.activity.MainActivity;
import com.ebr163.ibikg.activity.RegionActivity;
import com.ebr163.ibikg.event.FakeEvent;
import com.ebr163.ibikg.event.LoadCustomFieldsEvent;
import com.ebr163.ibikg.event.PostAdvertEvent;
import com.ebr163.ibikg.fragment.base.BaseFragment;
import com.ebr163.ibikg.helper.BitmapHelper;
import com.ebr163.ibikg.helper.ConvertHelper;
import com.ebr163.ibikg.model.AdPostData;
import com.ebr163.ibikg.model.ads.AdDesc;
import com.ebr163.ibikg.service.SettingsService;
import com.ebr163.ibikg.ui.SmartyImageView;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.greenrobot.event.EventBus;

/**
 * Created by Bakht on 20.03.2016.
 */
public class ShareFragment extends BaseFragment implements View.OnClickListener {

    protected TextInputLayout city;
    protected TextInputLayout region;
    protected TextInputLayout title;
    protected TextInputLayout description;
    protected TextInputLayout address;
    protected TextInputLayout curs;
    protected TextInputLayout price;
    protected CheckBox negotiable;
    protected Button share;
    protected SmartyImageView image1;
    protected SmartyImageView image2;
    protected SmartyImageView image3;
    protected SmartyImageView image4;
    protected SmartyImageView image5;
    protected SmartyImageView image6;
    protected List<Map<String, Object>> data;
    protected AdDesc adDesc = null;
    protected AdPostData postAdvert = new AdPostData();
    protected Map<String, View> customFields = new HashMap<>();
    protected View rootView;

    public static ShareFragment newInstance(String id, int step) {
        Bundle args = new Bundle();
        args.putString(C.fields.parent_id, id);
        args.putInt(C.fields.step, step);
        ShareFragment shareFragment = new ShareFragment();
        shareFragment.setArguments(args);
        return shareFragment;
    }

    public static ShareFragment newInstance(AdDesc adDesc) {
        Bundle args = new Bundle();
        args.putSerializable(C.fields.data, adDesc);
        ShareFragment shareFragment = new ShareFragment();
        shareFragment.setArguments(args);
        return shareFragment;
    }

    @Override
    public int getLayout() {
        return R.layout.fragment_share;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EventBus.getDefault().post(new LoadCustomFieldsEvent());
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        adDesc = (AdDesc) getArguments().getSerializable(C.fields.data);
        if (adDesc != null) {
            getArguments().putString(C.fields.parent_id, adDesc.category_id);
            getArguments().putString(C.location.city_id, adDesc.city_id);
            getArguments().putString(C.advert.currency_id, adDesc.currency_id);
            getArguments().putString(C.advert.advert_id, adDesc.id);
        }
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    protected void initUI(View view) {
        this.rootView = view;

        region = (TextInputLayout) view.findViewById(R.id.region);
        region.getEditText().setOnClickListener(this);

        city = (TextInputLayout) view.findViewById(R.id.city);
        city.getEditText().setOnClickListener(this);

        if (adDesc != null) {
            city.getEditText().setText(adDesc.city_name);
        }

        title = (TextInputLayout) view.findViewById(R.id.title);
        title.getEditText().setOnClickListener(this);

        if (adDesc != null) {
            title.getEditText().setText(adDesc.advert_name);
        }

        description = (TextInputLayout) view.findViewById(R.id.description);
        description.getEditText().setOnClickListener(this);

        if (adDesc != null) {
            description.getEditText().setText(adDesc.advert_descr);
        }

        address = (TextInputLayout) view.findViewById(R.id.address);
        address.getEditText().setOnClickListener(this);

        if (adDesc != null) {
            address.getEditText().setText(adDesc.address);
        }

        curs = (TextInputLayout) view.findViewById(R.id.curs);
        curs.getEditText().setOnClickListener(this);

        if (adDesc != null) {
            curs.getEditText().setText(adDesc.name_currency);
        }

        price = (TextInputLayout) view.findViewById(R.id.price);
        price.getEditText().setOnClickListener(this);

        if (adDesc != null) {
            price.getEditText().setText(adDesc.price);
        }

        negotiable = (CheckBox) view.findViewById(R.id.negotiable);

        negotiable.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    price.getEditText().setText("");
                    price.getEditText().setEnabled(false);
                } else {
                    price.getEditText().setEnabled(true);
                }
            }
        });

        share = (Button) view.findViewById(R.id.share);
        share.setOnClickListener(this);

        //TODO Это УГ нужно убрать
        image1 = (SmartyImageView) view.findViewById(R.id.image1);
        image1.setOnClickListener(this);

        image2 = (SmartyImageView) view.findViewById(R.id.image2);
        image2.setOnClickListener(this);

        image3 = (SmartyImageView) view.findViewById(R.id.image3);
        image3.setOnClickListener(this);

        image4 = (SmartyImageView) view.findViewById(R.id.image4);
        image4.setOnClickListener(this);

        image5 = (SmartyImageView) view.findViewById(R.id.image5);
        image5.setOnClickListener(this);

        image6 = (SmartyImageView) view.findViewById(R.id.image6);

        if (adDesc != null)
            if (adDesc.photos.size() != 0) {
                //1
                if (adDesc.photos.size() > 0 && adDesc.photos.get(0) != null) {
                    RequestListener<String, Bitmap> requestListener = new RequestListener<String, Bitmap>() {
                        @Override
                        public boolean onException(Exception e, String model, com.bumptech.glide.request.target.Target<Bitmap> target, boolean isFirstResource) {
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(Bitmap resource, String model, com.bumptech.glide.request.target.Target<Bitmap> target, boolean isFromMemoryCache, boolean isFirstResource) {
                            image1.setImageBitmap(resource);
                            Bitmap scaledBitmap = BitmapHelper.scaleDown(resource, 300, true);
                            postAdvert.slot1 = ConvertHelper.encodeToBase64(scaledBitmap, Bitmap.CompressFormat.PNG, 100);
                            image2.setVisibility(View.VISIBLE);
                            return false;
                        }
                    };

                    Glide.with(this)
                            .load("http:" + adDesc.photos.get(0).get("photo_path"))
                            .asBitmap()
                            .listener(requestListener)
                            .preload();
                }//2
                if (adDesc.photos.size() > 1 && adDesc.photos.get(1) != null) {
                    image2.setVisibility(View.VISIBLE);
                    image3.setVisibility(View.VISIBLE);
                    image4.setVisibility(View.INVISIBLE);

                    RequestListener<String, Bitmap> requestListener = new RequestListener<String, Bitmap>() {
                        @Override
                        public boolean onException(Exception e, String model, com.bumptech.glide.request.target.Target<Bitmap> target, boolean isFirstResource) {
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(Bitmap resource, String model, com.bumptech.glide.request.target.Target<Bitmap> target, boolean isFromMemoryCache, boolean isFirstResource) {
                            image2.setImageBitmap(resource);
                            Bitmap scaledBitmap = BitmapHelper.scaleDown(resource, 300, true);
                            postAdvert.slot2 = ConvertHelper.encodeToBase64(scaledBitmap, Bitmap.CompressFormat.PNG, 100);
                            return false;
                        }
                    };

                    Glide.with(this)
                            .load("http:" + adDesc.photos.get(1).get("photo_path"))
                            .asBitmap()
                            .listener(requestListener)
                            .preload();
                }//3
                if (adDesc.photos.size() > 2 && adDesc.photos.get(2) != null) {
                    image3.setVisibility(View.VISIBLE);
                    image4.setVisibility(View.INVISIBLE);

                    RequestListener<String, Bitmap> requestListener = new RequestListener<String, Bitmap>() {
                        @Override
                        public boolean onException(Exception e, String model, com.bumptech.glide.request.target.Target<Bitmap> target, boolean isFirstResource) {
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(Bitmap resource, String model, com.bumptech.glide.request.target.Target<Bitmap> target, boolean isFromMemoryCache, boolean isFirstResource) {
                            image3.setImageBitmap(resource);
                            Bitmap scaledBitmap = BitmapHelper.scaleDown(resource, 300, true);
                            postAdvert.slot3 = ConvertHelper.encodeToBase64(scaledBitmap, Bitmap.CompressFormat.PNG, 100);
                            return false;
                        }
                    };

                    Glide.with(this)
                            .load("http:" + adDesc.photos.get(2).get("photo_path"))
                            .asBitmap()
                            .listener(requestListener)
                            .preload();
                }//4
                if (adDesc.photos.size() > 3 && adDesc.photos.get(3) != null) {
                    image4.setVisibility(View.VISIBLE);
                    image5.setVisibility(View.VISIBLE);
                    image6.setVisibility(View.INVISIBLE);

                    RequestListener<String, Bitmap> requestListener = new RequestListener<String, Bitmap>() {
                        @Override
                        public boolean onException(Exception e, String model, com.bumptech.glide.request.target.Target<Bitmap> target, boolean isFirstResource) {
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(Bitmap resource, String model, com.bumptech.glide.request.target.Target<Bitmap> target, boolean isFromMemoryCache, boolean isFirstResource) {
                            image4.setImageBitmap(resource);
                            Bitmap scaledBitmap = BitmapHelper.scaleDown(resource, 300, true);
                            postAdvert.slot4 = ConvertHelper.encodeToBase64(scaledBitmap, Bitmap.CompressFormat.PNG, 100);
                            return false;
                        }
                    };

                    Glide.with(this)
                            .load("http:" + adDesc.photos.get(3).get("photo_path"))
                            .asBitmap()
                            .listener(requestListener)
                            .preload();
                }//5
                if (adDesc.photos.size() > 4 && adDesc.photos.get(4) != null) {
                    image5.setVisibility(View.VISIBLE);
                    image6.setVisibility(View.INVISIBLE);

                    RequestListener<String, Bitmap> requestListener = new RequestListener<String, Bitmap>() {
                        @Override
                        public boolean onException(Exception e, String model, com.bumptech.glide.request.target.Target<Bitmap> target, boolean isFirstResource) {
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(Bitmap resource, String model, com.bumptech.glide.request.target.Target<Bitmap> target, boolean isFromMemoryCache, boolean isFirstResource) {
                            image5.setImageBitmap(resource);
                            Bitmap scaledBitmap = BitmapHelper.scaleDown(resource, 300, true);
                            postAdvert.slot5 = ConvertHelper.encodeToBase64(scaledBitmap, Bitmap.CompressFormat.PNG, 100);
                            return false;
                        }
                    };

                    Glide.with(this)
                            .load("http:" + adDesc.photos.get(4).get("photo_path"))
                            .asBitmap()
                            .listener(requestListener)
                            .preload();
                }
            }
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == -1) {
            id = ((View) v.getParent()).getId();
        }
        Intent i = new Intent(Intent.ACTION_PICK);
        i.setType("image/*");
        switch (id) {
            case R.id.image1:
                startActivityForResult(i, 1);
                break;
            case R.id.image2:
                startActivityForResult(i, 2);
                break;
            case R.id.image3:
                startActivityForResult(i, 3);
                break;
            case R.id.image4:
                startActivityForResult(i, 4);
                break;
            case R.id.image5:
                startActivityForResult(i, 5);
                break;
            case R.id.city:
                String region = getArguments().getString(C.location.region_id);
                Intent cityIntent = new Intent(getActivity(), CityActivity.class);
                if (region != null) {
                    cityIntent.putExtra(C.location.region_id, Integer.parseInt(region));
                }
                startActivityForResult(cityIntent, 7);
                break;
            case R.id.curs:
                EventBus.getDefault().post(new FakeEvent());
                break;
            case R.id.region:
                city.getEditText().setText(null);
                getArguments().putString(C.location.city_id, "0");
                startActivityForResult(new Intent(getActivity(), RegionActivity.class), 6);
                break;
            case R.id.share:
                postAdvert.currency_id = getArguments().getString(C.advert.currency_id);
                postAdvert.key = SettingsService.getUser(getActivity()).key;
                postAdvert.advert_name = title.getEditText().getText().toString();
                postAdvert.advert_descr = description.getEditText().getText().toString();
                postAdvert.address = address.getEditText().getText().toString();
                postAdvert.category_id = getArguments().getString(C.fields.parent_id);
                postAdvert.city_id = getArguments().getString(C.location.city_id);
                postAdvert.price = price.getEditText().getText().toString();
                postAdvert.is_negotiable = negotiable.isChecked() ? "1" : "0";
                postAdvert.advert_id = getArguments().getString(C.advert.advert_id);
                postAdvert.customFields = getCustomFields();
                EventBus.getDefault().post(new PostAdvertEvent(postAdvert));
        }
    }

    public void startActivity(){
        Intent intent = new Intent(getActivity(), MainActivity.class);
        intent.putExtra("fragment", 4);
        startActivity(intent);
        getActivity().finish();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 6) {
            region.getEditText().setText(data.getStringExtra(C.location.region_name));
            getArguments().putString(C.location.region_id, data.getStringExtra(C.location.region_id));
            return;
        }
        if (requestCode == 7) {
            city.getEditText().setText(data.getStringExtra(C.location.city_name));
            getArguments().putString(C.location.city_id, data.getStringExtra(C.location.city_id));
            return;
        }

        if (requestCode == 1 && resultCode == Activity.RESULT_OK) {
            postAdvert.slot1 = setBitmap(image1, data);
        }
        if (requestCode == 2 && resultCode == Activity.RESULT_OK) {
            postAdvert.slot2 = setBitmap(image2, data);
        }
        if (requestCode == 3 && resultCode == Activity.RESULT_OK) {
            postAdvert.slot3 = setBitmap(image3, data);
        }
        if (requestCode == 4 && resultCode == Activity.RESULT_OK) {
            postAdvert.slot4 = setBitmap(image4, data);
        }
        if (requestCode == 5 && resultCode == Activity.RESULT_OK) {
            postAdvert.slot5 = setBitmap(image5, data);
        }

        if (requestCode < 5) {
            if (data != null){
                setVisibleImageView(requestCode + 1);
            }
        }

        if (requestCode == 11 && resultCode == Activity.RESULT_OK) {
            for (int i = 0; i < this.data.size(); i++) {
                String currency = data.getStringExtra(C.fields.data);
                if (currency.equals(this.data.get(i).get(C.fields.name_currency).toString())) {
                    getArguments().putString(C.advert.currency_id, this.data.get(i).get(C.fields.id).toString());
                    this.curs.getEditText().setText(currency);
                }
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void setVisibleImageView(int requestCode) {
        if (requestCode % 2 != 0) {
            getImageView(requestCode).setVisibility(View.VISIBLE);
            getImageView(requestCode + 1).setVisibility(View.INVISIBLE);
        } else {
            getImageView(requestCode).setVisibility(View.VISIBLE);
        }
    }

    private ImageView getImageView(int number) {
        switch (number) {
            case 1:
                return image1;
            case 2:
                return image2;
            case 3:
                return image3;
            case 4:
                return image4;
            case 5:
                return image5;
            case 6:
                return image6;
        }
        return null;
    }

    private String setBitmap(SmartyImageView image, Intent data) {
        Uri selectedImage = data.getData();
        image.setImage(selectedImage);
        return ConvertHelper.encodeToBase64(getBitmap(selectedImage), Bitmap.CompressFormat.JPEG, 100);
    }

    public void setData(Object data) {
        this.data = (List<Map<String, Object>>) data;
    }


    private Bitmap getBitmap(Uri uri) {
        InputStream in = null;
        try {
            ContentResolver resolver = getActivity().getContentResolver();
            final int IMAGE_MAX_SIZE = 300000;
            in = resolver.openInputStream(uri);
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(in, null, o);
            in.close();
            int scale = 1;
            while ((o.outWidth * o.outHeight) * (1 / Math.pow(scale, 2)) >
                    IMAGE_MAX_SIZE) {
                scale++;
            }

            Bitmap b = null;
            in = resolver.openInputStream(uri);
            if (scale > 1) {
                scale--;
                // scale to max possible inSampleSize that still yields an image
                // larger than target
                o = new BitmapFactory.Options();
                o.inSampleSize = scale;
                b = BitmapFactory.decodeStream(in, null, o);

                // resize to desired dimensions
                int height = b.getHeight();
                int width = b.getWidth();

                double y = Math.sqrt(IMAGE_MAX_SIZE
                        / (((double) width) / height));
                double x = (y / height) * width;

                Bitmap scaledBitmap = Bitmap.createScaledBitmap(b, (int) x,
                        (int) y, true);
                b.recycle();
                b = scaledBitmap;

                System.gc();
            } else {
                b = BitmapFactory.decodeStream(in);
            }
            in.close();
            return b;
        } catch (IOException e) {
            Log.e("error", e.getMessage(), e);
            return null;
        }
    }

    public void setCustomFields(List<Object> customFields){
        for (Object object: customFields){
            Map<String, Object> map = (Map<String, Object>) object;
            createCustomField(map);
        }
    }

    private void createCustomField(Map<String, Object> map){
        LayoutInflater lInflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        switch (map.get(C.custom_fields.custom_type).toString()){
            case "1":
                TextInputLayout editText = (TextInputLayout) lInflater.inflate(R.layout.custom_text_input_layout, null);
                editText.setHint(map.get(C.custom_fields.custom_name).toString());
                ((ViewGroup)rootView.findViewById(R.id.custom_fields)).addView(editText);
                customFields.put(map.get(C.custom_fields.id).toString(), editText);
                break;
            case "2":
                RelativeLayout spinner = (RelativeLayout) lInflater.inflate(R.layout.custom_spinner, null);
                String[] option = map.get(C.custom_fields.custom_options).toString().split(";");
                ArrayList<String> o = new ArrayList<>();
                o.add("Не выбрано");
                List<String> splitList = Arrays.asList(option);
                for (int i = 0; i<splitList.size(); i++){
                    o.add(splitList.get(i).trim());
                }
                ArrayAdapter<String> adapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_item, o);
                ((Spinner) spinner.findViewById(R.id.spinner)).setAdapter(adapter);
                ((TextView) spinner.findViewById(R.id.title)).setText(map.get(C.custom_fields.custom_name).toString());
                ((ViewGroup) rootView.findViewById(R.id.custom_fields)).addView(spinner);
                customFields.put(map.get(C.custom_fields.id).toString(), spinner.findViewById(R.id.spinner));
                break;
            case "3":
                CheckBox checkBox = (CheckBox) lInflater.inflate(R.layout.custom_check_box, null);
                checkBox.setText(map.get(C.custom_fields.custom_name).toString());
                ((ViewGroup)rootView.findViewById(R.id.custom_fields)).addView(checkBox);
                customFields.put(map.get(C.custom_fields.id).toString(), checkBox);
                break;
        }
    }

    public Map<String, String> getCustomFields() {
        Map<String, String> result = new HashMap<>();
        for (Map.Entry<String, View> entry : customFields.entrySet()) {
            String value = null;
            View view = entry.getValue();
            if (view instanceof TextInputLayout) {
                if (!"".equals(((TextInputLayout) view).getEditText().getText().toString()))
                    result.put("custom" + entry.getKey().trim(), ((TextInputLayout) view).getEditText().getText().toString());
            } else if (view instanceof Spinner) {
                if (!((Spinner) view).getSelectedItem().toString().equals("Не выбрано")) {
                    result.put("custom" + entry.getKey().trim(), ((Spinner) view).getSelectedItem().toString());
                }
            } else {
                result.put("custom" + entry.getKey().trim(), ((CheckBox) view).isChecked() ? "Да" : "Нет");
            }
        }

        return result;
    }
}

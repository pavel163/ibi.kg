package com.ebr163.ibikg.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.BaseAdapter;

import com.ebr163.ibikg.C;
import com.ebr163.ibikg.R;
import com.ebr163.ibikg.adapter.CityAdapter;
import com.ebr163.ibikg.fragment.base.BaseFragmentWithListView;

import java.util.List;
import java.util.Map;

/**
 * Created by Bakht on 26.03.2016.
 */
public class CityFragment extends BaseFragmentWithListView {

    public static CityFragment newInstance() {
        return new CityFragment();
    }

    @Override
    protected BaseAdapter getAdapter(List<Object> objectList) {
        return new CityAdapter(getActivity(), objectList);
    }

    @Override
    protected void clickItem(int position){
        Intent intent = new Intent();
        intent.putExtra(C.location.city_id, ((Map<String, String>) objectList.get(position)).get(C.location.id));
        intent.putExtra(C.location.city_name, ((Map<String, String>) objectList.get(position)).get(C.location.city_name));
        getActivity().setResult(getActivity().RESULT_OK, intent);
        getActivity().finish();
    }
}

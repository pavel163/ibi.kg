package com.ebr163.ibikg.activity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.ebr163.ibikg.BaseApplication;
import com.ebr163.ibikg.C;
import com.ebr163.ibikg.R;
import com.ebr163.ibikg.activity.base.BaseActivityWithFragment;
import com.ebr163.ibikg.fragment.CategoryFragment;
import com.ebr163.ibikg.fragment.base.BaseFragmentWithListView;
import com.ebr163.ibikg.model.Category;
import com.ebr163.ibikg.model.Response;
import com.ebr163.ibikg.service.http.HttpApiFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import rx.functions.Action1;
import rx.functions.Func1;

public class CategoryActivity extends BaseActivityWithFragment implements BaseFragmentWithListView.OnSetObjectsListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.activity_with_fragment);
        super.onCreate(savedInstanceState);
        setToolbarTitle(getIntent().getStringExtra(C.fields.title));
    }

    @Override
    protected void initFragment() {
        fragment = CategoryFragment.newInstance(getIntent().getStringExtra(C.fields.parent_id));
        getSupportFragmentManager().beginTransaction()
                .add(R.id.main_fragment_container, fragment)
                .commit();
    }

    @Override
    public void setObjects(Object params) {
        Action1<Response<Category>> onNextAction = new Action1<Response<Category>>() {
            @Override
            public void call(Response<Category> response) {
                if (response != null) {
                    List<Object> categories = new ArrayList<>();

                    for (Map.Entry<String, Category> entry : response.data.entrySet()) {
                        categories.add(entry.getValue());
                    }
                    ((BaseFragmentWithListView) fragment).setObjectList(categories);
                    ((BaseFragmentWithListView) fragment).setupListView(categories);
                    Log.d("Загрузка категорий", "Успех");
                }
            }
        };

        Func1 error = new Func1() {
            @Override
            public Object call(Object o) {
                ((BaseFragmentWithListView) fragment).setVisibleErrorMessage(View.VISIBLE);
                ((BaseFragmentWithListView) fragment).setVisibleProgress(View.GONE);
                Log.d("Загрузка категорий", "Провал");
                return null;
            }
        };

        BaseApplication.instance.httpService.subscribeAction(HttpApiFactory.HttpMethodType.GET_CATEGORY, onNextAction, error, ((CategoryFragment) fragment).getParentId());
    }

    @Override
    protected void onStop() {
        super.onStop();
        BaseApplication.instance.httpService.unsubscribe();
    }
}

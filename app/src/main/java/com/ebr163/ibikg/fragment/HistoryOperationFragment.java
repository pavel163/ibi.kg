package com.ebr163.ibikg.fragment;

import android.widget.BaseAdapter;

import com.ebr163.ibikg.adapter.HistoryAdapter;
import com.ebr163.ibikg.fragment.base.BaseFragmentWithListView;

import java.util.List;

/**
 * Created by Bakht on 17.04.2016.
 */
public class HistoryOperationFragment extends BaseFragmentWithListView {

    public static HistoryOperationFragment newInstance() {
        return new HistoryOperationFragment();
    }

    @Override
    protected BaseAdapter getAdapter(List<Object> objectList) {
        return new HistoryAdapter(getActivity(), objectList);
    }

    @Override
    protected void clickItem(int position) {

    }
}

package com.ebr163.ibikg.event;

import com.ebr163.ibikg.event.base.BaseEvent;

/**
 * Created by Bakht on 10.04.2016.
 */
public class OffsetEvent extends BaseEvent {

    public final int offset;
    public String type;

    public OffsetEvent(int offset) {
        this.offset = offset;
    }
}

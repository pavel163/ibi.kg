package com.ebr163.ibikg.ui.autoloadingrecycler;

/**
 * Created by Bakht on 09.04.2016.
 */
public class OffsetAndLimit {
    private int offset;
    private int limit;

    public OffsetAndLimit(int offset, int limit) {
        this.offset = offset;
        this.limit = limit;
    }

    public int getOffset() {
        return offset;
    }

    public int getLimit() {
        return limit;
    }
}

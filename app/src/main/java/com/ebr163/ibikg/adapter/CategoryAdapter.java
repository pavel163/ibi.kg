package com.ebr163.ibikg.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.ebr163.ibikg.R;
import com.ebr163.ibikg.model.Category;
import com.ebr163.ibikg.ui.SmartyImageView;
import com.ebr163.ibikg.ui.SmartyTextView;

import java.util.List;

/**
 * Created by Bakht on 12.03.2016.
 */
public class CategoryAdapter extends BaseAdapter {

    protected List<Object> categories;
    protected LayoutInflater lInflater;
    protected Context context;

    public CategoryAdapter(Context context, List<Object> categories) {
        this.categories = categories;
        this.context = context;
        lInflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return categories.size();
    }

    @Override
    public Object getItem(int position) {
        return categories.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View rowView = convertView;
        // reuse views
        if (rowView == null) {
            rowView = lInflater.inflate(R.layout.item_category, null);
            ViewHolder viewHolder = new ViewHolder();
            viewHolder.image = (SmartyImageView) rowView.findViewById(R.id.image);
            viewHolder.title = (SmartyTextView) rowView.findViewById(R.id.title);
            viewHolder.id = (TextView) rowView.findViewById(R.id.id);
            viewHolder.order = (TextView) rowView.findViewById(R.id.order);
            rowView.setTag(viewHolder);
        }

        ViewHolder holder = (ViewHolder) rowView.getTag();
        Category category = (Category) categories.get(position);
        if (category.icon_path != null){
            holder.image.setImage("http:"+category.icon_path);
        } else {
            holder.image.setVisibility(View.GONE);
        }
        holder.title.setText(category.name_category);
        holder.id.setText(category.id);
        holder.order.setText(category.order);

        return rowView;
    }

    static class ViewHolder {
        public SmartyImageView image;
        public SmartyTextView title;
        public TextView id;
        public TextView order;
    }
}

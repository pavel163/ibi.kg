package com.ebr163.ibikg.fragment.login;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.view.View;
import android.widget.Button;

import com.ebr163.ibikg.R;
import com.ebr163.ibikg.activity.login.SignUpActivity;
import com.ebr163.ibikg.event.LoginEvent;
import com.ebr163.ibikg.fragment.base.BaseFragment;
import com.ebr163.ibikg.service.SettingsService;
import com.ebr163.ibikg.ui.SmartyTextView;
import com.ebr163.ibikg.util.ValidationUtils;

import de.greenrobot.event.EventBus;

/**
 * Created by Bakht on 16.03.2016.
 */
public class SignInFragment extends BaseFragment implements View.OnClickListener{

    private TextInputLayout email;
    private TextInputLayout password;

    public static SignInFragment newInstance() {
        return new SignInFragment();
    }

    @Override
    public int getLayout() {
        return R.layout.fragment_sign_in;
    }

    @Override
    protected void initUI(View view) {
        email = (TextInputLayout) view.findViewById(R.id.email);
        email.getEditText().setText(SettingsService.getUserEmail(getActivity()));
        password = (TextInputLayout) view.findViewById(R.id.password);
        Button signIn = (Button)view.findViewById(R.id.sign_in);
        signIn.setOnClickListener(this);

        Button signUp = (Button)view.findViewById(R.id.sign_up);
        signUp.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        Intent intent = null;
        switch (v.getId()){
            case R.id.sign_in:
                String email = ValidationUtils.getFieldValue(this.email);
                String password = ValidationUtils.getFieldValue(this.password);
                if (email != null && password != null){
                    EventBus.getDefault().post(new LoginEvent(email, password));
                }
                break;
            case R.id.sign_up:
                intent = new Intent(getActivity(), SignUpActivity.class);
                startActivity(intent);
                break;
            default:
                break;
        }
    }
}

package com.ebr163.ibikg.activity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.ebr163.ibikg.BaseApplication;
import com.ebr163.ibikg.R;
import com.ebr163.ibikg.activity.base.BaseActivityWithFragment;
import com.ebr163.ibikg.fragment.RegionFragment;
import com.ebr163.ibikg.fragment.base.BaseFragmentWithList;
import com.ebr163.ibikg.fragment.base.BaseFragmentWithListView;
import com.ebr163.ibikg.model.ResponseList;
import com.ebr163.ibikg.service.http.HttpApiFactory;

import rx.functions.Action1;
import rx.functions.Func1;

public class RegionActivity extends BaseActivityWithFragment implements BaseFragmentWithList.OnSetObjectsListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.activity_with_fragment);
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void initFragment() {
        fragment = RegionFragment.newInstance();
        getSupportFragmentManager().beginTransaction()
                .add(R.id.main_fragment_container, fragment)
                .commit();
    }

    @Override
    public void setObjects(Object params) {
        Action1<ResponseList<Object>> onNextAction = new Action1<ResponseList<Object>>() {
            @Override
            public void call(ResponseList<Object> response) {
                if (response != null) {
                    ((BaseFragmentWithListView) fragment).setObjectList(response.data);
                    ((BaseFragmentWithListView) fragment).setupListView(response.data);
                    Log.d("Загрузка регионов", "Успех");
                }
            }
        };

        Func1 error = new Func1() {
            @Override
            public Object call(Object o) {
                ((BaseFragmentWithListView) fragment).setVisibleErrorMessage(View.VISIBLE);
                ((BaseFragmentWithListView) fragment).setVisibleProgress(View.GONE);
                Log.d("Загрузка регионов", "Провал");
                return null;
            }
        };

        BaseApplication.instance.httpService.subscribeAction(HttpApiFactory.HttpMethodType.GET_REGIONS, onNextAction, error, null);
    }

    @Override
    protected void onStop() {
        super.onStop();
        BaseApplication.instance.httpService.unsubscribe();
    }
}

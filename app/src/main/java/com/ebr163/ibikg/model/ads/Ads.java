package com.ebr163.ibikg.model.ads;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Bakht on 22.03.2016.
 */
public class Ads<T> {
    public boolean success;
    public int total_items_count;
    public Map<String, String> total_customs = new HashMap<>();
    public List<Ad> data = new ArrayList<>();
}

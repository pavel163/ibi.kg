package com.ebr163.ibikg.util;

import android.support.design.widget.TextInputLayout;

/**
 * Created by Bakht on 18.03.2016.
 */
public class ValidationUtils {

    public static String getFieldValue(TextInputLayout textInputLayout){
        String value = textInputLayout.getEditText().getText().toString();
        if (value == null || "".equals(value)){
            textInputLayout.setErrorEnabled(true);
            textInputLayout.setError("Пустое поле");
            return null;
        } else {
            textInputLayout.setErrorEnabled(false);
        }
        return value;
    }
}

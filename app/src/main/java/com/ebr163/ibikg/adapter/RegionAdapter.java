package com.ebr163.ibikg.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.ebr163.ibikg.C;
import com.ebr163.ibikg.R;
import com.ebr163.ibikg.ui.SmartyTextView;

import java.util.List;
import java.util.Map;

/**
 * Created by Bakht on 02.04.2016.
 */
public class RegionAdapter extends BaseAdapter {

    protected List<Object> regions;
    protected LayoutInflater lInflater;
    protected Context context;

    public RegionAdapter(Context context, List<Object> objectList) {
        this.regions = objectList;
        this.context = context;
        lInflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return regions.size();
    }

    @Override
    public Object getItem(int position) {
        return regions.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View rowView = convertView;
        if (rowView == null) {
            rowView = lInflater.inflate(R.layout.item_region, null);
            ViewHolder viewHolder = new ViewHolder();
            viewHolder.title = (SmartyTextView) rowView.findViewById(R.id.title);
            viewHolder.id = (SmartyTextView) rowView.findViewById(R.id.id);
            rowView.setTag(viewHolder);
        }

        ViewHolder holder = (ViewHolder) rowView.getTag();
        Map<String, Object> map = (Map<String, Object>) regions.get(position);
        holder.title.setText(map.get(C.location.region_name).toString());
        holder.id.setText(map.get(C.location.id).toString());
        return rowView;
    }

    static class ViewHolder {
        public SmartyTextView title;
        public SmartyTextView id;
    }
}

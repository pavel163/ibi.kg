package com.ebr163.ibikg.fragment.dialog;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;

import com.ebr163.ibikg.C;
import com.ebr163.ibikg.event.BlackListEvent;
import com.ebr163.ibikg.service.SettingsService;

import java.util.HashMap;
import java.util.Map;

import de.greenrobot.event.EventBus;

/**
 * Created by Bakht on 12.04.2016.
 */
public class MessageDialog extends DialogFragment {

    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Удаление")
                .setMessage("Удалить контакт из черного списка?")
                .setNegativeButton("Отмена", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dismiss();
                    }
                })
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Map<String, String> map = new HashMap<>();
                        map.put(C.fields.key, SettingsService.getUser(getActivity()).key);
                        map.put(C.fields.id, getArguments().getString(C.advert.id));
                        EventBus.getDefault().post(new BlackListEvent(map, getArguments().getInt("position")));
                    }
                });
        return builder.create();
    }
}

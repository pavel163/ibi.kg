package com.ebr163.ibikg.adapter.category;

import android.app.Activity;
import android.content.Intent;
import android.view.View;

import com.ebr163.ibikg.C;
import com.ebr163.ibikg.R;
import com.ebr163.ibikg.activity.MyAdDescActivity;
import com.ebr163.ibikg.ui.SmartyImageView;
import com.ebr163.ibikg.ui.SmartyTextView;

/**
 * Created by Bakht on 03.04.2016.
 */
public class MyAdHolder extends AdHolder {

    protected SmartyTextView publish;

    public static MyAdHolder newInstance(View parent) {
        SmartyImageView image = (SmartyImageView) parent.findViewById(R.id.image);
        SmartyTextView title = (SmartyTextView) parent.findViewById(R.id.title);
        SmartyTextView price = (SmartyTextView) parent.findViewById(R.id.price);
        SmartyTextView data = (SmartyTextView) parent.findViewById(R.id.data);
        SmartyTextView region = (SmartyTextView) parent.findViewById(R.id.region);
        SmartyTextView id = (SmartyTextView) parent.findViewById(R.id.id);
        SmartyTextView currency = (SmartyTextView) parent.findViewById(R.id.currency);
        SmartyTextView publish = (SmartyTextView) parent.findViewById(R.id.published);
        return new MyAdHolder(parent, image, title, price, data, region, id, currency, publish);
    }

    public MyAdHolder(View itemView, SmartyImageView image, SmartyTextView title, SmartyTextView price, SmartyTextView data,
                      SmartyTextView region, SmartyTextView id, SmartyTextView currency, SmartyTextView publish) {
        super(itemView, image, title, price, data, region, id, currency);
        this.publish = publish;
    }

    public void setPublish(String publish) {
        if (publish != null)
            if (publish.equals("1")) {
                this.publish.setText("Опубликовано");
            } else {
                this.publish.setText("Не опубликовано");
            }
    }

    @Override
    public void onClick(View v) {
        Intent intent = new Intent(v.getContext(), MyAdDescActivity.class);
        intent.putExtra(C.fields.parent_id, id.getText().toString());
        ((Activity)v.getContext()).startActivityForResult(intent, 111);
    }
}

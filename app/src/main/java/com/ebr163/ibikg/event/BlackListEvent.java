package com.ebr163.ibikg.event;

import com.ebr163.ibikg.event.base.BaseEvent;

import java.util.Map;

/**
 * Created by Bakht on 12.04.2016.
 */
public class BlackListEvent extends BaseEvent {

    public final Map<String, String> data;
    public final int position;

    public BlackListEvent(Map<String, String> data, int position) {
        this.data = data;
        this.position = position;
    }
}

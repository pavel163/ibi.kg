package com.ebr163.ibikg.activity;

import android.os.Bundle;

import com.ebr163.ibikg.C;
import com.ebr163.ibikg.R;
import com.ebr163.ibikg.fragment.SearchCategoryFragment;

public class SearchCategoryActivity extends CategoryActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setToolbarTitle("Дополнительные параметры");
    }

    @Override
    protected void initFragment() {
        fragment = SearchCategoryFragment.newInstance(getIntent().getStringExtra(C.fields.parent_id));
        getSupportFragmentManager().beginTransaction()
                .add(R.id.main_fragment_container, fragment)
                .commit();
    }
}

package com.ebr163.ibikg.ui;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.util.AttributeSet;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.ebr163.ibikg.R;


/**
 * Created by Bakht on 21.02.2016.
 */
public class SmartyImageView extends ImageView {

    public SmartyImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
        setImage(R.drawable.no_image);
    }

    public void setImage(String url) {
        Glide.with(getContext())
                .load(url)
                .fitCenter()
                .into(this);
    }

    public void setImage(Context context, String url) {
        Glide.with(context)
                .load(url)
                .fitCenter()
                .dontTransform()
                .into(this);
    }

    public void setImage(Uri url) {
        Glide.with(getContext())
                .load(url)
                .dontTransform()
                .into(this);
    }

    public void setImage(Drawable drawable) {
        setImageDrawable(drawable);
    }

    public void setImage(int res) {
        setImageResource(res);
    }
}

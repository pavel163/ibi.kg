package com.ebr163.ibikg.activity;

import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.ebr163.ibikg.BaseApplication;
import com.ebr163.ibikg.C;
import com.ebr163.ibikg.R;
import com.ebr163.ibikg.activity.base.BaseActivity;
import com.ebr163.ibikg.service.SettingsService;
import com.ebr163.ibikg.service.http.HttpApiFactory;
import com.ebr163.ibikg.util.ValidationUtils;

import java.util.HashMap;
import java.util.Map;

import rx.functions.Action1;
import rx.functions.Func1;

public class UpdatePassActivity extends BaseActivity implements View.OnClickListener {

    private TextInputLayout oldPass;
    private TextInputLayout newPass;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.activity_update_pass);
        super.onCreate(savedInstanceState);
        setToolbarTitle("Смена пароля");
    }

    @Override
    protected void initUI() {
        oldPass = (TextInputLayout) findViewById(R.id.old_pass);
        newPass = (TextInputLayout) findViewById(R.id.new_pass);
    }

    private void updatePass() {
        progress.show();
        Action1<Map<String, Object>> onNextAction = new Action1<Map<String, Object>>() {
            @Override
            public void call(Map<String, Object> response) {
                progress.hide();
                if (response != null) {
                    Toast.makeText(UpdatePassActivity.this, "Пароль изменен", Toast.LENGTH_LONG).show();
                }
            }
        };

        Map<String, String> map = new HashMap<>();
        map.put(C.fields.key, SettingsService.getUser(this).key);
        map.put(C.fields.old_pass, oldPass.getEditText().getText().toString().trim());
        map.put(C.fields.new_pass, newPass.getEditText().getText().toString().trim());
        BaseApplication.instance.httpService.subscribeAction(HttpApiFactory.HttpMethodType.UPDATE_PASS, onNextAction, getErrorFunc(), map);
    }

    private Func1 getErrorFunc() {
        return new Func1() {
            @Override
            public Object call(Object o) {
                progress.hide();
                Toast.makeText(UpdatePassActivity.this, "Ошибка. Проверьте подключение к интернету", Toast.LENGTH_LONG).show();
                Log.d("Смена пароля", "Провал");
                return null;
            }
        };
    }

    @Override
    public void onClick(View v) {
        if (!"".equals(ValidationUtils.getFieldValue(oldPass)) && !"".equals(ValidationUtils.getFieldValue(newPass))){
            updatePass();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        BaseApplication.instance.httpService.unsubscribe();
    }
}

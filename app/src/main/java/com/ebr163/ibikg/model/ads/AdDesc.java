package com.ebr163.ibikg.model.ads;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * Created by mac1 on 24.03.16.
 */
public class AdDesc implements Serializable{
    public String id;
    public String category_id;
    public String user_id;
    public String city_id;
    public String advert_name;
    public String advert_descr;
    public String price;
    public String currency_id;
    public String date_publish;
    public String address;
    public String advert_phone;
    public String contact;
    public String hits;
    public String published;
    public String mode_premium_until;
    public String mode_vip_until;
    public String mode_highlight_until;
    public String name_category;
    public String city_name;
    public String ibi_name;
    public String name_currency;
    public String shop_enabled;
    public String advert_url;
    public List<Map<String, String>> photos;
    public List<Map<String, Object>> advert_custom_values;
}

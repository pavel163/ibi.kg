package com.ebr163.ibikg.model.post;

/**
 * Created by Bakht on 26.03.2016.
 */
public class Message {
    public String key;
    public String reply_to;
    public String to_user_id;
    public String mail_title;
    public String mail_body;
}

package com.ebr163.ibikg.adapter.category;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.ebr163.ibikg.R;
import com.ebr163.ibikg.model.ads.Ad;

import java.util.List;

/**
 * Created by Bakht on 21.03.2016.
 */
public class AdAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    protected List<Object> ads;

    public AdAdapter(List<Object> ads) {
        this.ads = ads;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_ad, parent, false);
        return AdHolder.newInstance(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        AdHolder holder = (AdHolder) viewHolder;
        setData(holder, position);
        //TODO Порефакторить нижний код и подключить ButterKnife
    }

    @Override
    public int getItemCount() {
        return ads.size();
    }

    protected void setData(AdHolder holder, int position){
        Ad ad = (Ad) ads.get(position);
        holder.setId(ad.id);
        holder.setTitle(ad.advert_name.trim());
        if (ad.price != null){
            holder.setPrice(ad.price);
            holder.setCurrency(ad.name_currency);
        }
        holder.setData(ad.date_publish);
        holder.setRegion(ad.region_name);
        if (ad.photos != null && ad.photos.size() != 0){
            holder.setImage("http:" + ad.photos.get(0).get("photo_path").toString());
        } else {
            Glide.clear(holder.image);
            holder.image.setImage(R.drawable.no_image);
        }
    }

    public void setFilter(List<Object> objectList) {
        this.ads = objectList;
        notifyDataSetChanged();
    }
}

package com.ebr163.ibikg.fragment;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.ebr163.ibikg.C;
import com.ebr163.ibikg.R;
import com.ebr163.ibikg.activity.ShareActivity;
import com.ebr163.ibikg.model.ads.AdDesc;

/**
 * Created by Bakht on 03.04.2016.
 */
public class MyAdDescFragment extends AdDescFragment {

    private Menu menu;
    private OnMenuItemListener menuItemListener;

    public interface OnMenuItemListener {
        void onPublish(String id);
        void onRemovePublish(String id);
        void onRemoveAdvert(String id);
    }

    public static MyAdDescFragment newInstance() {
        return new MyAdDescFragment();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        menuItemListener = (OnMenuItemListener) context;
    }

    @Override
    public int getLayout() {
        return R.layout.fragmet_my_advert;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_fragment_my_advert, menu);
        this.menu = menu;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.no_publish){
            menuItemListener.onRemovePublish(data.id);
        }
        if (item.getItemId() == R.id.yes_publish){
            menuItemListener.onPublish(data.id);
        }
        if (item.getItemId() == R.id.delete_advert){
            //TODO не обновляется список. Надо пофиксить и сделать при удалении выход назад
            menuItemListener.onRemoveAdvert(data.id);
        }
        if (item.getItemId() == R.id.update_advert){
            Intent intent = new Intent(getActivity(), ShareActivity.class);
            intent.putExtra(C.fields.data, data);
            intent.putExtra(C.fields.parent_id, data.category_id);
            startActivityForResult(intent, 111);
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 111 && resultCode == Activity.RESULT_OK){
            clearCustomFields();
            objectsListener.setObjects();
        }
    }

    @Override
    public void setData(AdDesc data) {
        super.setData(data);
        if ("1".equals(data.published)){
            menu.getItem(2).setVisible(true);
        } else {
            menu.getItem(3).setVisible(true);
        }
    }

    @Override
    protected void setContactData() {}

    public void statusPublished(String status){
        if (status.equals("true")){
            menu.getItem(2).setVisible(true);
            menu.getItem(3).setVisible(false);
        } else {
            menu.getItem(2).setVisible(false);
            menu.getItem(3).setVisible(true);
        }
    }
}

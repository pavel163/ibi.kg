package com.ebr163.ibikg.activity.login;

import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.ebr163.ibikg.BaseApplication;
import com.ebr163.ibikg.C;
import com.ebr163.ibikg.R;
import com.ebr163.ibikg.event.LoginEvent;
import com.ebr163.ibikg.fragment.login.SignInFragment;
import com.ebr163.ibikg.model.Response;
import com.ebr163.ibikg.model.ResponseContact;
import com.ebr163.ibikg.model.User;
import com.ebr163.ibikg.service.SettingsService;
import com.ebr163.ibikg.service.http.HttpApiFactory;

import java.util.Map;

import rx.functions.Action1;
import rx.functions.Func1;

public class SignInActivity extends LoginActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.activity_with_fragment);
        super.onCreate(savedInstanceState);
        setToolbarTitle("Вход");
    }

    @Override
    protected void initFragment() {
        fragment = SignInFragment.newInstance();
        getSupportFragmentManager().beginTransaction()
                .add(R.id.main_fragment_container, fragment)
                .commit();
    }

    @Override
    protected void initToolbar() {
        super.initToolbar();
        toolbar.setNavigationIcon(R.drawable.ic_clear_24dp);
    }

    public void onEvent(final LoginEvent event) {
        Action1<Response<Object>> onNextAction = new Action1<Response<Object>>() {
            @Override
            public void call(Response<Object> response) {
                if (response != null) {
                    if (response.success) {
                        Map<String, String> map = (Map) response.data;
                        SettingsService.saveUser(SignInActivity.this, new User(event.name, event.email, event.phone, event.password, map.get(C.login.id), map.get(C.login.user_key)));
                        Log.d("Авторизация", "Успех");
                        getContact();
                    } else {
                        //TODO Заменить на SnackBar
                        progress.hide();
                        Toast.makeText(SignInActivity.this, "Ошибка. Некорректные данные", Toast.LENGTH_LONG).show();
                        Log.d("Авторизация", "Сервер вернул ошибку");
                    }
                }
            }
        };

        progress.show();
        BaseApplication.instance.httpService.subscribeAction(HttpApiFactory.HttpMethodType.AUTHORIZATION, onNextAction, getErrorFunc(), event);
    }

    private void getContact() {
        Action1<ResponseContact> onNextAction = new Action1<ResponseContact>() {
            @Override
            public void call(ResponseContact response) {
                progress.hide();
                if (response != null) {
                    if (response.success) {
                        User u = SettingsService.getUser(SignInActivity.this);
                        u = new User(response.data.ibi_name, u.email, response.data.phone, u.password, u.id, u.key);
                        SettingsService.saveUser(SignInActivity.this, u);
                        onBackPressed();
                        finish();
                    } else {
                        //TODO Заменить на SnackBar
                        Toast.makeText(SignInActivity.this, "Ошибка. Некорректные данные", Toast.LENGTH_LONG).show();
                        Log.d("getContact", "Сервер вернул ошибку");
                        progress.hide();
                    }
                }
            }
        };

        BaseApplication.instance.httpService.subscribeAction(HttpApiFactory.HttpMethodType.GET_CONTACT, onNextAction, getErrorFunc(), SettingsService.getUser(this).key);
    }

    @Override
    protected void onStop() {
        super.onStop();
        BaseApplication.instance.httpService.unsubscribe();
    }

    private Func1 getErrorFunc() {
        return new Func1() {
            @Override
            public Object call(Object o) {
                progress.hide();
                Toast.makeText(SignInActivity.this, "Ошибка. Проверьте подключение к интернету", Toast.LENGTH_LONG).show();
                Log.d("Авторизация", "Провал");
                return null;
            }
        };
    }
}
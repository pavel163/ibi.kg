package com.ebr163.ibikg.fragment;

import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;

import com.ebr163.ibikg.R;
import com.ebr163.ibikg.activity.base.BaseActivity;
import com.ebr163.ibikg.fragment.base.BaseFragment;
import com.ebr163.ibikg.fragment.base.BaseFragmentWithList;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Bakht on 17.04.2016.
 */
public class VPFragment extends BaseFragment {

    public static VPFragment newInstance(){
        return new VPFragment();
    }

    private ViewPager viewPager;
    private TabLayout tabLayout;

    private MyAdvertFragmentPublish fragment1;
    private MyAdvertFragmentNoPublish fragment2;

    @Override
    public int getLayout() {
        return R.layout.fragment_vp;
    }

    @Override
    protected void initUI(View view) {
        viewPager = (ViewPager) view.findViewById(R.id.viewpager);
        setupViewPager(viewPager);
        tabLayout = ((BaseActivity)getActivity()).getTabs();
        tabLayout.setupWithViewPager(viewPager);
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getChildFragmentManager());
        fragment1 = MyAdvertFragmentPublish.newInstance();
        fragment2 = MyAdvertFragmentNoPublish.newInstance();
        adapter.addFragment(fragment1, "Опубликованные");
        adapter.addFragment(fragment2, "Не опубликованные");
        viewPager.setOffscreenPageLimit(0);
        viewPager.setAdapter(adapter);
    }

    public void setObjectList(List<Object> objectList, int fragment){
        if (fragment == 1){
            fragment1.setObjectList(objectList);
            fragment1.setupRecyclerView(objectList);
        } else {
            fragment2.setObjectList(objectList);
            fragment2.setupRecyclerView(objectList);
        }
    }

    public void addObjectList(List<Object> objectList, int fragment){
        if (fragment == 1){
            fragment1.addObjectList(objectList);
            fragment1.updateList();
        } else {
            fragment2.addObjectList(objectList);
            fragment2.updateList();
        }
    }

    public void clearData(){
        fragment1.clearData();
        fragment2.clearData();
        fragment1.updateList();
        fragment2.updateList();
    }

    public void errorAction(){
        fragment1.setVisibleErrorMessage(View.VISIBLE);
        fragment1.setVisibleProgress(View.GONE);

        fragment2.setVisibleErrorMessage(View.VISIBLE);
        fragment2.setVisibleProgress(View.GONE);
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }
}

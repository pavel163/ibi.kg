package com.ebr163.ibikg.activity;

import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.ebr163.ibikg.BaseApplication;
import com.ebr163.ibikg.C;
import com.ebr163.ibikg.R;
import com.ebr163.ibikg.activity.base.BaseActivityWithFragment;
import com.ebr163.ibikg.fragment.MessageDescFragment;
import com.ebr163.ibikg.model.Response;
import com.ebr163.ibikg.model.ResponseMsgDesc;
import com.ebr163.ibikg.service.SettingsService;
import com.ebr163.ibikg.service.http.HttpApiFactory;

import java.util.HashMap;
import java.util.Map;

import rx.functions.Action1;
import rx.functions.Func1;

public class MessageDescActivity extends BaseActivityWithFragment implements MessageDescFragment.OnSetObjectsListener{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.activity_with_fragment);
        super.onCreate(savedInstanceState);
        setToolbarTitle("Сообщение");
    }

    @Override
    protected void initUI() {
        super.initUI();
    }

    @Override
    protected void initFragment() {
        fragment = MessageDescFragment.newInstance();
        getSupportFragmentManager().beginTransaction()
                .add(R.id.main_fragment_container, fragment)
                .commit();
        progress.show();
        setData();
    }

    private void setData() {
        Action1<ResponseMsgDesc<Object>> onNextAction = new Action1<ResponseMsgDesc<Object>>() {
            @Override
            public void call(ResponseMsgDesc<Object> response) {
                progress.hide();
                if (response != null) {
                    ((MessageDescFragment) fragment).setData(response);
                    Log.d("Загрузка сообщения", "Успех");
                }
            }
        };

        Func1 error = new Func1() {
            @Override
            public Object call(Object o) {
                progress.hide();
                Log.d("Загрузка сообщения", "Провал");
                return null;
            }
        };

        Map<String, String> map = new HashMap<>();
        map.put(C.fields.key, SettingsService.getUser(this).key);
        map.put(C.message.id, getIntent().getStringExtra(C.message.id));
        BaseApplication.instance.httpService.subscribeAction(HttpApiFactory.HttpMethodType.GET_USER_MAIL_BY_ID, onNextAction, error, map);
    }

    @Override
    public void setObjects(String id) {
        progress.show();
        Action1<Response<String>> onNextAction = new Action1<Response<String>>() {
            @Override
            public void call(Response<String> response) {
                progress.hide();
                Toast.makeText(MessageDescActivity.this, "Контакт добавлен в черный список", Toast.LENGTH_LONG).show();
                Log.d("В черный список", "Успех");
            }
        };

        Func1 error = new Func1() {
            @Override
            public Object call(Object o) {
                progress.hide();
                Toast.makeText(MessageDescActivity.this, "Ошибка. Контакт не добавлен в черный список", Toast.LENGTH_LONG).show();
                Log.d("В черный список", "Провал");
                return null;
            }
        };

        Map<String, String> map = new HashMap<>();
        map.put(C.fields.key, SettingsService.getUser(this).key);
        map.put(C.message.id, id);
        BaseApplication.instance.httpService.subscribeAction(HttpApiFactory.HttpMethodType.ADD_TO_BLACK_LIST, onNextAction, error, map);
    }

    @Override
    protected void onStop() {
        super.onStop();
        BaseApplication.instance.httpService.unsubscribe();
    }
}

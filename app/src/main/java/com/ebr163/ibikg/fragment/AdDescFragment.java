package com.ebr163.ibikg.fragment;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ebr163.ibikg.C;
import com.ebr163.ibikg.R;
import com.ebr163.ibikg.activity.MessageActivity;
import com.ebr163.ibikg.activity.ShowImageActivity;
import com.ebr163.ibikg.activity.login.SignInActivity;
import com.ebr163.ibikg.adapter.SmartyPagerAdapter;
import com.ebr163.ibikg.fragment.base.BaseFragment;
import com.ebr163.ibikg.model.User;
import com.ebr163.ibikg.model.ads.AdDesc;
import com.ebr163.ibikg.service.SettingsService;
import com.ebr163.ibikg.ui.SmartyImageView;
import com.ebr163.ibikg.ui.SmartyTextView;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import me.relex.circleindicator.CircleIndicator;

/**
 * Created by Bakht on 22.03.2016.
 */
public class AdDescFragment extends BaseFragment implements View.OnClickListener {

    protected ViewPager viewPager;
    protected View rootView;
    protected CircleIndicator defaultIndicator;
    protected OnSetObjectsListener objectsListener;
    protected AdDesc data;

    public static AdDescFragment newInstance() {
        return new AdDescFragment();
    }

    public interface OnSetObjectsListener {
        void setObjects();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        objectsListener = (OnSetObjectsListener) context;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public int getLayout() {
        return R.layout.fragment_ad_desc;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_share, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.share_item) {
            Intent sendIntent = new Intent();
            sendIntent.setAction(Intent.ACTION_SEND);
            sendIntent.putExtra(Intent.EXTRA_TEXT, "http:" + data.advert_url);
            sendIntent.setType("text/plain");
            startActivity(sendIntent);
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void initUI(View view) {
        initViewPager(view);
        rootView = view;
        if (data == null) {
            objectsListener.setObjects();
        } else {
            setData(data);
        }
    }

    private void initViewPager(View view) {
        //TODO поместить в collapsingtoolbar
        viewPager = (ViewPager) view.findViewById(R.id.viewpager);
        defaultIndicator = (CircleIndicator) view.findViewById(R.id.indicator_default);
    }

    private List<View> createView(List<String> linkImage) {
        LayoutInflater inflater = LayoutInflater.from(getActivity());
        List<View> pages = new ArrayList<>();
        for (int i = 0; i < linkImage.size(); i++) {
            View inflate = inflater.inflate(R.layout.viewpager_page, null);
            SmartyImageView imageView = (SmartyImageView) inflate.findViewById(R.id.image);
            final Intent intent = createInOnClickImage(i, linkImage);
            imageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivity(intent);
                }
            });
            imageView.setImage(this.getContext(), linkImage.get(i));
            pages.add(inflate);
        }
        return pages;
    }

    private Intent createInOnClickImage(int position, List<String> data) {
        Intent intent = new Intent(getActivity(), ShowImageActivity.class);
        intent.putStringArrayListExtra("data", (ArrayList<String>) data);
        intent.putExtra("pos", position);
        return intent;
    }

    public void setData(AdDesc data) {
        this.data = data;

        rootView.findViewById(R.id.progress).setVisibility(View.GONE);
        rootView.findViewById(R.id.content).setVisibility(View.VISIBLE);

        SmartyPagerAdapter adapter = new SmartyPagerAdapter(createView(getImages(data.photos)));
        viewPager.setAdapter(adapter);
        defaultIndicator.setViewPager(viewPager);

        SmartyTextView title = (SmartyTextView) rootView.findViewById(R.id.title);
        title.setText(data.advert_name);

        SmartyTextView price = (SmartyTextView) rootView.findViewById(R.id.price);
        price.setText(data.price + " " + data.name_currency);

        SmartyTextView address = (SmartyTextView) rootView.findViewById(R.id.address);
        address.setText(data.city_name + ", " + data.address);

        SmartyTextView description = (SmartyTextView) rootView.findViewById(R.id.description);
        description.setText(data.advert_descr);

        setCustomFields();

        setContactData();
    }

    protected void setCustomFields() {
        List<Map<String, Object>> customFields = data.advert_custom_values;
        for (Map<String, Object> customField : customFields) {
            setCustomField(customField);
        }
    }

    protected void setCustomField(Map<String, Object> customField){
        View view = getActivity().getLayoutInflater().inflate(R.layout.custom_field, null, false);
        ((TextView)view.findViewById(R.id.custom_value)).setText(customField.get(C.advert.custom_name).toString() + ": "
                + customField.get(C.advert.custom_value).toString());
        ((LinearLayout) rootView.findViewById(R.id.custom_fields)).addView(view);
    }

    protected void setContactData() {
        SmartyTextView contact = (SmartyTextView) rootView.findViewById(R.id.contact);
        contact.setText(data.ibi_name);

        SmartyTextView phone = (SmartyTextView) rootView.findViewById(R.id.phone);
        phone.setText(data.advert_phone);
        phone.setOnClickListener(this);

        SmartyTextView message = (SmartyTextView) rootView.findViewById(R.id.message);
        message.setOnClickListener(this);
    }

    private List<String> getImages(List<Map<String, String>> data) {
        List<String> images = new ArrayList<>();
        for (Map<String, String> map : data) {
            images.add("http:" + map.get(C.advert.photo_path));
        }
        return images;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.phone:
                Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + data.advert_phone));
                startActivity(intent);
                break;
            case R.id.message:
                User user = SettingsService.getUser(getActivity());
                if (user == null || user.key.equals("")) {
                    startActivity(new Intent(getActivity(), SignInActivity.class));
                } else {
                    Intent i = new Intent(getActivity(), MessageActivity.class);
                    i.putExtra(C.advert.ibi_name, data.ibi_name);
                    i.putExtra(C.advert.user_id, data.user_id);
                    startActivity(i);
                }
                break;
        }
    }

    protected void clearCustomFields(){
        ((LinearLayout) rootView.findViewById(R.id.custom_fields)).removeAllViews();
    }

    public AdDesc getData() {
        return data;
    }
}

package com.ebr163.ibikg.service.http;

import android.support.annotation.NonNull;
import android.util.Log;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.functions.Func1;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by Bakht on 11.03.2016.
 */
public class HttpService {

    protected HttpApiFactory httpApiFactory;
    private CompositeSubscription subscription = new CompositeSubscription();

    public HttpService(HttpApiFactory httpApiFactory) {
        this.httpApiFactory = httpApiFactory;
    }

    public void subscribeAction(@NonNull HttpApiFactory.HttpMethodType type, @NonNull Action1 action, Func1 error, Object query) {
        Observable observable = httpApiFactory.getHttpMethod(type, query);

        if (observable != null) {
            subscription.add(observable.subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .onErrorResumeNext(error)
                    .subscribe(action, new Action1<Throwable>() {
                        @Override
                        public void call(Throwable throwable) {
                            Log.e("httpService", "Ошибка в action");
                        }
                    }));
        }
    }

    public void unsubscribe() {
        subscription.clear();
    }
}


package com.ebr163.ibikg.activity.base;

import android.app.ProgressDialog;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.TextView;

import com.ebr163.ibikg.R;

import de.greenrobot.event.EventBus;

/**
 * Created by Bakht on 10.03.2016.
 */
public abstract class BaseActivity extends AppCompatActivity {

    protected Toolbar toolbar;
    protected ProgressDialog progress;
    protected TabLayout tabLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        progress = new ProgressDialog(this);
        progress.setMessage("Пожалуйста, подождите");
        initUI();
        initToolbar();
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    protected abstract void initUI();

    protected void initToolbar() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        tabLayout = (TabLayout) findViewById(R.id.tabs);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    protected void setToolbarTitle(String title) {
        toolbar.setTitleTextColor(Color.parseColor("#424242"));
        toolbar.setSubtitleTextColor(Color.GRAY);
        toolbar.setTitle(title);
    }

    protected void setToolbarSubTitle(String subTitle){
        toolbar.setSubtitle(subTitle);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    protected void setVisibleTabs(int visible){
        tabLayout.setVisibility(visible);
    }

    public TabLayout getTabs(){
        return tabLayout;
    }
}

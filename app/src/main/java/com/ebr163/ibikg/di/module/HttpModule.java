package com.ebr163.ibikg.di.module;

import com.ebr163.ibikg.service.http.HttpApiFactory;
import com.ebr163.ibikg.service.http.HttpService;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.concurrent.TimeUnit;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Bakht on 23.05.2016.
 */
@Module
public class HttpModule {

    @Singleton
    @Provides
    Retrofit provideHttpClient() {
        OkHttpClient.Builder b = new OkHttpClient.Builder();
        b.readTimeout(60, TimeUnit.SECONDS);
        b.writeTimeout(60, TimeUnit.SECONDS);
        b.connectTimeout(65, TimeUnit.SECONDS);

        OkHttpClient client = b.build();

        Gson gson = new GsonBuilder()
                .setLenient()
                .create();

        return new Retrofit.Builder()
                .baseUrl("http://ibi.kg")
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .client(client)
                .build();
    }

    @Singleton
    @Provides
    HttpApiFactory provideHttpApiFactory(Retrofit retrofit) {
        return new HttpApiFactory(retrofit);
    }

    @Singleton
    @Provides
    HttpService provideHttpService(HttpApiFactory httpApiFactory) {
        return new HttpService(httpApiFactory);
    }
}

package com.ebr163.ibikg.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.BaseAdapter;

import com.ebr163.ibikg.C;
import com.ebr163.ibikg.R;
import com.ebr163.ibikg.activity.AdActivity;
import com.ebr163.ibikg.activity.CategoryActivity;
import com.ebr163.ibikg.adapter.CategoryAdapter;
import com.ebr163.ibikg.fragment.base.BaseFragmentWithListView;
import com.ebr163.ibikg.model.Category;

import java.util.List;

/**
 * Created by Bakht on 11.03.2016.
 */
public class CategoryFragment extends BaseFragmentWithListView {

    private String parentId = "0";

    public static CategoryFragment newInstance() {
        return new CategoryFragment();
    }

    public static CategoryFragment newInstance(String id) {
        Bundle args = new Bundle();
        args.putString(C.fields.parent_id, id);
        CategoryFragment categoryFragment = new CategoryFragment();
        categoryFragment.setArguments(args);
        return categoryFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle args = getArguments();
        if (args != null) {
            parentId = args.getString(C.fields.parent_id);
        }
    }

    @Override
    protected void clickItem(int position) {
        Intent intent = null;
        int level = getActivity().getIntent().getIntExtra("level", 1);
        if ("1".equals(((Category) objectList.get(position)).childs) && level < 2) {
            intent = new Intent(getActivity(), CategoryActivity.class);
            intent.putExtra("level", level + 1);
        } else {
            intent = new Intent(getActivity(), AdActivity.class);
        }
        intent.putExtra(C.fields.parent_id, ((Category) objectList.get(position)).id);
        intent.putExtra(C.fields.title, ((Category) objectList.get(position)).name_category);
        startActivity(intent);
    }

    @Override
    protected BaseAdapter getAdapter(List<Object> objectList) {
        return new CategoryAdapter(getActivity(), objectList);
    }

    public String getParentId() {
        return parentId;
    }
}
